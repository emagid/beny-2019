<?php
require(__DIR__.'/../index.php');

$filePaths = [__DIR__.'/BENY_Booth.csv'];
$arrResult = [];
$head = true;
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if($head){
                $head = false;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
        $head = true;
    }
}

for($x = 0; $x < count($arrResult); $x++) {
    $name = trim($arrResult[$x][0]);
    $number = trim($arrResult[$x][1]);
    $category = $arrResult[$x][2];
    $category2 = $arrResult[$x][3];

    $booth = null;
    if($number){
        $booth = \Model\Booth::getItem(null,['where'=>"number = '$number'"])? : new \Model\Booth();
        if($booth->id == 0){
            $booth->number = trim($number);
            $booth->name = trim($name);
            $booth->save();
        }
    }

    if($category){
        $slug = str_replace(' ', '-', strtolower($category));
        $categoryModel = \Model\Category::getItem(null,['where'=>"slug = '$slug'"])? : new \Model\Category();
        if($categoryModel->id == 0){
            $categoryModel->name = $category;
            $categoryModel->slug = $slug;
            $categoryModel->save();
        }


        if($booth){
            $bootCategory = \Model\BoothCategory::getItem(null,['where'=>"booth_id = '$booth->id' and 'category_id' = '$categoryModel->id'"])? : new \Model\BoothCategory();
            $bootCategory->booth_id = $booth->id;
            $bootCategory->category_id = $categoryModel->id;
            $bootCategory->save();
        }
    }

    if($category2){
        $slug2 = str_replace(' ', '-', strtolower($category2));
        $categoryModel2 = \Model\Category::getItem(null,['where'=>"slug = '$slug2'"])? : new \Model\Category();
        if($categoryModel2->id == 0){
            $categoryModel2->name = $category2;
            $categoryModel2->slug = $slug2;
            $categoryModel2->save();
        }

        if($booth){
            $bootCategory = \Model\BoothCategory::getItem(null,['where'=>"booth_id = '$booth->id' and 'category_id' = '$categoryModel2->id'"])? : new \Model\BoothCategory();
            $bootCategory->booth_id = $booth->id;
            $bootCategory->category_id = $categoryModel2->id;
            $bootCategory->save();
        }
    }

    
    echo "Processed Line $x : $name saved \n";
}

