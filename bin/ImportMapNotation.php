<?php
require(__DIR__.'/../index.php');

$filePaths = [__DIR__.'/Map.csv'];
$arrResult = [];
$head = true;
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if($head){
                $head = false;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
        $head = true;
    }
}

for($x = 0; $x < count($arrResult); $x++) {
    $notation = trim($arrResult[$x][0]);
    $store = trim($arrResult[$x][1]);
    if($notation){
        $notationModel = \Model\Notation::getItem(null,['where'=>"name = '$notation'"])? : new \Model\Notation();
        if($notationModel->id == 0){
            $notationModel->name = trim($notation);
            $notationModel->save();
        }

        if($store){
            $storeModel = \Model\Store::getItem(null,['where'=>"name = '$store'"])? : new \Model\Store();
            if($storeModel->id == 0){
                $storeModel->name = trim($store);
                $storeModel->notation_id = $notationModel->id;
                $storeModel->save();
            }


        }
    }
    
    echo "Processed Line $x : $notation : $store  saved \n";
}

