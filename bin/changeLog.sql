create table administrator(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
first_name character varying,
last_name character varying,
email character varying,
username character varying,
password character varying,
hash character varying,
permissions character varying);

create table admin_roles(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
admin_id integer not null,
role_id integer not null
);

create table booth(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
number character varying
);

create table booth_category(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
booth_id INTEGER,
category_id INTEGER
);

create table category(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
description character varying,
slug character varying,
parent_category INTEGER
);

create table config(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
value character varying
);

create table newsletter(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
email character varying
);

create table page(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
title character varying,
slug character varying,
description character varying,
featured_image character varying,
meta_title character varying,
meta_keywords character varying,
meta_description character varying
);

create table role(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying not null
);

create table public.user(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
email character varying not null,
password character varying not null,
hash character varying,
first_name character varying,
last_name character varying
);

create table user_roles(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
user_id integer not null,
role_id integer not null
);

insert into config (name,value) values (
'Meta Title','Bike Expo WIP'
);
insert into config (name,value) values (
'Meta Description','Bike Expo WIP'
);
insert into config (name,value) values (
'Meta Keywords','Bike Expo WIP'
);

insert into administrator (first_name, last_name, email, username, password, hash, permissions) VALUES (
'Master','User','admin@admin.com','emagid','5569cb9713e7fed733917f7c465d8c0a091e08a0e20837bdc836989b702fa3aa','591232c9fccd7c9d29c1e090f60964b7','Catalog,Categories,Products,Price alerts,Newproducts,Kenjos,Attributes,Hottest Deal,Attributes,Bezels,Bracestraps,Buckles,Buckle_colors,Casebks,Casematerials,Crowns,Crystals,Funcs,Lug_widths,Movement_drops,Water_ress,Water_res_multis,Order Management,Orders,Customer Center,Wishlist,Users,Questions,Newsletter,Content,Pages,News,Banners,Main,Featured,Deal of the Week,System,Coupons,Shipping Methods,Brands,Colors,Collections,Materials,Configs,Answers,Administrators'
);

INSERT INTO role (name) VALUES ('admin');

INSERT INTO admin_roles(admin_id, role_id) VALUES ('1','1');

-- Apr 5th
create table notation(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying
);

create table store(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
description character varying,
notation_id INTEGER
);

GRANT ALL ON table notation to benymyma_user;
GRANT ALL ON table store to benymyma_user;

GRANT ALL ON SEQUENCE notation_id_seq to benymyma_user;
GRANT ALL ON SEQUENCE store_id_seq to benymyma_user;

ALTER table store add column image CHARACTER VARYING;

-- Apr 14
ALTER table booth add column icon CHARACTER VARYING;