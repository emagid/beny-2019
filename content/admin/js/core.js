/**
 * Created by Admin on 6/8/16.
 */
$(document).ready(function(){
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var currentUrl = window.location.href;

    var parameters = getParameterByName('auto_submit', currentUrl);

    if(parameters){
        $("form[name = 'form1']").submit();
        window.onbeforeunload = null;
    } else {
        // Prevent navigation to previous page by clicking backspace.
        window.onbeforeunload = function() {
            return "Your current entries will be lost.";
        };
    }
});