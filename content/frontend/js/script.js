$('.my_filters li a').click(function(e){
	e.preventDefault();
	var brands_filter = $(this).text();
	var t = [];
	$('.my_filter_items li').hide().each(function(){
		if ($('span', this).text().toUpperCase()[0] == brands_filter){
			t.push($(this).clone().show());
		};
	});
	$(t).each(function(){
		$(this).appendTo('.my_filter_items');
	});
    return false;
});

$(function(){
	$('#clone').click(function(e){
		e.preventDefault();
		$('#ship_data').find('input').each(function(){
			var name = $(this).attr('name');
			if (name != undefined){
				name = name.substr(5);
				var val = $('input[name="bill_'+name+'"]').val();
				$(this).val(val);
			}
		})
		$('#ship_data').find('select').each(function(){
			var name = $(this).attr('name');
			if (name != undefined){
				name = name.substr(5);
				var selected = $('select[name="bill_'+name+'"]').val();
				console.log(selected);
				$('option', this).each(function(){
					if ($(this).val() == selected){
						$(this).attr('selected', 'selected');
					} else {
						$(this).removeAttr('selected');
					}
				});
			}	
		})
		$('select[name="ship_state"]').change();
		return false;
	});

	// initial timeout redirect homepage
	// var initial = null;

	// function invoke() {
	// 	initial = window.setTimeout(
	// 		function() {
	// 			window.location.href = '/';
	// 		}, 30000);
	// }

	// invoke();

	// $('body').on('click mousemove', function(){
	// 	window.clearTimeout(initial);
	// 	invoke();
	// })

});