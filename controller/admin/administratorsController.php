<?php

use Emagid\Core\Membership,
	Model\Admin,
	Emagid\Html\Form;

class administratorsController extends adminController {
	
	function __construct(){
		parent::__construct("Admin");
	}

	function index(Array $params = [])
	{
		$this->_viewData->hasCreateBtn = true;
		if (strtolower($this->_viewData->logged_admin->getRoleName()) != 'admin') {
			$this->_viewData->pagination = new \Emagid\Pagination("Admin", ['where'=>"id = {$this->_viewData->logged_admin->id}"]);
			$this->_viewData->admins = [$this->_viewData->logged_admin];
			$this->loadView($this->_viewData);
		} else {
			parent::index($params);
		}
	}
	
	public function update(Array $arr = []) {
		$adm = new \Model\Admin(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->all_admin_sections = Admin::$admin_sections_nested;
		$this->_viewData->admin_permissions = explode(",", $adm->permissions);
		parent::update($arr);
	}
	
	public function update_post() {
		if(isset($_POST['locations']) && $_POST['locations']){
			$_POST['locations'] = json_encode($_POST['locations']);
		}
		$hash_password = true;
		$password = "";

		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);
		
		if($id>0){
			$id = $_POST['id'];
			$admin_old = Admin::getItem($id);

			if($_POST['password'] == ""){
				$hash_password = false;
			}
		}else{
			$hash_password=true;
		}
		$admin = Admin::loadFromPost();
		
		if(isset($_POST['admin_section'])) {
			$permissions = implode(",", $_POST['admin_section']);
			unset($_POST['admin_section']);
		} else {
			$permissions = "";
		}
		$admin->permissions = trim($permissions, ',');

		if($hash_password) {
			$hash = Membership::hash($admin->password);
			$admin->password = $hash['password'];
			$admin->hash = $hash['salt'];
		}  else {
			$admin->password = $admin_old->password;
			$admin->hash = $admin_old->hash;
		}

		if ($admin->save()){
		 	$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and role_id = 1 and admin_id = '.$admin->id]);
		 	if (count($adminRoles) <= 0){
		 		$adminRole = new \Model\Admin_Roles();
		 		$adminRole->role_id = $_POST['role'];
		 		$adminRole->admin_id = $admin->id;
		 		$adminRole->save();
		 		$rolesNames = [\Model\Role::getItem(1)->name];
		 	} else {
		 		$rolesNames = [];
		 		foreach($adminRoles as $admin_role){
		 			$rolesNames[] = \Model\Role::getItem($admin_role->role_id)->name;
		 		}
		 	}

		 	if ($admin->id == \Emagid\Core\Membership::userId()){
		 		Membership::setAuthenticationSession($admin->id, $rolesNames, $admin);
		 	}

		 	$n = new \Notification\MessageHandler('Administrator saved.');
           	$_SESSION["notification"] = serialize($n);
		} else {
			$n = new \Notification\ErrorHandler($admin->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'administrators/update/'.$id);
		};
		
		redirect(ADMIN_URL.'administrators');
	}

	public function delete(Array $arr = []) {
      	$class = new $this->_model();
      	$class::delete($arr['id']);

      	$content = str_replace("\Model\\", "", $this->_model);
        $n = new \Notification\MessageHandler('Administrator deleted.');
       	$_SESSION["notification"] = serialize($n);

   		redirect(ADMIN_URL.'administrators');
    }


}
















































