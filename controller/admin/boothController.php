<?php

use Model\User;
use Model\Address;
use Model\Payment_Profile;

class boothController extends adminController {
  
	function __construct(){
		parent::__construct("Booth");
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
		$this->_viewData->booths = \Model\Booth::getList();
		$this->loadView($this->_viewData);
    }

    function update(Array $params = []){
		$boothId = isset($params['id'])?$params['id']:null;
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->boothCategories = array_map(function($item){
			return $item->category_id;
		}, \Model\BoothCategory::getList(['where' => ['booth_id' => $boothId]]));
		parent::update($params);
	}
	
    function update_post() {
		$obj = new $this->_model($_POST);

		foreach($_FILES as $fileType=>$file){
			if ($file['error'] == 0){
				if ($fileType == 'icon' || $fileType == 'image'){
					$image = new \Emagid\Image();
					$image->upload($_FILES[$fileType], UPLOAD_PATH.$this->_content.DS);
					$this->afterImageUpload($image);
					$obj->$fileType = $image->fileName;
					$obj->save();
				}
			}
		}

		if ($obj->save()){
			if($categories = $_POST['category']){
				$existing = array_map(function($item){
					return $item->category_id;
				}, \Model\BoothCategory::getList(['where' => ['booth_id' => $obj->id]]));

				$diff = array_diff($categories, $existing);

				foreach($diff as $categoryId){
					$new = new \Model\BoothCategory();
					$new->booth_id = $obj->id;
					$new->category_id = $categoryId;
					$new->save();
				}

				// delete
				$diff = array_diff($existing, $categories);

				foreach($diff as $categoryId){
					$delete = \Model\BoothCategory::getItem(null, ['where' => ['booth_id' => $obj->id, 'category_id' => $categoryId]]);
					if($delete){
						\Model\BoothCategory::delete($delete->id);
					}
				}


			}
			$content = str_replace("\Model\\", "", $this->_model);
			$content = str_replace('_', ' ', $content);
			$n = new \Notification\MessageHandler(ucwords($content).' saved.');
			$_SESSION["notification"] = serialize($n);
		} else {
			$n = new \Notification\ErrorHandler($obj->errors);
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
		}

		if (isset($_POST['redirectTo'])){
			redirect($_POST['redirectTo']);
		} else {
			redirect(ADMIN_URL.$this->_content);
		}
    }
}
































