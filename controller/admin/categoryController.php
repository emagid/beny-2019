<?php

class categoryController extends adminController {
	
	function __construct(){
		parent::__construct("Category");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;

		parent::index($params);
	}

	function update(Array $arr = []){
		$cat = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->categories = \Model\Category::getList(['where'=>"id<>".$cat->id." AND active = 1"]);

		parent::update($arr);
	}

	function sort($arr = []){
		$this->_viewData->categories = \Model\Category::getNested(0);
//		dd($this->_viewData->categories[1]->children[1]);
		$this->loadView($this->_viewData);
//		parent::index($arr);
	}

	function updateCategory(){
		$data = json_decode($_POST['data'])[0];
		$display = 1;
		foreach($data as $da){
			\Model\Category::updateRelationship($da->id,$da->children[0]);
			$category = \Model\Category::getItem($da->id);
			$category->parent_category = 0;
			$category->display_order = $display;
			$category->save();

			$display++;
		}
	}

	function afterObjSave($obj){
		if (!is_null($obj->banner) && $obj->banner != ""){
			$image = new \Emagid\Image();
			$image->fileName = $obj->banner;
			$image->fileType = explode(".", $image->fileName);
			$image->fileType = strtolower(array_pop($image->fileType));
			$image->fileDir = UPLOAD_PATH.'categories'.DS;
			$image->resize('1200_344'.$image->fileName, false, 1200, 344);
		}
	}

	public function delete(Array $arr = [])
	{
		$_GET['redirect'] = '/admin/category';
		parent::delete($arr);
	}
  
}