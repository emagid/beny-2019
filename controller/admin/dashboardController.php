<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct(){
		parent::__construct('Dashboard');
	}

	public function index(Array $params = []){
		$this->_viewData->new_orders = 0;

		$this->_viewData->recent_orders = 0;

		$this->_viewData->new_questions = 0;

		$this->_viewData->monthly_sales = [];

		$this->_viewData->monthly_sum = 0;
		foreach($this->_viewData->monthly_sales as $sale){
			$this->_viewData->monthly_sum += $sale->total;
		}

		$this->_viewData->recent_clients = \Model\User::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);
		foreach($this->_viewData->recent_clients as $client){
			$client->insert_time = new DateTime($client->insert_time);
			$client->insert_time = $client->insert_time->format('m-d-Y H:i:s');
		}

		$this->_viewData->page_title = 'Dashboard';
//		$this->setGraphsData();
		$this->loadView($this->_viewData);
	}

	private function setGraphsData(){
		$dataset = [
			'previous'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1))=>(object)[]
			],
			'current'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))=>(object)[],
				date("Y-m")=>(object)[]
			]
		];

		foreach($dataset as $period=>$set){
			foreach($set as $date=>$data){
				$year = explode('-', $date)[0];
				$month = explode('-', $date)[1];
				$dataset[$period][$date] = \Model\Order::getDashBoardData($year, $month);
			}
		}

		$this->_viewData->graphsData = $dataset;
	}
}










































