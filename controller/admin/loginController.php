<?php

use Emagid\Core\Membership;

/**
 * Base class to controll login
 */

class loginController extends \Emagid\Mvc\Controller {
	
	public function login(){
		$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
		if(\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['admin', 'venue'])){
			redirect('/admin/reporting');
		}

		$model = new \stdClass();
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}
	
	public function login_post()
	{	
		if (\Model\Admin::login($_POST['username'],$_POST['password'])){
			redirect(ADMIN_URL);
		}
 
		$model = new \stdClass();
		$model->errors = ['Invalid username or password'];
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}

	public function logout(){
		Membership::destroyAuthenticationSession();
		redirect(ADMIN_URL . 'login');
	}

}