<?php

use Model\User;
use Model\Address;
use Model\Payment_Profile;

class notationController extends adminController {
  
	function __construct(){
		parent::__construct("Notation");
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
		$this->_viewData->notations = \Model\Notation::getList();
		$this->loadView($this->_viewData);
    }

    function update(Array $params = []){
		parent::update($params);
	}
	
    function update_post() {
		$_POST['redirectTo'] = '/admin/notation';
		parent::update_post();
    }
}
































