<?php

use Model\User;
use Model\Address;
use Model\Payment_Profile;

class storeController extends adminController {
  
	function __construct(){
		parent::__construct("Store");
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
		$this->_viewData->stores = \Model\Store::getList();
		$this->loadView($this->_viewData);
    }

    function update(Array $params = []){
		$this->_viewData->notations = \Model\Notation::getList();
		parent::update($params);
	}
	
    function update_post() {
		$_POST['redirectTo'] = '/admin/store';
		parent::update_post();
    }
}
































