<?php

use Model\User;
use Model\Address;
use Model\Payment_Profile;

class usersController extends adminController {
  
	function __construct(){
		parent::__construct("User");
		if ($this->emagid->route['action'] == 'update'){
			$this->_viewData->page_title = 'Manage Customer';
		} else {
			$this->_viewData->page_title = 'Manage Customers';
		}
	}

	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;    
        
        parent::index($params);
    }

    function update(Array $params = []){
		$user = new $this->_model(isset($params['id'])?$params['id']:null);
		
		$this->_viewData->orders = \Model\Order::getList(['where'=>"user_id = ".$user->id, 'orderBy'=>'insert_time desc']);
		$this->_viewData->addresses = \Model\Address::getList(['where'=>"active = 1 and user_id = ".$user->id]);
		$this->_viewData->payment_profiles = \Model\Payment_Profile::getList(['where'=>"active = 1 and user_id = ".$user->id]);
		$this->_viewData->wishlist = \Model\Wishlist::getProductsByUserId($user->id);
		 
		foreach($this->_viewData->payment_profiles as $profile){
			$profile->cc_number = '****'.substr($profile->cc_number, -4);
		}

		parent::update($params);
	}
	
    function update_post() {
      	$hash_password = true;
      	$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);
      	$password = "";
      	if($id>0){
	        $id = $_POST['id'];
	        $user_exist = User::getItem($id);
	        if($_POST['password'] == ""){
	            $password = $user_exist->password;
	            $hash_password = false;
	        }
      	} else {
        	$hash_password=true;
      	}
      
      	$user = User::loadFromPost();
      
      	if($hash_password) {
	        $hash = \Emagid\Core\Membership::hash($user->password);
	
	        $user->password = $hash['password'];
	        $user->hash = $hash['salt'];
      	} else {
        	$user->password = $password;
      	}
      
      	if ($user->save()){
      		$n = new \Notification\MessageHandler('Customer Account saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users');
      	} else {
      		$n = new \Notification\ErrorHandler($user->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users/update/'.$id);
      	};
    }
  
    function address($params) {
	    $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    $address = null;
	    $user_id = filter_input(INPUT_GET,'user_id',FILTER_VALIDATE_INT);
	    $user = null;
	    if((int)$id>0) {
	      	$address = \Model\Address::getItem($id);
	      	if($address==null) {redirect(ADMIN_URL.'users');}
	      	$user = User::getItem($user_id);
	      	if($user==null) {
	        	redirect(ADMIN_URL.'users');
	      	}
	    } else {
	      	$user = \Model\User::getItem($user_id);
	      
	      	if($user==null) {
	        	redirect(ADMIN_URL.'users');
	      	}
	      	$address = new \Model\Address();
	      	$address->country = "United States";
	    }
	    
	    $this->_viewData->user = $user;
	    $this->_viewData->address = $address;
	    $this->_viewData->form = new \Emagid\Html\Form($address);
	   
	    if($address->id>0) {
	      	$this->_viewData->page_title = "Edit Address for ".$user->full_name(). " ";
	    } else {
	      	$this->_viewData->page_title = "New Address for ".$user->full_name(). " ";
	    }
	    
	    $this->_viewData->page_title .= link_to(ADMIN_URL.'users/update/'.$user->id,"<i class='fa fa-chevron-left'></i> Back",['class'=>'btn btn-default']);
	    $this->loadView($this->_viewData);
    }
  
    function address_post() {
		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);

		$address = \Model\Address::loadFromPost();
		
		if ($address->save()){
      		$n = new \Notification\MessageHandler('Address saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users/update/'.$address->user_id);
      	} else {
      		$n = new \Notification\ErrorHandler($address->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users/address/'.$id.'?user_id='.$address->user_id);
      	};
    }

    function address_delete($params) {
	    $address = \Model\Address::getItem((isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0);
	    $userId = $address->user_id;
	    \Model\Address::delete($address->id);
	    $n = new \Notification\MessageHandler('Address deleted.');
        $_SESSION["notification"] = serialize($n);
	    redirect(ADMIN_URL.'users/update/'.$userId);
    }
    
    function payment($params) {
	    $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
	    $payment_profile = null;
	    $user_id = filter_input(INPUT_GET,'user_id',FILTER_VALIDATE_INT);
	    
	    if((int)$id>0) {
	      $payment_profile = \Model\Payment_Profile::getItem($id);
	      if($payment_profile==null) {redirect(ADMIN_URL.'users');}
	      $user = \Model\User::getItem($user_id);
	      if($user==null) {
	        redirect(ADMIN_URL.'users');
	      }
	    } else {
	      $user = \Model\User::getItem($user_id);
	      
	      if($user==null) {
	        redirect(ADMIN_URL.'users');
	      }
	      $payment_profile = new \Model\Payment_Profile();
	      $payment_profile->country = "United States";
	     
	    }
	    
	    $this->_viewData->user = $user;
	    $this->_viewData->payment_profile = $payment_profile;
	    if (!is_null($this->_viewData->payment_profile->cc_number)){
	    	$this->_viewData->payment_profile->cc_number = '****'.substr($this->_viewData->payment_profile->cc_number, -4);
	    }
	    $this->_viewData->form = new \Emagid\Html\Form($payment_profile);
	   
	    if($payment_profile->id>0) {
	      $this->_viewData->page_title = "Edit Payment Method for ".$user->full_name(). " ";
	    } else {
	      $this->_viewData->page_title = "New Payment Method for ".$user->full_name(). " ";
	    }
	    
	    $this->_viewData->page_title .= link_to(ADMIN_URL.'users/update/'.$user->id,"<i class='fa fa-chevron-left'></i> Back",['class'=>'btn btn-default']);
	    $this->loadView($this->_viewData);
    }
  
    function payment_post() {
		$id = filter_input(INPUT_POST,'id',FILTER_VALIDATE_INT);
	    $user = null;
	    $user_id = filter_input(INPUT_POST,'user_id',FILTER_VALIDATE_INT);
	    if($user_id!==null && $user_id!==false && $user_id>0) {
	      $user = \Model\User::getItem($user_id);
	    }
	    if($user==null) {redirect(ADMIN_URL.'users');}
	    
	    if ($id > 0){
	    	$old = \Model\Payment_Profile::getItem($id);
	    } else {
	    	$old = new \Model\Payment_Profile();
	    }

	    $payment_profile = \Model\Payment_Profile::loadFromPost();
	    if (!is_null($old) && $payment_profile->cc_number == '****'.substr($old->cc_number, -4)){
	    	$payment_profile->cc_number = $old->cc_number;
	    }

	    if ($payment_profile->save()){
      		$n = new \Notification\MessageHandler('Payment Method saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users/update/'.$payment_profile->user_id);
      	} else {
      		$n = new \Notification\ErrorHandler($payment_profile->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'users/payment/'.$id.'?user_id='.$user_id);
      	};

	    redirect(ADMIN_URL.'users/update/'.$payment_profile->user_id);
    }

    function payment_delete($params) {
	    $payment = \Model\Payment_Profile::getItem((isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0);
	    $userId = $payment->user_id;
	    \Model\Payment_Profile::delete($payment->id);
	    $n = new \Notification\MessageHandler('Payment Method deleted.');
        $_SESSION["notification"] = serialize($n);
	    redirect(ADMIN_URL.'users/update/'.$userId);
    }
	function send_coupon() {
	    
	   $coupon_id= $_POST['coupon_id']; 
	   $user_id = $_POST['user_id']; 
	   $product_id	= $_POST['product_id'];

	   $user = \Model\User::getItem($user_id);
	   $coupon = \Model\Coupon::getItem($coupon_id);
	   $product = \Model\Product::getItem($product_id);
		if($coupon->type==1){
	   			$type = "$";
   		}else{
   			$type = "%";
   		}
		global $emagid;
		$emagid->email->from->email = 'noreply@itmbustbetime.com';

		$email = new \Emagid\Email();

		$email->addTo($user->email);
		$email->subject($type.$coupon->discount_amount.' off for '.$product->name);
		$email->body = "Hello, $user->last_name $user->first_name! Get $type$coupon->discount_amount off for $product->name! Just use this promocode:$coupon->code!";
		if($email->send()){
			echo "Email for $user->last_name $user->first_name whith $coupon->discount_amount% off for $product->name has been sent!";
		 }
    }
}
































