<?php

class brandsController extends siteController {

	public function brand(Array $params = []){
		$slug = (isset($params['brand_slug'])) ? $params['brand_slug'] : '';
		$this->viewData->brand  = \Model\Brand::getItem(null,['where'=>'brand.slug like \''.$slug.'\'']);

		$where = 'product.active = 1 AND availability!=0 AND product.brand = '.$this->viewData->brand->id;
		if (count($this->filters->where) > 0){
			$where = array_merge(explode('AND', $where), $this->filters->where);
			$where = implode(' AND ', $where);
		}

		$parameters = [
				'where'=>$where,
				'page_size'=>'12',
				'orderBy'=>'case availability when 1 then 1 when 2 then 2 when 0 then 3 end'];

		if(isset($this->filters->priceFilter) && $this->filters->priceFilter) {
			if (strtolower($this->filters->priceFilter) == 'lowtohigh') {
				$parameters['orderBy'] .= ',price ASC';
			} elseif (strtolower($this->filters->priceFilter) == 'hightolow') {
				$parameters['orderBy'] .= ',price DESC';
			}
		}
		$this->viewData->pagination = new \Emagid\Pagination('\Model\Product',$parameters);
		$this->viewData->products = $this->viewData->pagination->getList();

		if (!is_null($this->viewData->brand->meta_keywords) && $this->viewData->brand->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->brand->meta_keywords;
		}
		if (!is_null($this->viewData->brand->meta_description) && $this->viewData->brand->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->brand->meta_description;
		}
		if (!is_null($this->viewData->brand->meta_title) && $this->viewData->brand->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->brand->meta_title;
		}

		$this->loadView($this->viewData);
	}

}