<?php

class categoriesController extends siteController {
	
	public function index(Array $params = []){
		redirect(SITE_URL);
	}

	public function category(Array $params = []){
//		$slug = (isset($params['category_slug'])) ? $params['category_slug'] : '';
		if(isset($params['subcategory']) && $params['subcategory'] != ''){
			$slug = $params['subcategory'];
		} elseif(isset($params['category_slug']) && $params['category_slug'] != ''){
			$slug = $params['category_slug'];
		} else {
			$slug = '';
		}
		if ($slug == 'women-s' || $slug == 'men-s'){
			$this->viewData->category = new \Model\Category();
			$this->viewData->category->title = ($slug == 'women-s')?"Women's":"Men's";
			$this->viewData->category->slug = $slug;
			$where = 'product.gender = '.(($slug == 'women-s') ? 3 : 2);
		} else {
			$this->viewData->category = \Model\Category::getItem(null,['where'=>'category.active = 1 AND category.slug LIKE \''.$slug.'\'']);
			$where = 'product.id IN (SELECT pc.product_id FROM product_categories pc WHERE pc.category_id = '.$this->viewData->category->id.' and pc.active = 1 )';
		}

		if (count($this->filters->where) > 0){
			$where = array_merge(explode('AND', $where), $this->filters->where);
			$where = implode(' AND ', $where);
		}
		
		$this->viewData->pagination = new \Emagid\Pagination('\Model\Product',[
			'where'=>$where,
			'page_size'=>'12'
		]);		
		$this->viewData->products = $this->viewData->pagination->getList();

		if (!is_null($this->viewData->category->meta_keywords) && $this->viewData->category->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->category->meta_keywords;
		}
		if (!is_null($this->viewData->category->meta_description) && $this->viewData->category->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->category->meta_description;
		}
		if (!is_null($this->viewData->category->meta_title) && $this->viewData->category->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->category->meta_title;
		}
		
		$this->loadView($this->viewData);
	}

}