<?php

class directoryController extends siteController {
	
	public function index(Array $params = []){
		$booths = \Model\Booth::getList();
//		$this->viewData->boothArr = array_chunk($booths, floor(count($booths)/3));
		$this->viewData->categories = \Model\Category::getList();
		$categoryBooth = [];
		foreach($this->viewData->categories as $data){
			$name = str_replace(' ', '_', strtolower($data->name));
			$name = str_replace('-', '_', $name);
			$categoryBooth[$name] = [];
			$boothCategorys = \Model\BoothCategory::getList(['where' => ['category_id' => $data->id]]);
			foreach($boothCategorys as $bc){
				if($booth = \Model\Booth::getItem($bc->booth_id)){
					$categoryBooth[$name][] = $booth->number;
				}
			}
		}

		$boothIcon = [];

		foreach($booths as $booth){
			$name = $booth->name;
			$name = str_replace('\\', '', $name);
			$name = str_replace("'", '', $name);
			$icon = addslashes($booth->icon);
			if(!$icon){
				$icon = '/content/frontend/img/logo.png';
			} else {
				$icon = '/content/uploads/booths/'.$icon;
			}
			$boothIcon[] = ['number'=> $booth->number, 'icon' => $icon, 'name' => addslashes($name)];
		}

		$this->viewData->boothIcons = $boothIcon;
		$this->viewData->boothCategory = $categoryBooth;
		
		$this->loadView($this->viewData);
	}	

}