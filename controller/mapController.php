<?php

class mapController extends siteController {
	
	public function index(Array $params = []){
		$stores = \Model\Store::getList();

		$data = [];
		foreach($stores as $store){
			$notation = \Model\Notation::getItem($store->notation_id);
			$image = $store->image?'/content/uploads/stores/'.$store->image:null;
			$data[] = ['symbol' => strtolower($notation->name), 'name' => addslashes($store->name), 'description' => addslashes($store->description), 'image' => $image];
		}

		$this->viewData->sets = $data;
		
		$this->loadView($this->viewData);
	}	

}