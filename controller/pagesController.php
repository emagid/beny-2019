<?php

class pagesController extends siteController {

	public function index(Array $params = []) {
		redirect(SITE_URL);
	}

	public function page(Array $params = []) {
		$this->loadView($this->viewData);
	}

	public function hub(Array $params = []) {
		$this->loadView($this->viewData);
	}

	public function food(Array $params = []) {
		$this->loadView($this->viewData);
	}

	public function start(Array $params = []) {
		$this->loadView($this->viewData);
	}

	public function route(Array $params = []) {
		$this->loadView($this->viewData);
	}



}