<?php

class productsController extends siteController {

	public function index(Array $params = []){
		redirect(SITE_URL);
	}

	public function product(Array $params = []){
		$slug = (isset($params['product_slug'])) ? $params['product_slug'] : '';
		$this->viewData->product = \Model\Product::getItem(null,['where'=>'product.active = 1 AND product.slug LIKE \''.$slug.'\'']);
		$this->viewData->product_questions = \Model\Question::getList(['where'=>'product_id = '.$this->viewData->product->id." and question.status = '".\Model\Question::$status[1]."'"]);
		foreach($this->viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->viewData->brand_name = \Model\Brand::getItem($this->viewData->product->brand);
		$similar_products = [];
		$categories = \Model\Product_Category::get_product_categories($this->viewData->product->id, 10);
		foreach($categories as $category){
			$similar_products = array_merge($similar_products, \Model\Product_Category::get_category_products($category, 10, 'and product_id <> '.$this->viewData->product->id));
		}
		if(isset($_GET['alert'])){
			$code =$_GET['alert'];

			$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
			 
			if($new_price){
				if($new_price->product_id == $this->viewData->product->id){
					$_SESSION['price']=$new_price->price;
					$_SESSION['code']=$code;
					$_SESSION['id_product']=$new_price->product_id;
					
				}
				
			}
		}
		 
		$similar_products = array_merge($similar_products, \Model\Product::getList(['where'=>'brand = '.$this->viewData->product->brand.' and id <> '.$this->viewData->product->id, 'limit'=>10]));
		$similar_products = array_unique($similar_products, SORT_REGULAR);
		shuffle($similar_products);
		$similar_products = array_slice($similar_products, 0, 8);

		$this->viewData->similar_products = $similar_products;

		if (!is_null($this->viewData->product->meta_keywords) && $this->viewData->product->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->product->meta_keywords;
		}
		if (!is_null($this->viewData->product->meta_description) && $this->viewData->product->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->product->meta_description;
		}
		if (!is_null($this->viewData->product->meta_title) && $this->viewData->product->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->product->meta_title;
		}

		$this->viewData->hottestDeal = \Model\Hottest_Deal::getItem(null,['where'=>['product_id'=>$this->viewData->product->id]]);
		$this->loadView($this->viewData);
	}

	public function addQuestion(){
		$question = new \Model\Question($_POST);
		$product = \Model\Product::getItem($question->product_id);
		if($_SESSION['secpic']==$_POST['captcha']){

		$email = new \Emagid\Email();
		$question->insert_time = date("Y-m-d H:i:s");
		$question->viewed='FALSE'; 
		$question->save();
			
		//$email->addTo("order@modernvice.com");
		$email->addTo('questions@modernvice.com');//TODO st-dev questions email for MV
		$email->subject('You have the one new question!');
		$email->body .= '<p><a href="www.modernvice.com"><img src="http://beta.modernvice.com/content/frontend/img/logo.png" /></a></p><br />'
					   .'<p>You  have the new question:</p>';
		
		$email->body .= '<h2>';
		$email->body .='<a href="http://modernvice.com/product/'.$product->slug.'">'.$product->name.'</a>';
		$email->body .= '</h2>';
		$email->body .= '<p>Subject:'.$question->subject.'</p>';
		$email->body .= '<p>'.$question->text_question.'</p>';
		$email->body .= '<p>'.$question->reply_email.'</p>';
							
		$email->send();
		$n = new \Notification\MessageHandler('Thanks for your question! We will be shortly with you!');
           	$_SESSION["notification"] = serialize($n);
		}else{
			$n = new \Notification\ErrorHandler('Captcha is wrong!');
           	$_SESSION["notification"] = serialize($n);
		}
		

		redirect(SITE_URL.'product/'.$product->slug);
	}

	public function fastSearch(){
		$products = \Model\Product::search($_GET['keywords'], 5);

		echo '[';
		foreach($products as $key=>$product){

			$img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev default product image
            if(!is_null($product->featured_image) && $product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$product->featured_image)){ 
             	$img_path = UPLOAD_URL . 'products/' . $product->featured_image;
            }

			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "slug": "'.$product->slug.'", "featured_image": "'.$img_path.'", "price":"'.$product->price.'", "mpn":"'.$product->mpn.'" }';
			if ($key < (count($products)-1)){
				echo ",";
			}
		}
		echo ']';
	}

	public function search(){
		$this->viewData->keywords = $_GET['keywords'];
		$this->viewData->params['keywords'] = $this->viewData->keywords;

		$where = " lower(name) like '%".strtolower(urldecode($this->viewData->keywords))."%' ";

		if (count($this->filters->where) > 0){
			$where = array_merge(explode('AND', $where), $this->filters->where);
			$where = implode(' AND ', $where);
		}

		$this->viewData->pagination = new \Emagid\Pagination('\Model\Product',[
			'where'=>$where,
			'page_size'=>'12'
		]);		
		$this->viewData->products = $this->viewData->pagination->getList();

		$this->loadView($this->viewData);
	}

	 public function captcha(){
      	$this->template = false;
      $width = 100;                  //Øèðèíà èçîáðàæåíèÿ
 $height = 60;                  //Âûñîòà èçîáðàæåíèÿ
 $font_size = 17.5;   			//Ðàçìåð øðèôòà
 $let_amount = 4;               //Êîëè÷åñòâî ñèìâîëîâ, êîòîðûå íóæíî íàáðàòü
 $fon_let_amount = 30;          //Êîëè÷åñòâî ñèìâîëîâ, êîòîðûå íàõîäÿòñÿ íà ôîíå
 $path_fonts = 'fonts/';        //Ïóòü ê øðèôòàì
 
 
 $letters = array('a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','2','3','4','5','6','7','9');
 $colors = array('10','30','50','70','90','110','130','150','170','190','210');
 
 $src = imagecreatetruecolor($width,$height);
 $fon = imagecolorallocate($src,255,255,255);
 imagefill($src,0,0,$fon);
 
 $fonts = array();
 $dir=opendir($path_fonts);
 while($fontName = readdir($dir))
 {
   if($fontName != "." && $fontName != "..")
   {
     $fonts[] = $fontName;
   }
 }
 closedir($dir);
 
 for($i=0;$i<$fon_let_amount;$i++)
 {
   $color = imagecolorallocatealpha($src,rand(0,255),rand(0,255),rand(0,255),100); 
   $font = $path_fonts.$fonts[rand(0,sizeof($fonts)-1)];
   $letter = $letters[rand(0,sizeof($letters)-1)];
   $size = rand($font_size-2,$font_size+2);
   imagettftext($src,$size,rand(0,45),rand($width*0.1,$width-$width*0.1),rand($height*0.2,$height),$color,$font,$letter);
 }
 
 for($i=0;$i<$let_amount;$i++)
 {
   $color = imagecolorallocatealpha($src,$colors[rand(0,sizeof($colors)-1)],$colors[rand(0,sizeof($colors)-1)],$colors[rand(0,sizeof($colors)-1)],rand(20,40)); 
   $font = $path_fonts.$fonts[rand(0,sizeof($fonts)-1)];
   $letter = $letters[rand(0,sizeof($letters)-1)];
   $size = rand($font_size*2.1-2,$font_size*2.1+2);
   $x = ($i+1)*$font_size + rand(4,7);
   $y = (($height*2)/3) + rand(0,5);
   $cod[] = $letter;   
   imagettftext($src,$size,rand(0,15),$x,$y,$color,$font,$letter);
 }
 
 $_SESSION['secpic'] = implode('',$cod);
 
 header ("Content-type: image/gif"); 
 imagegif($src); 

	}

	 
}





































