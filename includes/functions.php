<?php

/**Format date and time functions*/
function db_to_time($datetime = ""){
	$unixdatetime = strtotime($datetime);
	$date_time = strftime("%m-%d-%Y | %I:%M %p", $unixdatetime);
	return strftime($date_time, '0 ');
		}//close db_to_time
		
		function db_to_date($datetime = ""){
			$unixdatetime = strtotime($datetime);
			return strftime("%B %d, %Y", $unixdatetime);
		}//close db_to_time
		
		function time_to_db($time=""){
			if($time == ""){$time = time();}
			$mysql_datetime = strftime("%Y-%m-%d %H:%M:%S", $time);
			return $mysql_datetime;
		}//close time to db
		
		function date_to_db($time=""){
			if($time == ""){$time = time();}
			$mysql_date = strftime("%Y-%m-%d", $time);
			return $mysql_date;
		}//close date to db
		
	/**
	* Function to calculate the same day one month in the future.
	*
	* This is necessary because some months don't have 29, 30, or 31 days. If the
	* next month doesn't have as many days as this month, the anniversary will be
	* moved up to the last day of the next month.
	*
	* @param $start_date (optional)
	*   UNIX timestamp of the date from which you'd like to start. If not given,
	*   will default to current time.
	*
	* @return $timestamp
	*   UNIX timestamp of the same day or last day of next month.
	*/
	function jjg_calculate_next_month($start_date = FALSE) {
		if ($start_date) {
		$now = $start_date; // Use supplied start date.
	} else {
		$now = time(); // Use current time.
	}
	
	  // Get the current month (as integer).
	$current_month = date('n', $now);
	
	  // If the we're in Dec (12), set current month to Jan (1), add 1 to year.
	if ($current_month == 12) {
		$next_month = 1;
		$plus_one_month = mktime(0, 0, 0, 1, date('d', $now), date('Y', $now) + 1);
	}
	  // Otherwise, add a month to the next month and calculate the date.
	else {
		$next_month = $current_month + 1;
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now), date('Y', $now));
	}
	
	$i = 1;
	  // Go back a day at a time until we get the last day next month.
	while (date('n', $plus_one_month) != $next_month) {
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now) - $i, date('Y', $now));
		$i++;
	}
	
	return $plus_one_month;
}

	/**
	* Function to calculate the same day one year in the future.
	*
	* @param $start_date (optional)
	*   UNIX timestamp of the date from which you'd like to start. If not given,
	*   will default to current time.
	*
	* @return $timestamp
	*   UNIX timestamp of the same day or last day of next month.
	*/
	function jjg_calculate_next_year($start_date = FALSE) {
		if ($start_date) {
		$now = $start_date; // Use supplied start date.
	} else {
		$now = time(); // Use current time.
	}
	$month = date('m', $now);
	$day = date('d', $now);
	$year = date('Y', $now) + 1;
	  $plus_one_year = strtotime("$year-$month-$day"); // Use ISO 8601 standard.
	  return $plus_one_year;
	}

	
	/**
	* Password hash with crypt
	* @param string $info
	* @param string $encdata 
	*/	
	function hasher($info, $encdata = false) { 
		$strength = "08";
				//check password
				//if encrypted data (the password in database) is passed, check it against input password ($info) 
		if ($encdata) { 
			if (substr($encdata, 0, 60) == crypt($info, "$2a$".$strength."$".substr($encdata, 60))) { 
				return true; 
			}else { 
				return false; 
			} 
		}else{
				//create new hashed password
				//make a salt and hash it with input password, and add salt to end 
			$salt = ""; 
			for ($i = 0; $i < 22; $i++) { 
				$salt .= substr("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", mt_rand(0, 63), 1); 
			} 
				//return 82 char string (60 char hash & 22 char salt) 
			return crypt($info, "$2a$".$strength."$".$salt).$salt; 
		} 
	}


	/**
	* limited output to desired num of words
	* @param string $content
	* @param number/int $num_words 
	*/	
	function limited_the_content($content, $num_words = 200){
		$stripped_content = strip_tags($content);
		$words = explode(' ', $stripped_content);
		if(count($words) < $num_words){
			echo $stripped_content;
		}else{
			$words = array_slice($words, 0, $num_words);
					//echo implode(' ', $words) . '...';
			echo implode(' ', $words);	
				}//end if
			}

	/**
	* output human readabel status
	* @param string/number $status
	*/	
	function get_status($status){

		switch ($status) {
			case 1:
			return "activated";
			break;
			case 0:
			return "deactivated";
			break;
			case 2:
			return "quick link";
			break;
			
			default:
					# code...
			break;
		}
	}
	
		/**
	* add a element to the beginning of the array
	* @param array $myarray
	* @param ***   $value
	* @param number/int... $position
	*/	
	function array_insert($myarray,$value,$position=0){
		$fore=($position==0)?array():array_splice($myarray,0,$position);
		$fore[]=$value;
		$ret=array_merge($fore,$myarray);
		return $ret;
	}		


/////////////////////Get countries and US states
	function get_states(){
		$states = array(
			'AL' => 'Alabama',
			'AK' => 'Alaska',
			'AZ' => 'Arizona',
			'AR' => 'Arkansas',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DE' => 'Delaware',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'HI' => 'Hawaii',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'IA' => 'Iowa',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'ME' => 'Maine',
			'MD' => 'Maryland',
			'MA' => 'Massachusetts',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MS' => 'Mississippi',
			'MO' => 'Missouri',
			'MT' => 'Montana',
			'NE' => 'Nebraska',
			'NV' => 'Nevada',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NY' => 'New York',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VT' => 'Vermont',
			'VA' => 'Virginia',
			'WA' => 'Washington',
			'WV' => 'West Virginia',
			'WI' => 'Wisconsin',
			'WY' => 'Wyoming',
			'DC' => 'District of Columbia',
			'GU' => 'Guam',
			'PR' => 'Puerto Rico',
			'VI' => 'Virgin Islands'
			);

return $states;
}

function get_countries(){				
	$countries = array(
		'United States',
		'United States Minor Outlying Islands',
		'Afghanistan',
		'Aland Islands',
		'Albania',
		'Algeria',
		'American Samoa',
		'Andorra',
		'Angola',
		'Anguilla',
		'Antarctica',
		'Antigua And Barbuda',
		'Argentina',
		'Armenia',
		'Aruba',
		'Australia',
		'Austria',
		'Azerbaijan',
		'Bahamas',
		'Bahrain',
		'Bangladesh',
		'Barbados',
		'Belarus',
		'Belgium',
		'Belize',
		'Benin',
		'Bermuda',
		'Bhutan',
		'Bolivia',
		'Bosnia And Herzegovina',
		'Botswana',
		'Bouvet Island',
		'Brazil',
		'British Indian Ocean Territory',
		'Brunei Darussalam',
		'Bulgaria',
		'Burkina Faso',
		'Burundi',
		'Cambodia',
		'Cameroon',
		'Canada',
		'Cape Verde',
		'Cayman Islands',
		'Central African Republic',
		'Chad',
		'Chile',
		'China',
		'Christmas Island',
		'Cocos (Keeling) Islands',
		'Colombia',
		'Comoros',
		'Congo',
		'Congo, The Democratic Republic Of The',
		'Cook Islands',
		'Costa Rica',
		'Cote D\'Ivoire',
		'Croatia',
		'Cuba',
		'Cyprus',
		'Czech Republic',
		'Denmark',
		'Djibouti',
		'Dominica',
		'Dominican Republic',
		'Ecuador',
		'Egypt',
		'El Salvador',
		'Equatorial Guinea',
		'Eritrea',
		'Estonia',
		'Ethiopia',
		'Falkland Islands (Malvinas)',
		'Faroe Islands',
		'Fiji',
		'Finland',
		'France',
		'French Guiana',
		'French Polynesia',
		'French Southern Territories',
		'Gabon',
		'Gambia',
		'Georgia',
		'Germany',
		'Ghana',
		'Gibraltar',
		'Greece',
		'Greenland',
		'Grenada',
		'Guadeloupe',
		'Guam',
		'Guatemala',
		'Guernsey',
		'Guinea',
		'Guinea-Bissau',
		'Guyana',
		'Haiti',
		'Heard Island And Mcdonald Islands',
		'Holy See (Vatican City State)',
		'Honduras',
		'Hong Kong',
		'Hungary',
		'Iceland',
		'India',
		'Indonesia',
		'Iran, Islamic Republic Of',
		'Iraq',
		'Ireland',
		'Isle Of Man',
		'Israel',
		'Italy',
		'Jamaica',
		'Japan',
		'Jersey',
		'Jordan',
		'Kazakhstan',
		'Kenya',
		'Kiribati',
		'Korea, Democratic People\'S Republic Of',
		'Korea, Republic Of',
		'Kuwait',
		'Kyrgyzstan',
		'Lao People\'S Democratic Republic',
		'Latvia',
		'Lebanon',
		'Lesotho',
		'Liberia',
		'Libyan Arab Jamahiriya',
		'Liechtenstein',
		'Lithuania',
		'Luxembourg',
		'Macao',
		'Macedonia, The Former Yugoslav Republic Of',
		'Madagascar',
		'Malawi',
		'Malaysia',
		'Maldives',
		'Mali',
		'Malta',
		'Marshall Islands',
		'Martinique',
		'Mauritania',
		'Mauritius',
		'Mayotte',
		'Mexico',
		'Micronesia, Federated States Of',
		'Moldova, Republic Of',
		'Monaco',
		'Mongolia',
		'Montserrat',
		'Morocco',
		'Mozambique',
		'Myanmar',
		'Namibia',
		'Nauru',
		'Nepal',
		'Netherlands',
		'Netherlands Antilles',
		'New Caledonia',
		'New Zealand',
		'Nicaragua',
		'Niger',
		'Nigeria',
		'Niue',
		'Norfolk Island',
		'Northern Mariana Islands',
		'Norway',
		'Oman',
		'Pakistan',
		'Palau',
		'Palestinian Territory, Occupied',
		'Panama',
		'Papua New Guinea',
		'Paraguay',
		'Peru',
		'Philippines',
		'Pitcairn',
		'Poland',
		'Portugal',
		'Puerto Rico',
		'Qatar',
		'Reunion',
		'Romania',
		'Russian Federation',
		'Rwanda',
		'Saint Helena',
		'Saint Kitts And Nevis',
		'Saint Lucia',
		'Saint Pierre And Miquelon',
		'Saint Vincent And The Grenadines',
		'Samoa',
		'San Marino',
		'Sao Tome And Principe',
		'Saudi Arabia',
		'Senegal',
		'Serbia And Montenegro',
		'Seychelles',
		'Sierra Leone',
		'Singapore',
		'Slovakia',
		'Slovenia',
		'Solomon Islands',
		'Somalia',
		'South Africa',
		'South Georgia And The South Sandwich Islands',
		'Spain',
		'Sri Lanka',
		'Sudan',
		'Suriname',
		'Svalbard And Jan Mayen',
		'Swaziland',
		'Sweden',
		'Switzerland',
		'Syrian Arab Republic',
		'Taiwan, Province Of China',
		'Tajikistan',
		'Tanzania, United Republic Of',
		'Thailand',
		'Timor-Leste',
		'Togo',
		'Tokelau',
		'Tonga',
		'Trinidad And Tobago',
		'Tunisia',
		'Turkey',
		'Turkmenistan',
		'Turks And Caicos Islands',
		'Tuvalu',
		'Uganda',
		'Ukraine',
		'United Arab Emirates',
		'United Kingdom',
		'Uruguay',
		'Uzbekistan',
		'Vanuatu',
		'Venezuela',
		'Viet Nam',
		'Virgin Islands, British',
		'Virgin Islands, U.S.',
		'Wallis And Futuna',
		'Western Sahara',
		'Yemen',
		'Zambia',
		'Zimbabwe'
		);

return $countries;
}
//close get countries and US states

function get_month(){
	$months = array(
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November',
		'12' => 'December'
		);


	return $months;
}
//close get month

function get_day(){
	$days = array();
	for($i=1; $i<=31; $i++){
		if(strlen($i) == 1){ $i = '0' . $i; }
		$days[$i] = $i;
	}
	return $days;
}

function get_year($min, $max){
	$years = range($min, $max);
	return $years;
}

function get_year_for_age(){
	$age_limit = date('Y') - 18;
	$years = range(1905, $age_limit);
	return $years;
}

function get_card_type(){
	$type = array(
		'1' => 'visa',
		'2' => 'mastercard',
		'3' => 'discover',
		'4' => 'amex'
		);

	return $type;
}
//close get month


/*
//string $filename = example.jpg
//string $folder = directory under UPLOAD_PATH
//int $width
//int $height
//CONSTANT definded in config.php
//define('UPLOAD_URL' , SITE_URL . 'content/upload/');
//define('UPLOAD_PATH' , __DIR__ . '\\content\\upload\\');
//define('MIN_URL' , SITE_URL . 'content/media/min/');
//define('MIN_PATH' , __DIR__ . '\\content\\media\\min\\');

//return string resized image url
*/
function resize_image($filename, $folder, $width, $height, $filetype = "jpg"){
	$ori_image_url = UPLOAD_PATH . '\\' . $folder . '\\' . $filename;
	$size = $width . '_' . $height;
	$res_image_url = MIN_PATH . $size . '\\' . $filename;

	$image_dir = dirname($res_image_url);
                //d(MIN_PATH);
                //d($image_dir);
                //d(is_dir($image_dir));

	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}
                //d($res_image_url);
                //d(file_exists($res_image_url));
	if(!file_exists($res_image_url)){
                     ////////
                    // ----------Image Resizing Function--------
		$target_file = $ori_image_url;
		$resized_file = $res_image_url;
		$wmax = $width;
		$hmax = $height;
		
		ak_img_resize($target_file, $resized_file, $wmax, $hmax, $filetype);
                    /////////
	}
	return MIN_URL . $size . '/' . $filename;
}//close resize_image


function excerpt($max_length, $value){
	if(strlen($value) > $max_length){
		$value = substr($value, 0, $max_length);
		$value .= "...";
	}
	return $value;
}

/*
	$table_name = string
	$culumns = array('culumn1', 'culumn2');
	$keywords = string, snow man falling from the sky
	$where = string, example1 = 1 AND example1 != 2 OR example3 LIKE '%hello%'
	$distinct = string, use as GROUP BY '{$distinct}'
	$primary_key = string, count($primary_key) as occurrences
	$search_mode= string "more" or "less" for more or less result
*/
	function search($table_name, $culumns, $keywords, $where="", $distinct="id", $primary_key="id", $order_by="id", $sort="ASC", $search_mode="more"){
		
		$keywords = trim(urldecode($keywords));
		
				////////////////////////////////////prepare keyword for search
		$string = "";
		$new_string = "";
		
		$string = $keywords;
				$string = strtolower(htmlentities(urldecode($string)));//make the keyword lowercase and filter the html tag out
				
					if(strlen($string) >= 2){//if the entered keyword is longer than 2 characater, do the search function below   //2
						
						$string = PorterStemmer::Stem($string);//removing the commoner morphological and inflexional endings from words in English
						$clean_string = new cleaner();
						$string = $clean_string->parseString($string);//remove the stupid words e.x me, I , do, what, and so on
						
					//if(strlen($string) >= 2){//2.5
						//if the clean string short than 2 chanracters, output nothing
						
								$split = explode(" ",$string);//split each word in the entered keyword and put them into an array
								//print_r($split);exit();
								//test the result of explode
								
								
								//filter out any word that less than 3 characters long in the array that created above
								//and make them a new string varibale
								foreach ($split as $array => $value) {//3
										if (strlen($value) >= 1) {//4
											$new_string .= ' '.$value.' ';
										  }//close4
										}//end the foreach loop //close 3
										
								$new_string=substr($new_string,0,(strLen($new_string)-1));//remove the last space " "
									//echo $new_string; exit();
								
								//make the new string variable to an array again, because we need the array function to do the slq
								$split_stemmed = explode(" ",$new_string);
								///////////orginal $sql = "SELECT DISTINCT COUNT(*) As occurrences, field1, field2, field3 FROM tablename WHERE (";
								
							//}//close 2.5
						}//close 2
						
						
						$sql = "SELECT *, COUNT({$primary_key}) As occurrences FROM {$table_name} ";
						
			//////////////////if senatized keyword is not shorter than 2 characters long, add keyword search query
						if(strlen($string) >= 2){
							$keyword_sql = "WHERE (";
								
								
							while(list($key,$val)=each($split_stemmed)){//5
									  if($val<>" " and strlen($val) > 0){//6
									  	$val = '%'. $val .'%';
									 // $val = '%'. $val;
									  	
									  	$keyword_sql .= "(";
									  		foreach($culumns as $culumn){
									  			$keyword_sql .= "{$culumn} LIKE '{$val}' OR ";
									  		}
										$keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-3));//this will eat the last OR
										if($search_mode == "more"){
											$keyword_sql .= ") OR ";
}elseif($search_mode == "less"){
	$keyword_sql .= ") AND";
}


									  }//close 6
							}//end the while loop //close5
							
									  $keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-4));//this will eat the last OR
									  //$sql_find_all_for_count = $sql . ") GROUP BY id";
									  if($where != ""){
									  	$keyword_sql .= ")  AND " . $where;  
}else{
	$keyword_sql .= ")";
}

$keyword_sql .= " GROUP BY {$distinct} ";  


$sql = $sql . $keyword_sql;
$sql .= "ORDER BY {$order_by} {$sort}";

			}else{//if search keyword is shorter than 2
				
				$keyword_sql = "WHERE ";
				$keyword_sql .= "(";
					foreach($culumns as $culumn){
						$keyword_sql .= "{$culumn} LIKE '%" . $string . "%' OR ";
					}
						$keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-3));//this will eat the last OR
						
						
						if($where != ""){
							$keyword_sql .= ") AND " . $where;  
}else{
	$keyword_sql .= ")";
}
$keyword_sql .= " GROUP BY {$distinct} ";  

$sql = $sql . $keyword_sql;
$sql .= "ORDER BY {$order_by} {$sort} LIMIT 30";
}
			//close keyword search query



		//dd($sql);
return $sql;

	}//close search function
	
	
	
	function params_string($params, $exception = array()){
		$url_var = ""; 
		if(is_array($params) && count($params)>0){
			foreach($params as $key => $val){
				if(!in_array($key, $exception)){
					$url_var .= $key . "/"  . $val . "/";
				}
			}
		}
		return $url_var;
	}

	function file_format($filename){
		$filename_array = explode(".", $filename);
		$format =  array_pop($filename_array);
		return $format;
	}

	function special_chars_clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}



	/*Resize image base on min length of the shorter side(width or height)*/
	function zy_img_resize($target, $newcopy, $min_len, $ext) {
		list($w_orig, $h_orig) = getimagesize($target);

		$change_scale = "";
		if($w_orig > $h_orig)
		{
			$h = $min_len;
			$change_scale = $h/$h_orig;
			$w = $w_orig*$change_scale;
		}else{
			$w = $min_len;
			$change_scale = $w/$w_orig;
			$h = $h_orig*$change_scale;
		}

		$img = "";
		$ext = strtolower($ext);
		if ($ext == "gif"){ 
			$img = imagecreatefromgif($target);
		} else if($ext =="png"){ 
			$img = imagecreatefrompng($target);
		} else { 
			$img = imagecreatefromjpeg($target);
		}
		$tci = imagecreatetruecolor($w, $h);
// imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
		imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		if ($ext == "gif"){ 
			imagegif($tci, $newcopy);
		} else if($ext =="png"){ 
			imagepng($tci, $newcopy);
		} else { 
			imagejpeg($tci, $newcopy, 84);
		}
	}
	

/*
    Check if the image already has re-sized. If yes, retrun the path. If not, generated one and return the path.
*/
    function resize_image_by_minlen($folder_name=NULL, $filename, $minlen, $filetype = "jpg"){
    	if($folder_name !== NULL)
    	{
    		$ori_image_url = $folder_name;//UPLOAD_PATH . $folder_name . DS . $filename;
    	}else{
    		$ori_image_url = UPLOAD_PATH . $filename;
    	}


    	$res_image_url = MIN_PATH . $minlen . DS . $filename;

    	$image_dir = dirname($res_image_url);

    	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}

    	if(!file_exists($res_image_url)){

// ----------Call Image Resizing Function--------
    		$target_file = $ori_image_url;
    		$resized_file = $res_image_url;

    		zy_img_resize($target_file, $resized_file, $minlen, $filetype);

    	}
    	return MIN_URL . $minlen . '/' . $filename;
}//close resize_image_by_minlen

/*After we have a resied image, we use this function to generate the crop version*/
function images_thumb($filename, $minlen, $width, $height, $filetype){
	$size = $width . '_' . $height;
	$target_file = MIN_PATH . $minlen . DS . $filename;
	$thumb_file = THUMB_PATH . $size . DS . $filename;

	$image_dir = dirname($thumb_file);

	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}
	if(!file_exists($thumb_file)){
		ak_img_thumb($target_file, $thumb_file, $width, $height, $filetype);
	}
	return THUMB_PATH . $size . DS . $filename;
}


	/**
     * prints out script tag
     * @param type $name: name of script, or if script is in subfolder, then put subfolder/path/to/script.js
     */
	function script($name,$full_path=FRONT_JS) {
		echo "<script type=\"text/javascript\" src=\"".$full_path."{$name}\"></script>";
	}
	
    /**
     * 
     * @param type $name
     * @param type $full_path: default path is in content/css. if css is not there then it can be overwritten
     */
    function css($name,$full_path = FRONT_CSS) {
    	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$full_path."{$name}\">";
    }
    
    function link_to($href,$text,$html_objects = ['class'=>"btn btn-primary btn-add"]) {
    	$attrs = "";
    	foreach($html_objects as $key=>$val) {
    		$attrs .= "{$key}='{$val}'";
    	}
    	return "<a href='{$href}' {$attrs}>{$text}</a>";
    }
    
    function filter_get() {
      $get_filter = array();
      foreach($_GET as $key=>$value) {
        $get_filter[$key] = filter_input(INPUT_GET,$key);
      }
      return $get_filter;
    }
    /**
     * function to save only for single image
     * @param type $file
     * @param type $obj
     * @param type $field_name
     * @param type $upload_path
     * @param type $sizes
     */
    function save_image($file,$obj,$field_name,$upload_path,$sizes) {
      if($file && !empty($file) && is_array($file) && $file['error'] != 4){//dd($file);
            
                    if($obj->{$field_name} != "" && file_exists($upload_path.$obj->{$field_name})){
                      unlink($upload_path . $obj->{$field_name});}

                    $tem_file_name = $file['tmp_name'];
                    $file_name = uniqid() . "_" . basename($file['name']);
                    $file_name = str_replace(' ', '_', $file_name);
                    $file_name = str_replace('-', '_', $file_name);
                    
                    
                    $allow_format = array('jpg', 'png', 'gif'); 
                    $filetype = explode("/", $file['type']); //$file['type'] has value like "image/jpeg"
                    $filetype = strtolower(array_pop($filetype));
                    if($filetype == 'jpeg'){$filetype = 'jpg';}
                    if(in_array($filetype, $allow_format)){
                      
                      $img_path = compress_image($tem_file_name, $upload_path . $file_name, 60,$file['size']);
                      if($img_path===false) {
                        move_uploaded_file($tem_file_name, $upload_path . $file_name);
                      }
                      
                        //move_uploaded_file($tem_file_name, $upload_path . $file_name);
                        $obj->{$field_name} = $file_name;
                        
                        foreach($sizes as $key=>$val) {
                        	if(is_array($val) && count($val)==2) {
                        		resize_image_by_minlen($upload_path, 
                        		$obj->{$field_name}, min($val), $filetype);

                    			$path = images_thumb($obj->{$field_name},min($val), 
                    				$val[0],$val[1], file_format($obj->{$field_name}));
                    			$image_size_str = implode('_',$val);
                    			copy($path, $upload_path.$image_size_str.$file_name);
                    		}
                		}
                		
                		$obj->save();
                    }
                
                     
        }//close upload featured_image
        
        
    }
    
        function compress_image($source_url, $destination_url, $quality,$size,$btsize=535396) {
		$info = getimagesize($source_url);
       if($size<$btsize) {return false;}
    		if ($info['mime'] == 'image/jpeg') {
        			$image = imagecreatefromjpeg($source_url);
                    imagejpeg($image, $destination_url, $quality);
          }

    		elseif ($info['mime'] == 'image/gif') {
        			$image = imagecreatefromgif($source_url);
                    imagegif($image, $destination_url, $quality);
            }

   		elseif ($info['mime'] == 'image/png') {
        			$image = imagecreatefrompng($source_url);
                    imagepng($image, $destination_url, $quality);
        }

    		//imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}
    
    function get_token() {
          if(isset($_SESSION['csrf_token'])) {
            return $_SESSION['csrf_token'];
          } else {
            $_SESSION['csrf_token'] = md5(uniqid(rand(),TRUE));
            return  $_SESSION['csrf_token'];
          }
        }