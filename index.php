<?php 
error_reporting(0);
date_default_timezone_set('America/New_York');
if (session_status() == PHP_SESSION_NONE) {
    \session_start();
}

// ini_set('display_errors', 1);


require_once(__DIR__."/libs/Emagid/emagid.php");
require_once("vendor/autoload.php");
require_once(__DIR__."/conf/emagid.conf.php");
require_once(__DIR__.'/includes/functions.php');
require_once(__DIR__.'/templates/notification_template.php');
require_once(__DIR__.'/libs/Carbon.php');
$emagid = new \Emagid\Emagid($emagid_config);

$emagid->loadMvc($site_routes);
