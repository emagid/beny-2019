<?php

namespace Emagid\Core;

/**
 * base class for DB access and modeling .
 */
abstract class Db
{

  /**
   * @var object - private db object, should be created once per instance of the encasing object.
   */
  private $db;

  function query($sql)
  {
    $db = $this->getConnection();

    $db->query($sql);
  }

  public function getDb()
  {
    return $this->db;
  }

  public function setDb($db)
  {
    $this->db = $db;
  }

}

class PdoConn extends Db
{

  /**
   * @var Array of error messages from the PDO object
   */
  public $errors = [];

  /**
   * Get or creates the db object
   * 
   * @return Object - the db object.
   */
  protected function getConnection()
  {
    if (isset($this->db) && $this->db)
      return $this->db; 


     global $emagid;
      $port_str =  '';

      // $charset = ';charset=utf8';
      $charset = '';


      if ($emagid && $emagid->connection_string)
      {
        if (isset($emagid->connection_string->driver)){
          $this->driver = $emagid->connection_string->driver;
        }

        if (isset($emagid->connection_string->port)){
            $port_str = ";port={$emagid->connection_string->port}";

        }


        if (isset($emagid->connection_string->charset)){
            $charset = ";port={$emagid->connection_string->port}";

        }

    }


    $this->db = new \PDO("{$this->driver}:host={$emagid->connection_string->host};dbname={$emagid->connection_string->db_name}{$port_str}{$charset}",
      $emagid->connection_string->username, $emagid->connection_string->password);

    return $this->db;
  }

  function buildWhere($where)
  {

    if (is_array($where))
    {

      $arr = [];

      foreach ($where as $key => $val)
      {
        array_push($arr, sprintf("(%s='%s')", $key, $val));
      }

      return implode(" AND ", $arr);
    }
    else
    {
      return $where;
    }
  }

  function getResults($sql)
  {
    return $this->execute($sql);
  }

  function execute($sql, $data = [] , $params = [])
  {

    $db = $this->getConnection();

    $sth = $db->prepare($sql);
    
    if (count($data))
    {
      $res = $sth->execute($data);
    }
    else
    {
      $res = $sth->execute();
    }

    $this->errors = $sth->errorInfo();

    return $sth->fetchAll(\PDO::FETCH_ASSOC);
    // return $res;
  }


  /**
  * returns last id after insert 
  */
  public function getLastId($seq_name = null ){
    $db = $this->getConnection();

    $id = $db->lastInsertId($seq_name);

    return $id;
  }

  function getVar($sql)
  {
    $db = $this->getConnection();
    $sth = $db->prepare($sql);

    $sth->execute();

    $result = $sth->fetchColumn();

    return $result;
  }



  public static function buildInsertStatement($table, $fields){
    $modified_keys = array_keys($fields);

    $keys_str = implode(',', $modified_keys);
    $vals_str = implode(',',
      array_map(function($item)
      {
        return ':' . $item;
      }, $modified_keys)
    );

    $sql = "INSERT INTO {$table} ({$keys_str}) VALUES($vals_str)";

    return $sql;


  }



  /**
  * Will look for the list of tables in the DB .
  *
  * @return Array 
  */
  public static function getTablesList(){
    global $emagid;

    $sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_schema,table_name;";
    $db = $emagid->getDb();

    $list = $db->execute($sql) ;

    $list = array_map(function($item){ return $item['table_name'] ; }, $list);


    return $list;
  }


}
