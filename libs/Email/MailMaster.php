<?php
namespace Email;
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/7/15
 * Time: 11:31 AM
 */
class MailMaster{
    private $mandrill;
    private $fromAddress;
    private $fromName;
    private $to;
    private $mergeTags;
    private $toEmail;
    private $subject;
    private $template;
    private $attachmentType;
    private $attachmentFile;
    private $attachmentName;

    private $defaultCC = [];
    public function __construct()
    {
        if(class_exists('\Mandrill')) {
            global $emagid;
            $this->mandrill = new \Mandrill($emagid->email->api_key);
        } else {
            throw new \Exception('Could not find Mandrill class. You may include it in the config array when instantiating Emagid;');
        }
    }

    /**
     * @param $fromName string
     * @return $this
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }


    /**
     * @param $fromAddress string
     * @return $this
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;
        return $this;
    }

    public function setAttachment($type, $fileName, $filePath)
    {
        $this->attachmentName = $fileName;
        $this->attachmentFile = $filePath;
        $this->attachmentType = $type;
        return $this;
    }

    /**
     * $to should be format as ['email' => 'abc@abc.com', 'name' => 'JANUS ZHOU', 'type' => 'to/cc/bcc']
     * @param array $to
     * @return $this
     * @throws \Exception
     */
    public function setTo(array $to)
    {
        if(!isset($to['email'])){
            throw new \Exception('Email required');
        }

        $this->to = $to;
        $this->toEmail = $to['email'];
        return $this;
    }

    /**
     * $mergeTags should be as ['FORGOT_PASSWORD_WORD_LINK' => 'link']
     * @param $mergeTags
     * @return $this
     */
    public function setMergeTags(array $mergeTags)
    {
        $this->mergeTags = $mergeTags;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function send()
    {
        $message = [
            'subject' => $this->subject,
            'from_email' => $this->fromAddress,
            'from_name' => $this->fromName ,
            'to' => array_merge([$this->to], $this->defaultCC),
            'headers' => array('Reply-To' => $this->fromAddress),
            'track_clicks' => true,
            'track_opens' => true,
            'merge_vars' => $this->formatMergeTags()
        ];

        if($this->attachmentFile && $this->attachmentType){
            $message['attachments'] = [['type' => $this->attachmentType, 'name' => $this->attachmentName, 'content' => base64_encode(file_get_contents($this->attachmentFile))]];
        }

        $async = false;

        $ipPool = 'Main Pool';
        try {
            return $this->mandrill->messages->sendTemplate($this->template, [], $message, $async, $ipPool);
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }

    }

    private function formatMergeTags()
    {
        $returnData = [];
        $data = [ 'rcpt' => '',  'vars' => []];
        $data['rcpt'] = $this->toEmail;

        if(!$this->mergeTags){
            return null;
        }

        foreach($this->mergeTags as $name => $tag){
            $data['vars'][] = ['name' => $name, 'content' => $tag];
        }

        $returnData[] = $data;

        foreach($this->defaultCC as $cc){
            $rcptArray = [];
            $rcptArray['rcpt'] = $cc['email'];
            $rcptArray['vars'] = $data['vars'];
            $returnData[] = $rcptArray;
        }

        return $returnData;
    }

}