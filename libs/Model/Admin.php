<?php 

namespace Model; 

class Admin extends \Emagid\Core\Model {
	
	static $tablename = 'administrator'; 

	//static $role = ['0'=>'custom', '1'=>'super', '2'=>'administrator', '3'=>'manager'];

	static $admin_sections =  ['Content','Pages', 'Administrators','Users','System', 'Newsletter', 'Configs', 'Booth', 'Category', 'Map', 'Notation', 'Store'];
	
	static $admin_sections_nested = [
		'Booth'=>[],
		'Category' => [],
		'Map' => [
			'Notation',
			'Store'
		],
		'Administrators'=>[]
	];
	
	public static $fields =  [
		'first_name'=>[
			'name'=>'First Name'
		], 
		'last_name'=>[
			'name'=>'Last Name'
		],
		'email'=>[
			'type'=>'email'
		],
		'username'=>[
			'required'=>true,
			'unique'=>true
		],
		'password'=>[
			'type'=>'password',
			'required'=>true,
		],
		'hash', 
		'permissions'
	];
	
	function full_name() {
		return $this->first_name.' ' .$this->last_name;
	}
	
	public static function login($username , $password){
		$list = self::getList(['where'=>"username = '{$username}'"]);
		if (count($list) > 0) {
			$admin = $list[0];
			$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );
			if ($password['password'] == $admin->password ) {

				$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and admin_id = '.$admin->id]);
				$rolesIds = [];
				foreach($adminRoles as $role){
					$rolesIds[] = $role->role_id;
				}
				$rolesIds = implode(',', $rolesIds);
				
				$roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
	    		$rolesNames = [];
				foreach($roles as $role){
					$rolesNames[] = $role->name;
				}

	    		\Emagid\Core\Membership::setAuthenticationSession($admin->id,$rolesNames, $admin);

				return true;
			}
		}
		return false;
	}
	
	public static function getNestedSections ($permissions = null){
		if (is_null($permissions)){
			return self::$admin_sections_nested;
		} else {
			$permissions = explode(',', $permissions);			
			$sections = self::$admin_sections_nested;
			$authorized = [];
			foreach ($sections as $parent=>$children){
				if (in_array($parent, $permissions)){
					$authorized[$parent] = [];
					foreach ($children as $child){
						if (in_array($child, $permissions)){
							array_push($authorized[$parent], $child);
						}
					}
				}
			}
			return $authorized;
		}
	}

	public function getRoleName()
	{
		$adminRole = Admin_Roles::getItem(null, ['where'=>"admin_id = {$this->id}"]);
		$role = Role::getItem($adminRole->role_id);
		return $role->name;
	}

	public static function getAllVenue()
	{
		$data = [];
		$brokerRole = Role::getItem(null, ['where' => "name = 'venue'"]);

		$adminRoles = Admin_Roles::getList(['where' => "role_id = {$brokerRole->id}"]);
		foreach($adminRoles as $role){
			$data[] = self::getItem($role->admin_id);
		}

		return $data;
	}

	public function lastLogin()
	{
		$sql = "select * from login_log where user_id = {$this->id} ORDER BY id desc limit 1";
		$login = LoginLog::getItem(null, ['sql' => $sql]);
		if($login){
			return $login->time;
		} else {
			return null;
		}

	}
}




