<?php 

namespace Model; 

class Booth extends \Emagid\Core\Model {

	static $tablename = 'booth'; 
	
	public static $fields =  [
		'name', 'number', 'icon'
	];
	
}