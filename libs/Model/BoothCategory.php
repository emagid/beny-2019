<?php 

namespace Model; 

class BoothCategory extends \Emagid\Core\Model {

	static $tablename = 'booth_category';
	
	public static $fields =  [
		'booth_id', 'category_id'
	];
	
}