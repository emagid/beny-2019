<?php

namespace Model;

class Category extends \Emagid\Core\Model {

  	static $tablename = "category";

  	public static $fields = [ 
	    'name',
	    'description',
	    'slug',
	    'parent_category',
	];
  	
	static $relationships = [
    ];

	public static function getNested($parentId){
		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} ", 'orderBy'=>"display_order"]);
		
		foreach($categories as $category){
			$category->children = self::getNested($category->id);
		};
			
		return $categories;
	}

	public function getParent(){
		if($this->parent_category != 0){
			return self::getItem($this->parent_category);
		} else {
			return '';
		}
	}

	public function getName(){
		return $this->alias != '' ? $this->alias: $this->name;
	}

	public function buildMenu(){
		$active = isset($this->emagid->route['category_slug']) && $this->emagid->route['category_slug'] == $this->slug? 'active': '';
		$dropdown = count($this->children) > 0?'dropdown':'';
		$init_menu = '<li class="'.$active.' '.$dropdown.'"><a class="dropdown-tiggle dropdown-toggle-special" role="button" aria-expanded="false" href="'.SITE_URL.'collections/'.$this->slug.'">' .$this->getName(). '</a>';
		$html = $init_menu;
		if(count($this->children) > 0) {
			$html .= '<ul class="dropdown-menu" role="menu">';
			foreach ($this->children as $child) {
				$html .= $this->buildChildMenu($child);
			}
			$html .= '</ul>';
		}
		$html .= '</li>';
		return $html;
	}

	function buildChildMenu($value){
		$parent = Category::getItem($value->parent_category);
		$html = '<li><a href="'.SITE_URL.'collections/'.$parent->slug.'/'.$value->slug.'">'.$value->getName().'</a>';
//		$html = '<ul class="dropdown-menu" role="menu"><li><a href="'.SITE_URL.'category/'.$value->slug.'">'.$value->getName().'</a>';
		if(count($value->children) > 0) {
			$html .= '<ul class="dropdown-menu" role="menu">';
			foreach($value->children as $child){
				$html .= $this->buildChildMenu($child);
			}
			$html .= '</ul>';
		}
		$html .= '</li>';
//		$html .= '</li></ul>';

		return $html;
	}

	public function buildSortable(){
		$html = '<li data-name="'.$this->name.'" data-id="'.$this->id.'">'.$this->name.'<ol>';
		if($this->children){
			foreach($this->children as $child){
				$html .= $this->buildChildSortable($child);
			}
		}
		$html .= '</ol></li>';
		return $html;
	}

	function buildChildSortable($child){
		$element = $child;
		$html = '<li data-name="'.$element->name.'" data-id="'.$element->id.'">'.$element->name.'<ol>';
		if($element->children){
			foreach($element->children as $c) {
				$html .= $this->buildChildSortable($c);
			}
		}
		$html .= '</ol></li>';
		return $html;
	}

	/*
	 * for category admin sortable
	 * */
	public static function updateRelationship($id,$children){
		$display = 1;
		foreach($children as $child){
			$c = self::getItem($child->id);
			$c->parent_category = $id;
			$c->display_order = $display;
			d($c);
			$c->save();
			if($child->children[0]){
				self::updateRelationship($child->id,$child->children[0]);
			}
			$display++;
		}
	}
}