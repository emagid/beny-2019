<?php

namespace Model;

class Newsletter extends \Emagid\Core\Model {
	static $tablename = "newsletter";
	public static $fields = [
		'email'=>['required'=>true, 'type'=>'email']
	];
}
