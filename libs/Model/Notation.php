<?php 

namespace Model; 

class Notation extends \Emagid\Core\Model {

	static $tablename = 'notation';
	
	public static $fields =  [
		'name'
	];
	
}