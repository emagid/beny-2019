<?php 

namespace Model; 

class Store extends \Emagid\Core\Model {

	static $tablename = 'store';
	
	public static $fields =  [
		'name', 'description', 'notation_id', 'image'
	];
	
}