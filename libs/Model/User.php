<?php

namespace Model;

class User extends \Emagid\Core\Model {
    static $tablename = "public.user";

    public static $fields  =  [
    	'email'=>['required'=>'true','unique'=>'true','type'=>'email'],
    	'password','hash',
    	'first_name'=>['required'=>'true', 'name'=>'First Name'],
    	'last_name'=>['required'=>'true', 'name'=>'Last Name'], 
    	 ];

    /**
    * concatenates first name and last name to create full name string and returns it
    * @return type: string of full name
    */
    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }

    /**
    * Verify login and create the authentication cookie / session 
    */
    public static function login($email , $password){
        $user = self::getList(['where'=>"email = '".$email."'"]);
        if (count($user)>0){
            $user = $user[0];
            $hash = \Emagid\Core\Membership::hash($password, $user->hash);            
            if ($hash['password'] == $user->password) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and user_id = '.$user->id]);
                $rolesIds = [];
                foreach($userRoles as $role){
                    $rolesIds[] = $role->role_id;
                }
                $rolesIds = implode(',', $rolesIds);

                $roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
                $rolesNames = [];
                foreach($roles as $role){
                    $rolesNames[] = $role->name;
                }

                \Emagid\Core\Membership::setAuthenticationSession($user->id, $rolesNames, $user);

                return true;
            } else {
            	$n = new \Notification\ErrorHandler('Incorrect email or password.');
	           	$_SESSION["notification"] = serialize($n);
            }
        } else {
        	$n = new \Notification\ErrorHandler('Email not found.');
	        $_SESSION["notification"] = serialize($n);
        }
    }
}
























