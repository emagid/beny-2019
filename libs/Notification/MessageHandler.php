<?php

namespace Notification;

class MessageHandler extends \Notification\NotificationsHandler
{
  /**
   * 
   * Construct the error object
   * 
   */
  public function __construct($message)
  {
    $this->message = $message;
    $this->buildHTML();
  }
  
  /**
   * 
   * Display the error
   * 
   */
  protected function buildHTML()
  {
    $this->html .= "<div class='alert alert-success'>";
    $this->html .= "<strong>Notification: </strong>";
    $this->html .= $this->message;
    $this->html .= "</div>";
  }
  
}
