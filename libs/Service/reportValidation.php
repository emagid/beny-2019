<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 11/16/16
 * Time: 8:32 PM
 */
namespace Service;

class ReportValidation {
    public static function totalAmountValidation($type, $value, $date){
        if((is_numeric($value) && floatval($value) <= 0) || $value == '-' || strtolower($value) == 'n/a'){
            // invalid total on report, send warning email
            \reportingController::sendReportingErrorEmail($type, $date);
            return false;
        }

        return true;
    }
}