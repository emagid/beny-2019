<?php 
use Emagid\Core\Membership,
Model\Users,
Model\Page,
Model\Category,
Model\Banner_items;

if(!Membership::isInRoles(['mag_user'])){
}else{
    $logged_user = Users::getUser();
}
$meta_title = "MetaTitle";
$meta_keyword = "keywords, keywords, keywords";
$meta_description = "MetaDescription";
global $emagid;
global $dynamic_meta_title;
global $dynamic_meta_description;

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<!--[if lte IE 9]>
<script type="text/javascript" src="<?=FRONT_JS?>js/ie8/html5shiv-printshiv.min.js"></script>
<script "text/javascript" src="<?=FRONT_JS?>js/ie8/respond.min.js"></script>
<![endif]-->

<head>
    <!-- META DATA -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $meta_title; ?></title>
    <meta name="Description" content="<?php echo $meta_description; ?>">
    <meta name="Keywords" content="<?php echo $meta_keyword; ?>">

    <meta property="og:title" content="<?=SITE_NAME?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=SITE_URL?>" />

    <!-- FAVICONS -->
    <!-- http://realfavicongenerator.net/ -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_URL?>favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=SITE_URL?>favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_URL?>favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=SITE_URL?>favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_URL?>favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=SITE_URL?>favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_URL?>favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=SITE_URL?>favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=SITE_URL?>favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="<?=SITE_URL?>favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=SITE_URL?>favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="<?=SITE_URL?>favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=SITE_URL?>favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?=SITE_URL?>favicons/manifest.json">
    <link rel="shortcut icon" href="<?=SITE_URL?>favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="msapplication-TileImage" content="<?=SITE_URL?>favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=SITE_URL?>favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!-- SITE CSS -->

    <?php
    if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='localhost') {
        define('WP_ENV', 'development');
    } else {
        define('WP_ENV', 'production');
    }
    if (WP_ENV == 'development') { ?>
    	<link rel="stylesheet" href="<?=FRONT_CSS?>main.css">
    <?php } else { ?>
    	<link rel="stylesheet" href="<?=FRONT_CSS?>main.min.css">
    <?php } ?>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=SITE_URL?>"><svg width="100" height="33">
                  <image xlink:href="<?=FRONT_IMG?>logo.svg" src="<?=FRONT_IMG?>logo.png" width="100" height="33" />
                  </svg>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li <?php if ($this->emagid->route['controller'] == 'brands') {echo 'class="active"';} ?>>
                        <a href="<?=SITE_URL?>brands">BRANDS</a>
                    </li>
                    <?php foreach($model->categories as $category) { ?>
                    <li <?php if (isset($this->emagid->route['page_name']) && $this->emagid->route['page_name'] == $category->slug)  {echo 'class="active"';} ?>>
						<a href='<?= SITE_URL.'category/'.$category->slug;?>'><?= $category->name;?></a>
					</li>
						<?php if (count($category->children) > 0){?>
						<ul>
							<?php foreach ($category->children as $child){?>
								<li><a href="<?=SITE_URL?>category/<?=$child->slug?>"><?php echo $child->name?></a></li>
							<?php }?>
						</ul>
						<?php }?>
					<?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-24">
                <?php $emagid->controller->renderBody($model); ?>
            </div>
        </div>
    </div>
    <footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-18">
					<ul class="list-inline list-unstyled">
						<li><a href="<?=SITE_URL?>brands">BRANDS</a></li>
						<!-- categories -->
						<?php foreach($model->categories as $category) { ?>
							<a href='<?= SITE_URL.'category/'.$category->slug;?>'><?= $category->name;?></a>
						<?php } ?>	  
						<!-- Static news link -->
						<li><a href="#">News</a></li>
						<!-- pages -->
						<?php foreach($model->pages as $page) { ?>
							<a href='<?= SITE_URL.'page/'.$page->slug;?>'><?= $page->title;?></a>
						<?php } ?>
						<!-- Static my account link -->
						<li><a href="#">My Account</a></li>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul class="list-inline list-unstyled social">
						<li><a href=""><i class="icon-facebook"></i></a></li>
						<li><a href=""><i class="icon-twitter"></i></a></li>
						<li><a href=""><i class="icon-instagramm"></i></a></li>
						<li><a href=""><i class="icon-gplus"></i></a></li>
					</ul>
					<p class="text-center">Powered by <a href="http://emagid.com" target="_blank">eMagid</a></p>
				</div>
			</div>
			<p>Copyright  2016  ModernVice INC.</p>
		</div>
	</footer>
    <?php function footer() { ?>
	    <script src="<?=FRONT_JS?>jquery.min.js"></script>
	    <?php
	    if (WP_ENV == 'development') { ?>
	    	<script src="<?=FRONT_JS?>main.js"></script>
	    <?php } else { ?>
	    	<script src="<?=FRONT_JS?>main.min.js"></script>
	    <?php } ?>
    <?php } ?>
</body>
</html>

<script type="text/javascript">
// google analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-52481123-1', 'auto');
ga('send', 'pageview');
</script>