<svg style='    
    position: absolute;
    z-index: 1000;
    top: 0;
    left: 0;' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3456 6264">
  <defs>
    <style>
      .cls-1, .cls-3, .cls-4, .cls-5 {
        fill: #fff;
      }

      .cls-1 {
        opacity: 0;
      }

      .cls-2 {
        fill: #6e7277;
      }

      .cls-3, .cls-5 {
        font-size: 20px;
      }

      .cls-3, .cls-4, .cls-5 {
        font-family: BikeSans-Semibold, Bike Sans;
        font-weight: 600;
      }

      .cls-4 {
        font-size: 21px;
      }

      .cls-5 {
        letter-spacing: -0.05em;
      }
    </style>
  </defs>
  <title>directory_icons</title>
  <g id="BACKGROUND">
    <rect class="cls-1" width="3456" height="6264"/>
  </g>
  <g id="GRAY_BOXES" data-name="GRAY BOXES">
    <g class='booth' data-name='Unlimited Biking'>
      <rect class="cls-2" x="1285.22" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1298.17 1950.94)">206</text>
    </g>
    <g class='booth' data-name='OneMotor'>
      <rect class="cls-2" x="1210.44" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1223.37 1950.94)">205</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1594.47" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1607.65 1448.93)">709</text>
    </g>
    <g class='booth' data-name='Taiwan Bicycle Association'>
      <rect class="cls-2" x="1594.47" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1607.65 1523.44)">609</text>
    </g>
    <g class='booth' data-img='aerotech' data-name='Aero Tech Designs'>
      <rect class="cls-2" x="1519.56" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1532.74 1448.93)">708</text>
    </g>
    <g class='booth' data-img='better' data-name='Better World Club'>
      <rect class="cls-2" x="1519.56" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1532.74 1523.44)">608</text>
    </g>
    <g class='booth' data-img='bff' data-name='BFF Pro'>
      <rect class="cls-2" x="1445.54" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1458.72 1448.93)">707</text>
    </g>
    <g class='booth' data-img='honey' data-name='Honey Stinger'>
      <rect class="cls-2" x="1445.54" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1458.72 1523.44)">607</text>
    </g>
    <g class='booth' data-img='aids' data-name='BRAKING AIDS® Ride/Wall Street Rides FAR'>
      <rect class="cls-2" x="1285.22" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1298.17 1448.93)">706</text>
    </g>
    <g class='booth' data-name='The Vitamin Shoppe'>
      <rect class="cls-2" x="1209.44" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1222.37 1448.93)">705</text>
    </g>
    <g class='booth' data-name='Nuu-Muu'>
      <rect class="cls-2" x="1285.22" y="1623.36" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1298.17 1663.88)">506</text>
    </g>
    <g class='booth' data-name='Skillion'>
      <rect class="cls-2" x="1210.44" y="1623.36" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1223.37 1663.88)">505</text>
    </g>
    <g class='booth' data-name='Visit Natural North Florida'>
      <rect class="cls-2" x="1285.22" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1298.17 1739.69)">406</text>
    </g>
    <g class='booth' data-img='enovative' data-name='Enovative Technologies, LLC'>
      <rect class="cls-2" x="1210.44" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1223.37 1739.69)">405</text>
    </g>
    <g class='booth' data-name="Wild Bill's Olde Fashioned Soda Pop">
      <rect class="cls-2" x="1285.22" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1298.17 1877.39)">306</text>
    </g>
    <g class='booth' data-name='Verizon Fios'>
      <rect class="cls-2" x="1210.44" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1223.37 1877.39)">305</text>
    </g>
  </g>
  <g id="New_Copy" data-name="New Copy">
    <g class='booth' data-name='SpiceRoads Cycle Tours'>
      <rect class="cls-2" x="1503.18" y="1063.82" width="60.13" height="60.13"/>
      <text class="cls-5" transform="translate(1505.48 1102.43)">1003</text>
    </g>
    <g class='booth' data-name='SLEEFS'>
      <rect class="cls-2" x="1569.52" y="1063.82" width="60.13" height="60.13"/>
      <text class="cls-5" transform="translate(1570.83 1102.43)">1004</text>
    </g>
    <g class='booth' data-img='brew' data-name='Brew Dr Kombucha'>
      <rect class="cls-2" x="1740.19" y="1063.82" width="79.84" height="60.13"/>
      <text class="cls-5" transform="translate(1752.61 1102.42)">1005</text>
    </g>
    <g class='booth' data-name='Po Campo'>
      <rect class="cls-2" x="2034.44" y="1063.82" width="81.45" height="60.13"/>
      <text class="cls-5" transform="translate(2049 1102.43)">1006</text>
    </g>
    <g class='booth' data-name='SassyCyclist'>
      <rect class="cls-2" x="2210.72" y="1063.82" width="82.61" height="60.13"/>
      <text class="cls-5" transform="translate(2223.31 1102.43)">1007</text>
    </g>
    <g class='booth' data-name='The Stick'>
      <rect class="cls-2" x="2408.01" y="1063.82" width="86.04" height="60.13"/>
      <text class="cls-5" transform="translate(2423.3 1102.43)">1008</text>
    </g>
    <g class='booth' data-img='epic' data-name='Epic Provisions'>
      <rect class="cls-2" x="1596.66" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1610.35 1739.69)">409</text>
    </g>
    <g class='booth' data-name='Québec du Sud'>
      <rect class="cls-2" x="1670.24" y="1699.99" width="142.7" height="67.34"/>
      <text class="cls-3" transform="translate(1716.89 1739.69)">410</text>
    </g>
    <g class='booth' data-name='Vélo Québec'>
      <rect class="cls-2" x="1819.08" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1832.01 1739.69)">412</text>
    </g>
    <g class='booth' data-img='chameleon' data-name='Chameleon Cold Brew'>
      <rect class="cls-2" x="1749.61" y="1625.27" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1764.73 1669.1)">511</text>
    </g>
    <g class='booth' data-name='Massaging Insoles The Original & Best!'>
      <rect class="cls-2" x="1822.35" y="1625.27" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1837.47 1669.1)">512</text>
    </g>
    <g class='booth' data-img='floravelo' data-name='FloraVelo'>
      <rect class="cls-2" x="1894.45" y="1625.27" width="141.16" height="67.34"/>
      <text class="cls-3" transform="translate(1946.25 1669)">513</text>
    </g>
    <g class='booth' data-img='okcycle' data-name='Ok Cycle Tours'>
      <rect class="cls-2" x="1447.72" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1462.42 1739.69)">407</text>
    </g>
    <g class='booth' data-img='harmless' data-name='Harmless Harvest'>
      <rect class="cls-2" x="1522.19" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1534.85 1739.69)">408</text>
    </g>
    <g class='booth' data-img='ash' data-name='AshChromics'>
      <rect class="cls-2" x="1522.19" y="1625.27" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1534.85 1669.1)">508</text>
    </g>
    <g class='booth' data-img='geico' data-name='GEICO'>
      <rect class="cls-2" x="1447.72" y="1625.27" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1462.42 1669.1)">507</text>
    </g>
    <g class='booth' data-name='Rockbros USA'>
      <rect class="cls-2" x="1447.72" y="1192.47" width="139.35" height="65.81"/>
      <text class="cls-3" transform="translate(1495.44 1231.41)">907</text>
    </g>
    <g class='booth' data-img='national' data-name='National MS Society - Bike MS'>
      <rect class="cls-2" x="1671.14" y="1191.71" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1684.09 1231.41)">910</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1745.61" y="1191.71" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1759.63 1231.41)">911</text>
    </g>
    <g class='booth' data-img='hospital' data-name='Hospital for Special Surgery'>
      <rect class="cls-2" x="1745.61" y="1264.26" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1759.63 1303.96)">811</text>
    </g>
    <g class='booth' data-name='Paragon Sports'>
      <rect class="cls-2" x="1822.59" y="1188.87" width="208.8" height="141.85"/>
      <text class="cls-3" transform="translate(1903.43 1267.95)">812</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1745.61" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1759.09 1523.44)">611</text>
    </g>
    <g class='booth' data-img='iq' data-name='iq technologies'>
      <rect class="cls-2" x="1820.08" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1833.56 1523.44)">612</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1670.14" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1683.09 1448.93)">710</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1670.14" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1683.09 1523.44)">610</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1745.61" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1759.09 1448.93)">711</text>
    </g>
    <g class='booth' data-img='light' data-name='Light & Motion'>
      <rect class="cls-2" x="1820.08" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1833.54 1448.93)">712</text>
    </g>
    <g class='booth' data-name='REI'>
      <polygon class="cls-2" points="1447.72 1836.31 1447.72 1910.83 1447.72 1917.35 1447.72 1978.16 1812.93 1978.16 1812.93 1917.35 1812.93 1910.83 1812.93 1836.31 1447.72 1836.31"/>
      <text class="cls-3" transform="translate(1614.35 1912.22)">207</text>
    </g>
    <g class='booth' data-img='bolle' data-name='Bolle'>
      <rect class="cls-2" x="2365.1" y="2105.96" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2376.55 2145.67)">105</text>
    </g>
    <g class='booth' data-name='Oakley'>
      <rect class="cls-2" x="2217.23" y="2105.96" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2229.08 2145.67)">103</text>
    </g>
    <g class='booth' data-img='gore' data-name='Gore Wear'>
      <rect class="cls-2" x="2291.12" y="2105.96" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2302.97 2145.67)">104</text>
    </g>
    <g class='booth' data-img='kryptonite' data-name='Kryptonite'>
      <rect class="cls-2" x="2069.11" y="2105.96" width="141.81" height="67.34"/>
      <text class="cls-3" transform="translate(2123.96 2145.67)">101</text>
    </g>
    <g class='booth' data-img='clif' data-name='CLIF Bar & Company'>
      <rect class="cls-2" x="2270.15" y="1408.53" width="149.58" height="141.85"/>
      <text class="cls-3" transform="translate(2331.97 1485.82)">615</text>
    </g>
    <g class='booth' data-name='PRIMAL'>
      <rect class="cls-2" x="2428.55" y="1408.53" width="56.64" height="141.85"/>
      <rect class="cls-2" x="2428.55" y="1408.36" width="67.34" height="141.85"/>
      <text class="cls-3" transform="translate(2441.94 1485.82)">617</text>
    </g>
    <g class='booth' data-name='NuunHydration'>
      <rect class="cls-2" x="2268.34" y="1699.99" width="125" height="67.34"/>
      <text class="cls-3" transform="translate(2314.12 1739.69)">415</text>
    </g>
    <g class='booth' data-img='manhattan' data-name='Manhattan Portage'>
      <rect class="cls-2" x="2428.04" y="1190.02" width="67.34" height="141.85"/>
      <text class="cls-3" transform="translate(2444.78 1265.31)">817</text>
    </g>
    <g class='booth' data-img='league' data-name='League of American Bicyclists'>
      <rect class="cls-2" x="2355.17" y="1191.71" width="65.88" height="67.34"/>
      <text class="cls-3" transform="translate(2374.52 1231.41)">916</text>
    </g>
    <g class='booth' data-img='kmc' data-name='KMC Chain'>
      <rect class="cls-2" x="2279.63" y="1191.71" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2295.74 1231.41)">915</text>
    </g>
    <g class='booth' data-img='bosch' data-name='Bosch eBike Systems'>
      <rect class="cls-2" x="2279.2" y="1264.26" width="141.85" height="67.34"/>
      <text class="cls-3" transform="translate(2336.68 1303.96)">815</text>
    </g>
    <g class='booth' data-img='bikerent' data-name='Bike Rent NYC'>
      <rect class="cls-2" x="2508.87" y="1625.91" width="125" height="67.34"/>
      <text class="cls-3" transform="translate(2553.79 1667)">518</text>
    </g>
    <g class='booth' data-name='ZOIC Clothing'>
      <rect class="cls-2" x="2435.99" y="1625.66" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2450.04 1667.23)">517</text>
    </g>
    <g class='booth' data-img='bikeflights' data-name='Bike Flights'>
      <rect class="cls-2" x="2618.49" y="1699.99" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2629.93 1739.69)">420</text>
    </g>
    <g class='booth' data-img='garneau' data-name='Garneau USA'>
      <rect class="cls-2" x="2691.67" y="1624.65" width="56.64" height="141.85"/>
      <rect class="cls-2" x="2691.67" y="1624.65" width="67.34" height="141.85"/>
      <text class="cls-3" transform="translate(2706.22 1702.37)">421</text>
    </g>
    <g class='booth' data-img='forto' data-name='FORTO Coffee'>
      <rect class="cls-2" x="2490.15" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2503.22 1877.39)">318</text>
    </g>
    <g class='booth' data-name='Priority Bicycles'>
      <rect class="cls-2" x="2564.63" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2580.91 1950.94)">219</text>
    </g>
    <g class='booth' data-img='batch' data-name='Batch Bicycles'>
      <rect class="cls-2" x="2639.09" y="1911.23" width="109" height="67.34"/>
      <text class="cls-3" transform="translate(2672.18 1950.94)">220</text>
    </g>
    <g class='booth' data-name='New York Bike Lawyers'>
      <rect class="cls-2" x="2564.61" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2580.38 1877.39)">319</text>
    </g>
    <g class='booth' data-img='bourbon' data-name='Bourbon Country Burn'>
      <rect class="cls-2" x="2415.67" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2430.47 1877.39)">317</text>
    </g>
    <g class='booth' data-name='TDA Global Cycling'>
      <rect class="cls-2" x="2490.15" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2503.22 1950.94)">218</text>
    </g>
    <g class='booth' data-name='Ortlieb'>
      <rect class="cls-2" x="2415.67" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2430.47 1950.94)">217</text>
    </g>
    <g class='booth' data-img='addmotor' data-name='Addmotor Inc.'>
      <rect class="cls-2" x="2266.72" y="1837.87" width="140.41" height="66.97"/>
      <text class="cls-3" transform="translate(2319.84 1877.39)">315</text>
    </g>
    <g class='booth' data-name='Nutcase'>
      <rect class="cls-2" x="2266.72" y="1911.41" width="67.34" height="66.97"/>
      <text class="cls-3" transform="translate(2281.2 1950.94)">215</text>
    </g>
    <g class='booth' data-img='giordana' data-name='Giordana'>
      <rect class="cls-2" x="2340.92" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2361.15 1950.94)">216</text>
    </g>
    <g class='booth' data-name='Selle Anatomica'>
      <rect class="cls-2" x="1820.08" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1835.91 1877.39)">312</text>
    </g>
    <g class='booth' data-img='dcbike' data-name='DC Bike Ride'>
      <rect class="cls-2" x="1894.55" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1909.11 1877.39)">313</text>
    </g>
    <g class='booth' data-img='cleverhood' data-name='Cleverhood'>
      <rect class="cls-2" x="1969.99" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1985.05 1950.94)">214</text>
    </g>
    <g class='booth' data-img='danny' data-name="Danny's Cycles">
      <rect class="cls-2" x="1894.55" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1909.62 1950.94)">213</text>
    </g>
    <g class='booth' data-img='flatbike' data-name='Flatbike, Inc.'>
      <rect class="cls-2" x="1819.08" y="1911.23" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1834.15 1950.94)">212</text>
    </g>
    <g class='booth' data-img='lumos' data-name='Lumos Helmet'>
      <rect class="cls-2" x="1969.03" y="1837.69" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1983.58 1877.39)">314</text>
    </g>
    <g class='booth' data-name='Walz Caps, Inc'>
      <rect class="cls-2" x="1598.66" y="1625.27" width="91.09" height="67.34"/>
      <text class="cls-3" transform="translate(1627.35 1667.66)">509</text>
    </g>
    <g class='booth' data-img='benm' data-name='Bike New York Membership'>
      <rect class="cls-2" x="1598.24" y="1191.71" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1612.26 1231.41)">909</text>
    </g>
    <g class='booth' data-name='Taiwan Tourism Bureau'>
      <rect class="cls-2" x="1597.26" y="1264.26" width="141.21" height="67.34"/>
      <text class="cls-3" transform="translate(1649.6 1303.96)">809</text>
    </g>
    <g class='booth' data-name='Velosock'>
      <rect class="cls-2" x="1522.79" y="1264.26" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1535.12 1303.96)">808</text>
    </g>
    <g class='booth' data-img='bragging' data-name='Bragging Rights'>
      <rect class="cls-2" x="1968.7" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1982.16 1448.93)">714</text>
    </g>
    <g class='booth' data-img='abus' data-name='ABUS Mobile Security, Inc.'>
      <rect class="cls-2" x="1894.23" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1909.62 1448.93)">713</text>
    </g>
    <g class='booth' data-img='bikase' data-name='BiKase'>
      <rect class="cls-2" x="1968.73" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1982.18 1523.44)">614</text>
    </g>
    <g class='booth' data-name='Sojourn Bicycling & Active Vacations'>
      <rect class="cls-2" x="1894.25" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1907.5 1523.44)">613</text>
    </g>
    <g class='booth' data-img='killington' data-name='Killington Bike Park'>
      <rect class="cls-2" x="2580.69" y="1479.46" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2598.11 1521.26)">618</text>
    </g>
    <g class='booth' data-img='lasik' data-name='LASIK Vision Institute'>
      <rect class="cls-2" x="2652.13" y="1479.46" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2666.68 1521.26)">619</text>
    </g>
    <path class="cls-2" d="M2349.09,1603.81"/>
    <path class="cls-2" d="M2281.75,1603.81"/>
    <g class='booth' data-name='Tannus America'>
      <rect class="cls-2" x="2266.89" y="1625.66" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2288.19 1667.33)">515</text>
    </g>
    <g class='booth' data-img='cyclecause' data-name='Cycle for the Cause'>
      <rect class="cls-2" x="2339.15" y="1625.66" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2352.92 1667.33)">516</text>
    </g>
    <g class='booth' data-name='Pain Pod USA'>
      <rect class="cls-2" x="2399.2" y="1700.17" width="67.34" height="66.97"/>
      <text class="cls-3" transform="translate(2415.7 1739.69)">417</text>
    </g>
    <g class='booth' data-name='New York City Triathlon'>
      <rect class="cls-2" x="2471.56" y="1700.17" width="67.34" height="66.97"/>
      <text class="cls-3" transform="translate(2485.7 1739.69)">418</text>
    </g>
    <g class='booth' data-name='New York State Tourism Industry Assc.'>
      <rect class="cls-2" x="2545.32" y="1699.99" width="67" height="67.34"/>
      <text class="cls-3" transform="translate(2560.6 1739.69)">419</text>
    </g>
    <g class='booth' data-img='citibike' data-name='Citi Bike'>
      <rect class="cls-2" x="2639.08" y="1837.69" width="109" height="67.34"/>
      <text class="cls-3" transform="translate(2672.18 1877.39)">320</text>
    </g>
    <g class='booth' data-img='fiveborough' data-name='Five Borough Bicycle Club'>
      <rect class="cls-2" x="2439.58" y="2105.96" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(2451.01 2145.67)">106</text>
    </g>
    <g class='booth' data-name='Tourisme Laurentides'>
      <rect class="cls-2" x="1892.95" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1908.5 1739.69)">413</text>
    </g>
    <g class='booth' data-name='Pinnacle Performance Eyewear'>
      <rect class="cls-2" x="1966.42" y="1699.99" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1981.94 1739.69)">414</text>
    </g>
    <g class='booth' data-img='greenmountain' data-name='Green Mountain Energy'>
      <rect class="cls-2" x="1447.72" y="1264.26" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1460.51 1303.96)">807</text>
    </g>
    <g class='booth' data-name=''>
      <rect class="cls-2" x="1211.02" y="1189.17" width="67.34" height="141.85"/>
      <text class="cls-3" transform="translate(1223.56 1265.26)">905</text>
    </g>
    <g class='booth' data-img='illuminite' data-name='illumiNITE'>
      <rect class="cls-2" x="1285.22" y="1189.17" width="67.34" height="141.85"/>
      <text class="cls-3" transform="translate(1297.75 1265.26)">906</text>
    </g>
    <g class='booth' data-name='SoloRock'>
      <rect class="cls-2" x="1245.39" y="1063.82" width="60.13" height="60.13"/>
      <text class="cls-5" transform="translate(1247.12 1102.43)">1001</text>
    </g>
    <g class='booth' data-img='empire' data-name='Empire State Ride To End Cancer'>
      <rect class="cls-2" x="1309.59" y="1063.82" width="60.13" height="60.13"/>
      <text class="cls-5" transform="translate(1312.02 1102.43)">1002</text>
    </g>
    <g class='booth' data-name="Velocity: Columbia's Ride to End Cancer">
      <rect class="cls-2" x="1135.4" y="1483.42" width="67.34" height="67.34"/>
      <text class="cls-4" transform="translate(1149.3 1523.44)">604</text>
    </g>
    <g class='booth' data-img='brilliant' data-name='Brilliant Reflective'>
      <rect class="cls-2" x="1209.5" y="1483.42" width="143.05" height="67.34"/>
      <text class="cls-4" transform="translate(1257.59 1523.44)">605</text>
    </g>
    <g class='booth' data-name='TD Bank'>
      <rect class="cls-2" x="1135.4" y="1409.22" width="67.34" height="67.34"/>
      <text class="cls-3" transform="translate(1149.3 1448.93)">704</text>
    </g>
  </g>
</svg>
