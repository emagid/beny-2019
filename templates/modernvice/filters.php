<?
	$uri = $this->emagid->uri;
	if (!strrpos($this->emagid->uri, '?')){
		$uri .= '?';
	} else {
		$uri .= '&';
	}
?>
<!-- Filters -->
<div class="container">
	<div class="filters">
		<ul class="nav nav-pills" role="tablist">

			<?/* if ($this->view != 'brand') { */?><!--
				<li role="presentation" class="dropdown">
					<?/* if (isset($model->filters->where['brand'])) { */?>
						<?/*
							$uriAux = preg_replace('/&?(brand='.$model->filters->brand.')/', '', $uri);
							$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
							$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
						*/?>
						<a href="<?/*=$uriAux*/?>">Brand - <?/*=$model->filters->brand*/?> - x</a>
					<?/* } else { */?>
						<a id="filter-brand" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
							Brand
							<span class="caret"></span>
						</a>
						<ul id="filter-brand" class="dropdown-menu" role="menu" aria-labelledby="filter-brand">
							<?/* foreach($model->brands as $brand) { */?>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="<?/*=$uri.'brand='.$brand->slug*/?>"><?/*=$brand->name*/?></a></li>
							<?/* } */?>
						</ul>
					<?/* } */?>
				</li>
			--><?/* } */?>

			<? if (!isset($this->emagid->route['category_slug']) || ($this->emagid->route['category_slug'] != 'men-s' && $this->emagid->route['category_slug'] != 'women-s')) { ?>
			<li role="presentation" class="dropdown">
				<? if (isset($model->filters->where['gender'])) { ?>
					<?
						$uriAux = preg_replace('/&?(gender='.$model->filters->gender.')/', '', $uri);
						$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
						$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
					?>
					<a href="<?=$uriAux?>">Gender - <?=$model->filters->gender?> - x</a>
				<? } else if(!empty($model->filters->genders)){ ?>
					<a id="filter-gender" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
						Gender
						<span class="caret"></span>
					</a>
					<ul id="filter-gender" class="dropdown-menu" role="menu" aria-labelledby="filter-gender">
						<? foreach($model->filters->genders as $gender=>$id) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'gender='.$gender?>"><?=$gender?></a></li>
						<? } ?>
					</ul>
				<? } else {}?>
			</li>
			<? } ?>
			
			<li role="presentation" class="dropdown">
				<? if (isset($model->filters->where['color'])) { ?>
					<?
						$uriAux = preg_replace('/&?(color='.$model->filters->color.')/', '', $uri);
						$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
						$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
					?>
					<a href="<?=$uriAux?>">Color - <?=$model->filters->color?> - x</a>
				<? } else if(!empty($model->filters->colors)){ ?>
					<a id="filter-color" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
						Color
						<span class="caret"></span>
					</a>
					<ul id="filter-color" class="dropdown-menu" role="menu" aria-labelledby="filter-color">
						<? foreach($model->filters->colors as $color) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'color='.$color->slug?>"><?=$color->name?></a></li>
						<? } ?>
					</ul>
				<? } else {} ?>
			</li>

			<li role="presentation" class="dropdown">
				<? if (isset($model->filters->where['material'])) { ?>
					<?
						$uriAux = preg_replace('/&?(material='.$model->filters->material.')/', '', $uri);
						$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
						$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
					?>
					<a href="<?=$uriAux?>">Material - <?=$model->filters->material?> - x</a>
				<? } else if(!empty($model->filters->materials)){ ?>
					<a id="filter-material" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
						Material
						<span class="caret"></span>
					</a>
					<ul id="filter-material" class="dropdown-menu" role="menu" aria-labelledby="filter-material">
						<? foreach($model->filters->materials as $material) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'material='.$material->slug?>"><?=$material->name?></a></li>
						<? } ?>
					</ul>
				<? } else {}?>
			</li>

			<li role="presentation" class="dropdown">
				<? if (isset($model->filters->where['collection'])) { ?>
					<?
						$uriAux = preg_replace('/&?(collection='.$model->filters->collection.')/', '', $uri);
						$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
						$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
					?>
					<a href="<?=$uriAux?>">Collection - <?=$model->filters->collection?> - x</a>	
				<? } else if(!empty($model->filters->collections)){ ?>
					<a id="filter-collection" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
						Collections
						<span class="caret"></span>
					</a>
					<ul id="filter-collection" class="dropdown-menu" role="menu" aria-labelledby="filter-collection">
						<? foreach($model->filters->collections as $collection) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'collection='.$collection->slug?>"><?=$collection->name?></a></li>
						<? } ?>
					</ul>
				<? } else {} ?>
			</li>

			<li role="presentation" class="dropdown">
				<? if (isset($model->filters->where['price'])) { ?>
					<?
						$uriAux = preg_replace('/&?(price='.$model->filters->price.')/', '', $uri);
						$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
						$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);
					?>
					<a href="<?=$uriAux?>">Price - <?=$model->filters->price?> - x</a>
				<? } else if(!empty($model->filters->price)){ ?>
					<a id="filter-collection" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
						Price
						<span class="caret"></span>
					</a>
					<ul id="filter-collection" class="dropdown-menu" role="menu" aria-labelledby="filter-collection">
						<? foreach($model->filters->price as $price) { ?>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'price='.$price?>">Under <?=$price?></a></li>
						<? } ?>
					</ul>
				<? } ?>
			</li>

			<li role="presentation" class="dropdown">
				<?if(isset($_GET['filter']) && $_GET['filter'] != ""){
					$uriAux = preg_replace('/&?(filter='.$_GET['filter'].')/', '', $uri);
					$uriAux = preg_replace('/(\?&)/', '?', $uriAux);
					$uriAux = preg_replace('/(\?|&)$/', '', $uriAux);?>
					<a href="<?=$uriAux?>">Price - <?=$_GET['filter']?> - x</a>
				<?} else {?>
				<a id="filter-collection" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
					Price Filter
					<span class="caret"></span>
				</a>
				<ul id="filter-collection" class="dropdown-menu" role="menu" aria-labelledby="filter-collection">
					<? foreach($model->filters->filter as $filter=>$val) { ?>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$uri.'filter='.$filter?>"><?=$val?></a></li>
					<? } ?>
				</ul>
			</li>
			<?}?>
		</ul>

	</div> <!-- filters -->
</div> <!-- container -->