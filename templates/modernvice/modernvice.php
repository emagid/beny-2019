<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->


<!--[if lte IE 9]>
<script type="text/javascript" src="<?=FRONT_JS?>js/ie8/html5shiv-printshiv.min.js"></script>
<script "text/javascript" src="<?=FRONT_JS?>js/ie8/respond.min.js"></script>
<![endif]-->

<head>
    <!-- META DATA -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BENY</title>
    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">
    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

    <meta property="og:title" content="<?=SITE_NAME?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=SITE_URL?>" />

    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="msapplication-TileImage" content="<?=SITE_URL?>favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=SITE_URL?>content/frontend/img/favicon.png">
    <link rel = "shortcut icon" href = "<?=SITE_URL?>content/frontend/img/favicon.png">
    <link rel = "stylesheet" href = "<?=SITE_URL?>content/frontend/css/main.css">
    <link rel = "stylesheet" href = "<?=SITE_URL?>content/frontend/css/beny.css">
    <link rel = "stylesheet" href = "<?=SITE_URL?>content/frontend/css/photobooth.css">
    <link rel = "stylesheet" href = "<?=SITE_URL?>content/frontend/fonts/bikesans.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="theme-color" content="#ffffff">
    <meta name="google-site-verification" content="jjNUcRDPX0H4RcovCMgI763lWcMQCCwLYe8_Img5Ki8" />
    <link href='https://fonts.googleapis.com/css?family=Cabin:400,500,600' rel='stylesheet' type='text/css'>
    <!-- Disables Zoom on Mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- <link rel="stylesheet" href="print.css" type="text/css" media="print"> -->


    <!-- SITE CSS -->

    <?php
    if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='localhost') {
        define('WP_ENV', 'development');
    } else {
        define('WP_ENV', 'production');
    }
    ?>
 <link rel="stylesheet" href="<?=FRONT_CSS?>main.css">
    <script src="<?=FRONT_JS?>jquery.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=FRONT_JS?>vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="<?=FRONT_JS?>jquery.zoom.min.js"></script>

    <? if (WP_ENV == 'development') { ?>
        <script src="<?=FRONT_JS?>main.js"></script>
    <?php } else { ?>
        <script src="<?=FRONT_JS?>main.min.js"></script>
    <?php } ?>

    <script src="<?=FRONT_JS?>script.js"></script>
    <script src="<?=FRONT_JS?>ui_scripts.js"></script>
    <script src="<?=FRONT_LIBS?>gifshot/gifshot.js"></script>
    <script>
        function fbshareCurrentPage(){
            window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; 
        }
    </script>

</head>
<body>
<!-- MODALS -->
    <?php if (is_null($model->user)) { ?>
    <!-- LOGIN -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Log In</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?=SITE_URL?>login">
                        <input type="hidden" name="redirect-url" value="<?= $this->emagid->uri ?>" />
                        <div class="form-group">
                            <label for="loginemail" class="sr-only">Email address</label>
                            <input name="loginemail" type="email" class="form-control" id="loginemail" placeholder="Email address *">
                        </div>
                        <div class="form-group">
                            <label for="loginpass" class="sr-only">Password</label>
                            <input name="loginpassword" type="password" class="form-control" id="loginpass" placeholder="Password *">
                        </div>
                        <div class="form-group">
                            <div class="pull-left">
                                <label>
                                    <input type="checkbox"> Remember Me
                                </label>
                            </div>
                            <div class="pull-right">
                                <a href="#">Forgot your password?</a>
                            </div>
                        </div>
                        <br /><br /><br />
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Log In</button>
                        </div>
                        <a href="#" class="btn btn-default btn-block" data-dismiss="modal" data-toggle="modal" data-target="#register">Create Account</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- REGISTER -->
    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Register</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?=SITE_URL?>login/register">
                        <input type="hidden" name="redirect-url" value="<?= $this->emagid->uri ?>" />
                        <div class="form-group">
                            <label for="registeremail" class="sr-only">Email address</label>
                            <input name="email" type="email" class="form-control" id="registeremail" placeholder="Email address *">
                        </div>
                        <div class="form-group">
                            <label for="registerfirstname" class="sr-only">First name</label>
                            <input name="first_name" type="text" class="form-control" id="registerfirstname" placeholder="First name">
                        </div>
                        <div class="form-group">
                            <label for="registerlastname" class="sr-only">Last name</label>
                            <input name="last_name" type="text" class="form-control" id="registerlastname" placeholder="Last name">
                        </div>
                        <div class="form-group">
                            <label for="loginpass" class="sr-only">Password</label>
                            <input name="password" type="password" class="form-control" id="loginpass" placeholder="Password *">
                        </div>
                        <div class="form-group">
                            <div class="pull-left">
                                <label>
                                    <input name="registeragree" type="checkbox" /> I've read and agree with the <a href="<?=SITE_URL?>page/terms-and-conditions" target="_blank">Terms & Conditions</a> and the <a href="<?=SITE_URL?>page/privacy-policy" target="_blank">Privacy Policy</a>
                                </label>
                            </div>
                        </div>
                        <br /><br /><br />
                        <div class="form-group">
                            <!-- Remove data-dismiss once account page is created -->
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <!-- NEWSLETTER -->
    <div class="modal fade" id="newsletter" tabindex="-1" role="dialog" aria-labelledby="newsletter" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Sign up & get exclusive offers</h4>
                    <hr />
                </div>
                <div class="modal-body">

                    <div class="inner">
                        <p>Up To</p> 
                        <span class="number">
                            70
                            <span class="right">
                                <span class="top">%</span>
                                <span class="bottom">Off</span>
                            </span>
                        </span>
                    </div>
                    <form method="post" action="<?=SITE_URL?>newsletter/add">
                        <div class="form-group">
                            <label for="loginemail" class="sr-only">Email address</label>
                            <input type="email" name="email" class="form-control" id="loginemail" placeholder="Email">
                            <input type="hidden" name="redirectTo" value="<?=$this->emagid->uri?>" />
                        </div>
                        <div class="form-group">
                            <!-- Remove data-dismiss once Sign Up page is created -->
                            <a href="#"><button type="submit" class="btn btn-primary btn-block">Sign Up</button></a>
                        </div>
                        <div class="text-center">
                            <a href="#" data-dismiss="modal" aria-label="Close"><em>No, Thank You</em></a>
                            <br /><br /><br />
                            <p> * By entering your email you are eligible to recieve promotional offers from MODERNVICE.COM</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<header>
<!-- Navigation -->
</header>
<!-- End Navigation -->



<?display_notification();?>

     <!-- <div class="container"> -->
        <div class="row">
            <!-- <div class="col-lg-24"> -->
                <?php $this->emagid->controller->renderBody($model); ?>
            <!-- </div> --> <!-- .col-lg-24 -->
        </div> <!-- .row -->
    <!-- </div> --> <!-- .container -->

    <?php function footer() { ?>
        
    <?php } ?>

    <script src="<?=FRONT_JS?>script.js"></script>
    
    <script type="text/javascript">

        // no rights clicks
        document.addEventListener('contextmenu', event => event.preventDefault());
    
         // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    window.location.href = '/';
                }, 90000);
        }

        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
        });
    </script>
</body>
</html>

<? if ($_SERVER['SERVER_NAME'] == 'modernvice.com' || $_SERVER['SERVER_NAME'] == 'www.modernvice.com') { ?>
<!--TODO st-dev google analytics for MV-->
<!--<script>-->
<!--  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){-->
<!--  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),-->
<!--  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)-->
<!--  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');-->
<!---->
<!--  ga('create', 'UA-15845578-1', 'auto');-->
<!--  ga('send', 'pageview');-->
<!--</script>-->

<? } ?>