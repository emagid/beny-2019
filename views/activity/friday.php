    <div class='background' style='height: 100%;
    min-height: 2500px !important;
    width: 100%; background:url(<?=FRONT_IMG?>inner_bg.jpg)'></div>
    <div class="home full_view activity" >

        <div class="return_home">
            <a href="/">
                <img src="<?=FRONT_IMG?>home-wht.png">
            </a>
        </div>


        <div class="inner_roadmap" id="friday">

            <div class="roadmap_header">
                <a href="/">
                    <img src="<?=FRONT_IMG?>beny-brand.png">
                </a>
                <h3>Activity Schedule</h3>
            </div>

            <div class="activity_schedule" >
                <h2><span class="box">Friday</span> <span class="opacity40">/ </span>
                    <span class="opacity40 box"><a href="/activity/saturday"> Saturday</a></span></h2>
                <ul>
                    <h3>Stage</h3>
                    <li>
                        <p>11:00AM</p>
                        <p>MINUTE TO WIN-IT: Solo Cups x 3</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>rockbros.gif">
                                    <p class='prize'>PRIZE</p>
                                    <p>Cycling Trainer</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>11:30AM</p>
                        <p>TOUR REPORT</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>nuun_logo.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Nuun Performance x2<br>Electrolytes & Vitamins x2<br>Water Bottles x2</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>12:00PM</p>
                        <p>JEOPARDY</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>tripleEight_logo.png">
                                    <p class='prize'>PRIZE</p>
                                    <p> Compass Helmet</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>12:30PM</p>
                        <p>TRIPS, RIDES, TOURS AND TREKS</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <!-- <img src="<?=FRONT_IMG?>rockbros.gif"> -->
                                    <p class='prize'>PRIZE</p>
                                    <p>Multiples</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>1:30PM</p>
                        <p>WHERE ARE WE NOW?</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>manhattanPortage_logo.gif">
                                    <p class='prize'>PRIZE</p>
                                    <p>1651 Pulaski Messenger Bag</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>2:00PM</p>
                        <p>PROJECT BIKEWAY</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>cleverHood.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>CleverZipster x 3</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>2:30PM</p>
                        <p>BREAK</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <p class='prize'>BREAK</p>
                                    <p>CLIF Bar - Video clip</p>
                                </div>
                            </div>
                        </div>
                    </li>

                                        <li>
                        <p>3:00PM</p>
                        <p>MINUTE TO WIN-IT</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>lumos.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Smart Bike Helmet</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>3:30PM</p>
                        <p>HOW TO FIX A FLAT AND FIX A FLAT CONTEST</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>rei.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Camp Dome 2 Tent</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>4:00PM</p>
                        <p>ABC QUICK CHECK</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>issi.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Flip III Pedals</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>4:30PM</p>
                        <p>JEOPARDY</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>promCampo.jpeg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Bike Share Bag</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>5:00PM</p>
                        <p>WHERE ARE WE NOW?</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>seaSucker.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Talon Bike Rack</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>5:30PM</p>
                        <p>HOW TO SECURE YOUR WHOLE BIKE - Bike Safety contest</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>kryptonite.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Var. Items</p>
                                </div>
                            </div>
                        </div>
                    </li>
                     <li>
                        <p>6:00PM</p>
                        <p>PROJECT BIKEWAY</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>primal.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Mens Bix Hoodie<br>Ella Women's Hoodie</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>6:30PM</p>
                        <p>TOUR REPORT</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>abus.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>GANITE Xplus 6500 Folding Lock</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <h6>*These activities are contests with prizes;<br>
audience members will be invited to participate</h6>
                </ul>

            </div>

        </div>

            <div class="bike-brand">
                <a href="/">
                    <img src="<?=FRONT_IMG?>bike-brand.svg">
                </a>
            </div>
    </div>


<?php echo footer(); ?>

<style type="text/css">
html,body, .full_view, .row{
    height: auto;
    padding-bottom: 100px;
    min-height: 2500px !important;
    width: 100%;
}
</style>


<script type="text/javascript">
    $(document).ready(function(){
        $('.activity_schedule ul li').click(function(){
          if ( !$('.activity_schedule .popup').is(':visible') ) {
            $(this).children('.popup').fadeIn();
            $(this).children('.popup').css('display', 'flex');
          }
        });

        $('.activity_schedule .popup_drop').click(function(){
            $('.popup').fadeOut();
        });
    });

</script>