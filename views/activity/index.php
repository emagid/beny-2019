    <div class='background' style='height: 100%;
    min-height: 2500px !important;
    width: 100%; background:url(<?=FRONT_IMG?>inner_bg.jpg)'></div>
    <div class="home full_view activity" >


        <div class="inner_roadmap">

            <div class="roadmap_header">
                <img src="<?=FRONT_IMG?>beny-brand.png">
                <h3>Food & Beverage</h3>
            </div>

            <div class="food_menu" id="drinks">
                <h2>New Belgium <br>Beer Garden</h2>
                <img class="nb-logo" src="<?=FRONT_IMG?>nb-logo-best.png">
                <ul>
                    <li>
                        <h4>Fat Tire Amber Ale</h4>
                        <p>A perfect balance of biscuit-like malt flavors with hoppy freshness</p>
                    </li>
                    <li>
                        <h4>Voodoo Ranger IPA</h4>
                        <p>Bursting with tropical aromas and juicy fruit flavors from Mosaic and Amarillo hops, this golden IPA is perfectly bitter with a refreshing, sublime finish</p>
                    </li>
                    <li>
                        <h4>CITRADELIC TANGERINE IPA</h4>
                        <p>Citra hops and tangerine elevate each sip onto a plane of juicy, tropical pleasure</p>
                    </li>
                    <li>
                        <h4>PILSENER</h4>
                        <p>Brewed in the traditional Czech style for a deeply refreshing flavor with a hint of malty sweetness</p>
                    </li>
                </ul>
            </div>
            <div class="food_menu" id="food">
                <h2>Food Trucks</h2>
                <ul>
                    <h5>(Click name to see menu)</h5>
                    <li>
                        <h4><a href="">Coney Shack</a></h4>
                    </li>
                    <li>
                        <h4>The Crepes Truck</h4>
                    </li>
                    <li>
                        <h4>The Mac Truck</h4>
                    </li>
                    <li>
                        <h4>Nuchas</h4>
                    </li>
                    <li>
                        <h4>Souvlaki Gr</h4>
                    </li>
                    <li>
                        <h4>Valduccis</h4>
                    </li>
                </ul>
            </div>

        </div>

            <div class="bike-brand activity">
                <a href="/">
                    <img src="<?=FRONT_IMG?>bike-brand.svg">
                </a>
            </div>
    </div>


<?php echo footer(); ?>

<style type="text/css">
html,body, .full_view, .row{
    height: auto;
    padding-bottom: 100px;
    min-height: 2500px !important;
        width: 100%;
}
    .full_view {
    background-repeat: no-repeat !important;
    -webkit-background-size: cover !important;
    -moz-background-size: cover !important;
    background-size: cover !important;
    -o-background-size: cover !important;
    position: relative;
    }
    .opacity80 {
        color:rgba(255, 255, 255, 0.8);
    }
    img.nb-logo {
        display: inline-block;
        vertical-align: middle;
        width: 110px;
        margin-left: 15px;
    }
    .food_menu {
        margin-top: 60px;
        width: 60%;
    }
    .food_menu ul {
        border-top: 3px solid rgba(255, 255, 255, 0.8);
        padding-left: 0;
        margin-left: 0;
    }
    .food_menu h2 {
        font-family: 'Bike-Sans-Extra-Bold';
        font-size: 64px;
        text-transform: uppercase;
        margin-top: 0;
        line-height: 1em;
        font-style: italic;
    }
    .food_menu h4 {
        font-family: 'Bike-Sans-Extra-Bold';
        font-size: 38px;
        text-transform: uppercase;
        margin-top: 10px;
        margin-bottom: 5px;
    }
    .food_menu h4 a {
        color:#fff;
    }
    .food_menu h5 {
        font-family: 'Bike-Sans-Extra-Bold';
        font-size: 32px;
        margin-top: 10px;
        margin-bottom:30px;
    }
    .food_menu#drinks ul li {
        list-style-type: none;
        line-height: 4em;
        margin-bottom: 35px;
    }
    .food_menu#drinks h2 {
        display: inline-block;
        vertical-align: middle;
    }
    .food_menu#food ul li {
        list-style-type: none;
        line-height: 4em;
        margin-bottom: 0;
    }
    .food_menu#food ul li h4 {
        margin-top: 0;
        font-size: 34px;
    }
    .food_menu ul li p {
        font-family: 'Bike-Sans-Medium-Italic';
        font-size: 30px;
        margin-top: 0;
        margin-bottom: 0;
        line-height: 1.25em;
    }
    .food_menu ul li p a {
        color:rgba(255, 255, 255, 1);
    }
    .food_menu ul li p a:hover {
        color:rgba(255, 255, 255, 0.85);
    }


    .inner_roadmap {
        padding-top: 100px;
        margin-left: 100px;
        color:#fff !important;        
    }
    .roadmap_header h3{
        text-transform: uppercase;
        font-family: 'Bike-Sans-Extra-Bold';
        font-size: 46px;
        margin-top: 10px;
    }
    .inner_roadmap h3.bike-bold {
        margin-top: 0;
    }
    .roadmap_header img {
        width: 60%;
    }
    .bike-brand {
        position: absolute;
        bottom: 100px;
        right: 100px;
    }
    .bike-brand img{
        opacity: 0.8;
        width: 175px;
    }
    .bike-brand img:hover {
        opacity: 1;
    }
</style>


<script type="text/javascript">
    
</script>