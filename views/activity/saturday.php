    <div class='background' style='height: 100%;
    min-height: 2500px !important;
    width: 100%; background:url(<?=FRONT_IMG?>inner_bg.jpg)'></div>
    <div class="home full_view activity" >

        <div class="return_home">
            <a href="/">
                <img src="<?=FRONT_IMG?>home-wht.png">
            </a>
        </div>


        <div class="inner_roadmap" id="friday">

            <div class="roadmap_header">
                <a href="/">
                    <img src="<?=FRONT_IMG?>beny-brand.png">
                </a>
                <h3>Activity Schedule</h3>
            </div>

            <div class="activity_schedule" >
                <h2>
                    <span class="opacity40 box"><a href="/activity/friday">Friday  </a></span> 
                    <span class="opacity40"> &nbsp;/ </span> 
                    <span class="box"> Saturday</span></h2>
                <ul>
                    <h3>Stage</h3>
                                        <li>
                        <p>10:30AM</p>
                        <p>ABC QUICK CHECK</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>floraVelo.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Cycling jersey</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>11:00AM</p>
                        <p>MINUTE TO WIN-IT: Waffle Head & Chew Race</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>honey.jpeg">
                                    <p class='prize'>PRIZE</p>
                                    <p>1 Box Waffles<br>1 Box Chews</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>11:30AM</p>
                        <p>JEOPARDY</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>thousand.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Gift Card for Helmet</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>12:00PM</p>
                        <p>ROLLING INTO SPRING FASHION SHOW</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>momentum.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Lock</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>1:00PM</p>
                        <p>BREAK</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <p class='prize'>BREAK</p>
                                    <p>CLIF Bar - Video clip</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>1:30PM</p>
                        <p>HOW TO FIX A FLAT and  FIX A FLAT CONTEST</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>rei.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Camp Dome 2 Tent</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>2:00PM</p>
                        <p>WHERE ARE WE NOW?</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>pure.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Coaster Bike</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>2:30PM</p>
                        <p>MINUTE TO WIN-IT: Solo Cup Stack</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>flatbike.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>CHANGE 633 Folding Mountain Bike Frame</p>
                                </div>
                            </div>
                        </div>
                    </li>
                                        <li>
                        <p>3:00PM</p>
                        <p>ROLLING INTO SPRING FASHION SHOW</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>momentum.png">
                                    <p class='prize'>PRIZE</p>
                                    <p>Helmet</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>4:00PM</p>
                        <p>MINUTE TO WIN-IT: Helmet Toss</p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <img src="<?=FRONT_IMG?>nutcase.jpg">
                                    <p class='prize'>PRIZE</p>
                                    <p>Certificate for Helmet</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>4:30PM</p>
                        <p>PROJECT BIKEWAY & TOUR REPORT </p>
                        <div class='popup'>
                            <div class='popup_drop'>
                                <div class='popup_box'>
                                    <div>
                                        <img style='height: 250px; width: auto; margin-right: 50px;' src="<?=FRONT_IMG?>paragon.png">
                                        <img style='height: 250px; width: auto;' src="<?=FRONT_IMG?>specialized.png">
                                    </div>
                                    <p class='prize'>PRIZE</p>
                                    <p>Gift Card x 2<br>Stormproof Backpack</p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <h6>*These activities are contests with prizes;<br>
audience members will be invited to participate</h6>
                </ul>

            </div>

        </div>

            <div class="bike-brand">
                <a href="/">
                    <img src="<?=FRONT_IMG?>bike-brand.svg">
                </a>
            </div>
    </div>


<?php echo footer(); ?>

<style type="text/css">
html,body, .full_view, .row {
    height: auto;
    padding-bottom: 100px;
    min-height: 2500px !important;
    width: 100%;
}
</style>


<script type="text/javascript">
    $(document).ready(function(){
        $('.activity_schedule ul li').click(function(){
          if ( !$('.activity_schedule .popup').is(':visible') ) {
            $(this).children('.popup').fadeIn();
            $(this).children('.popup').css('display', 'flex');
          }
        });

        $('.activity_schedule .popup_drop').click(function(){
            $('.popup').fadeOut();
        });
    });

</script>