<?php if(count($model->files)>0) { ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="20%">Name</th>
          <th width="15%" class="text-center">Download</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->files as $file){ ?>
        <tr>
         <td><?=$file?></td>
         <td class="text-center">
           <a class="btn-actions" href="/<?=$file?>">
           <i class="icon-pencil"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
</script>

