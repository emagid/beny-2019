<?php if (count($model->admins) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Username</th>
                <th width="20%">Email</th>
                <th width="20%">Role</th>
                <th width="20%">Type</th>
                <th width="10%" class="text-center">Edit</th>
                <th width="10%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->admins as $admin): ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><?php echo $admin->first_name; ?><?php echo $admin->last_name; ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><?php echo $admin->username; ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><?php echo $admin->email; ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><?php echo $admin->getRoleName(); ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><?php echo strtoupper($admin->type); ?></a>
                    </td>

                    <td class="text-center"><a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>administrators/update/<?php echo $admin->id; ?>"><i
                                class="icon-pencil"></i></td>

                    <td class="text-center"><a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>administrators/delete/<?php echo $admin->id; ?>"
                                               onClick="return confirm('Are You Sure?');"><i
                                class="icon-cancel-circled"></i> </a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php }; ?>

<?= footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'administrators';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>