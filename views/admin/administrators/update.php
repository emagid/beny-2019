<div class="row">
  <div class="col-md-12">
    <div class="box">
      <h4>General</h4>
      <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="id" value="<?php echo $model->admin->id; ?>" />
        <div class="form-group">
          <label>First name</label>
          <?= $model->form->editorFor("first_name") ?>
        </div>
        <div class="form-group">
          <label>Last name</label>
          <?= $model->form->editorFor("last_name") ?>
        </div>
        <div class="form-group">
          <label>Username</label>
          <?= $model->form->editorFor("username") ?>
        </div>
        <div class="form-group">
          <label>Email</label>
          <?= $model->form->editorFor("email") ?>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input name="password" type="password" />
        </div>
		  <div class="form-group"  <?=strtolower($this->_viewData->logged_admin->getRoleName()) != 'admin' ? 'style="display:none"': ''?>>
			  <label>Role</label>
			  <select name="role">
				  <option value="">--SELECT--</option>
				  <?php foreach(\Model\Role::getList(['orderBy'=>'id asc']) as $role){ ?>
					  <option value="<?=$role->id?>" <?=($model->admin->id != 0 && ($role->name == $model->admin->getRoleName()))?'selected':null?>><?=$role->name?></option>
				  <?php } ?>
			  </select>
		  </div>
      </div>
    </div>
    <div class="col-md-1"></div>
		<div class="col-md-11" <?=strtolower($this->_viewData->logged_admin->getRoleName()) != 'admin' ? 'style="display:none"': ''?>>
	    <div class="box">
	      <h4>Permissions</h4>
	      <ul class="permissiontable list-unstyled list-group">
	        <?php
	        foreach($model->all_admin_sections as $parent=>$children): ?>
	          <li class="parent_<?= strtolower(str_replace(' ','_',$parent));?>" data-name="<?= strtolower(str_replace(' ','_',$parent));?>">
	
	          <label>
	            <input type="checkbox" name="admin_section[]" class="parent" value="<?php echo $parent; ?>" <?php if(in_array($parent, $model->admin_permissions)){echo 'checked="checked"';} ?> />
	            <?php echo $parent; ?></label>
	
	          </li>
	          <?php 
	          if(is_array($children) && count($children)>0) {

	            foreach($children as $child) {
	            	if ($child == 'Users'){
	            		$label = 'Customer Accounts';
	            	} else if ($child == 'Pages'){
	            		$label = 'CMS';
	            	} else {
	            		$label = $child;
	            	}
	          ?>
	
	            <li class="child_<?= strtolower(str_replace(' ','_',$parent));?>" data-parent_name="<?= strtolower(str_replace(' ','_',$parent));?>" style="padding-left:40px;">
	              <label style="font-weight:400">
	                <input type="checkbox" name="admin_section[]" class="child" value="<?php echo $child; ?>" <?php if(in_array($child, $model->admin_permissions)){echo 'checked="checked"';} ?> />
	                <?php echo $label; ?></label>
	              </li>
	
	              <?php 
	            } ?>
	
	            <?php } ?>
	
	
	          <?php endforeach; ?>
	        </ul>
	      </div>
      </div>


      <div class="col-md-24">
        <input type="submit" class="btn-save" value="Save" />
      </div>
    </div>
  </form>

<?php echo footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("input.parent").on('change',function() {
	        if($(this).is(":checked")) {
	            var name = $(this).closest('li').data('name');
	            $(".child_"+name).find('input.child').each(function(i,e) {
	            	$(e).prop('checked',true);
	            });
	        } else {
	            var name = $(this).closest('li').data('name');
	            $(".child_"+name).find('input.child').each(function(i,e) {
	            	$(e).prop('checked',false);
	            });
	        }
        });
		$("input.child").on('change',function() {
			var name = $(this).closest("li").data('parent_name');
        	if($(this).is(":checked")) {
            	$(".parent_"+name).find("input.parent").each(function(i,e) {
            		$(e).prop('checked',true); 
          		});
        	} else {
            	var empty = true;
            	$(".child_"+name).find("input.child").each(function(i,e) {
                	if ($(this).is(":checked")){
						empty = false;
                    } 
            	});
            	if (empty){
            		$(".parent_"+name).find("input.parent").each(function(i,e) {
                		$(e).prop('checked',false); 
              		});
                }
        	}
		});
		$("select").multiselect();

	});
</script>