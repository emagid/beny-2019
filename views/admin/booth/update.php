<link href="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.min.css" />
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->booth->id; ?>"/>
    <input type="hidden" name="redirectTo" value="<?= $_SERVER['HTTP_REFERER'] ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>booth/delete/<?php echo $model->booth->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

                            <div class="form-group">
                                <label>Number</label>
                                <?php echo $model->form->editorFor("number"); ?>
                            </div>

                            <div class="form-group">
                                <label>Icon</label>

                                <p>
                                    <small style="color:#A81927"><b></b></small>
                                </p>
                                <p><input type="file" name="icon" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->booth->icon != "" && file_exists(UPLOAD_PATH . 'booths/' . $model->booth->icon)) {
                                        $img_path = UPLOAD_URL . 'booths/' . $model->booth->icon;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <input type="hidden" name="icon"
                                                   value="<?= $model->booth->icon ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Category</label>
                                <select name="category[]" class="multiselect" data-selected="<?=json_encode($model->boothCategories)?>" data-placeholder="Categories" multiple="multiple">
                                    <?php foreach ($model->categories as $cat) { ?>
                                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
<!--            <div role="tabpanel" class="tab-pane" id="images-tab">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-24">-->
<!--                        <div class="box">-->
<!--                            <div class="dropzone" id="dropzoneForm" action="--><?php //echo ADMIN_URL . 'booth/upload_images/' . $model->booth->id; ?><!--"></div>-->
<!--                            <button id="upload-dropzone" class="btn btn-danger">Upload</button>-->
<!--                            <br/>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="row">-->
<!--                    <div class="col-md-24">-->
<!--                        <div class="box">-->
<!--                            <table id="image-container" class="table table-sortable-container">-->
<!--                                <thead>-->
<!--                                <tr>-->
<!--                                    <th>Image</th>-->
<!--                                    <th>File Name</th>-->
<!--                                    <th>Display Order</th>-->
<!--                                    <th>Delete</th>-->
<!--                                </tr>-->
<!--                                </thead>-->
<!--                                <tbody>-->
<!--                                --><?php //foreach (\Model\Product_Image::getList(['where' => 'product_id=' . $model->product->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {
//
//                                    if ($pimg->exists_image()) {
//                                        ?>
<!--                                        <tr data-image_id="--><?php //echo $pimg->id; ?><!--">-->
<!--                                            <td><img src="--><?php //echo $pimg->get_image_url(); ?><!--" width="100"-->
<!--                                                     height="100"/></td>-->
<!--                                            <td>--><?php //echo $pimg->image; ?><!--</td>-->
<!--                                            <td class="display-order-td">--><?php //echo $pimg->display_order; ?><!--</td>-->
<!--                                            <td class="text-center">-->
<!--                                                <a class="btn-actions delete-product-image"-->
<!--                                                   href="--><?php //echo ADMIN_URL; ?><!--products/delete_prod_image/--><?php //echo $pimg->id; ?><!--?token_id=--><?php //echo get_token(); ?><!--">-->
<!--                                                    <i class="icon-cancel-circled"></i>-->
<!--                                                </a>-->
<!--                                            </td>-->
<!--                                        </tr>-->
<!--                                        --><?php
//                                    }
//                                } ?>
<!--                                </tbody>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url =<?php echo json_encode(ADMIN_URL.'products/'); ?>;
    $(document).ready(function () {
//        var categories = '<?//=$model->boothCategories?>//';
        $("select.multiselect").multiselect();
        $("select.multiselect").val($("select.multiselect").data('selected'));
//        $("select[name='color[]']").val(colors);
//        $("select[name='size[]']").val(sizes);
//        $("select[name='product_collection[]']").val(collections);
//        $("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });

        $("a.delete-product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            }
        });

    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'booth/upload_images/'.$model->booth->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>
