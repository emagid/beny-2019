<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<?php
 if(count($model->categorys)>0): ?>
 <?php //dd($model)?>
  <div class="box box-table">
    <table id="sortables" class="table">
      <thead>
        <tr>
          <th width="15%">Name</th>
          <th width="15%">Parent</th>
          <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody class="sort">
       <?php foreach($model->categorys as $obj){ ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>category/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>category/update/<?= $obj->id ?>"><?php echo $obj->getParent() ? $obj->getParent()->name : '' ; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>category/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>category/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script>
    $(document).ready(function(){
        var adjustment;
        $('#sortables').DataTable();
    })
</script>