<div id="page-wrapper">  
	<div class="left_col_dashboard">
	<div class="row">    
		<div class="col-lg-24 dashboardnotify">
<div class="top_panels">
	

		<div class="col-lg-6 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="monthly_gross_commission"></i>
						</div>
						<div class="col-xs-20 text-right">
							<div class="huge">$149K</div>
							<div class="panel_text">
								<p>
									Monthly Gross Commission
								</p>
							</div>
						</div>
					</div>
				</div>
				<a href="<?=ADMIN_URL?>orders">
					<div class="panel-footer viewsales">
						<span class="pull-left"><p>View Details</p></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		 </div>


		 	<div class="col-lg-6 col-md-6">
			<div class="panel panel-orange">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-line-chart fa-5x"></i>
						</div>
						<div class="col-xs-20 text-right">

							<div class="huge">17%
								<!-- £<?php echo number_format($model->month_data->gbp_total, 0); ?> -->
							</div>
							<div class="panel_text">
								<p>3-Month Closing Rate</p>
							</div>
						</div>
					</div>
				</div>
				<a href="<?=ADMIN_URL?>orders">
					<div class="panel-footer viewsales">
						<span class="pull-left"><p>View Details</p></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		 </div>

		 <div class="col-lg-6 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="opportunities_icon"></i>
						</div>
						<div class="col-xs-20 text-right">

							<div class="huge">14</div>
							<div class="panel_text">
								<p>New Leads</p>
							</div>
						</div>
					</div>
				</div>
				<a href="<?=ADMIN_URL?>opportunities">
					<div class="panel-footer viewsales">
						<span class="pull-left"><p>View Details</p></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		 </div>


		 <div class="col-lg-6 col-md-6">
			<div class="panel panel-blue">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="listings_icon"></i>
						</div>
						<div class="col-xs-20 text-right">

							<div class="huge">22
							</div>
							<div class="panel_text">
								<p>Open Listings</p>
							</div>
						</div>
					</div>
				</div>
				<a href="<?=ADMIN_URL?>orders">
					<div class="panel-footer viewsales">
						<span class="pull-left"><p>View Details</p></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		 </div>
	</div>
</div>

</div> <!-- top_panels -->

	<div class="dashboard_chart_row">
		<div class="col-md-8 chartsales">
			<div class="panel panel-default" style="min-height: 390px;">
		  		<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-money"></i> Leads Status</h3>
		  		</div>
				
				<!-- html executes chart javascript -->
				<div id="company-sales" style="width: calc(100% - 48px)">
					<canvas id="leads-status-chart" height="240"></canvas>
				</div>

			</div>
		</div>


		<div class="col-md-8 chartgross">
			<div class="panel panel-default" style="min-height: 390px;">
  				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-area-chart"></i> Closing Rate</h3>
  				</div>
				
				<div id="gross-margin" style="width: calc(100% - 48px)">
					<canvas id="closing-rate-chart" height="240"></canvas>
				</div>

			</div>
		</div>
		
		<div class="col-md-8 chartorders">
			<div class="panel panel-default" style="min-height: 390px;">
  				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-truck"></i> Gross Commissions</h3>
  				</div>

				<!-- html executes chart javascript -->
				<div id="monthly-orders" style="width: calc(100% - 48px)">
					<canvas id="gross-commissions-chart" height="100"></canvas>
				</div>

  
			</div>
		</div>
	</div>




		<div class="row"> 


<div class=" charts-list">
	<div class="col-md-12">
		<div class="panel panel-default" style="min-height: 220px;">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-money"></i> Recent Leads</h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped tablesorter">
						<thead>
							<tr>
								<th class="header">Order # <i class="fa fa-sort"></i></th>
								<th class="header">Order Date <i class="fa fa-sort"></i></th>
								<th class="header">Order Time <i class="fa fa-sort"></i></th>
								<th class="header">Amount <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($model->recent_orders as $order): ?>
							<tr>
								<td><a href="<?=ADMIN_URL?>orders/update/<?=$order->id?>"><?php echo $order->id; ?></a></td>
								<td><a href="<?=ADMIN_URL?>orders/update/<?=$order->id?>"><?php echo date("m/d/Y", strtotime($order->insert_time)); ?></a></td>
								<td><a href="<?=ADMIN_URL?>orders/update/<?=$order->id?>"><?php echo date("g:i A", strtotime($order->insert_time)); ?></a></td>
								<td><a href="<?=ADMIN_URL?>orders/update/<?=$order->id?>">$<?= number_format($order->total,2);?></a></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="text-right">
				<a href="<?php echo ADMIN_URL; ?>orders">View All Leads <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="panel panel-default" style="min-height: 220px;">
		  <div class="panel-heading">
		    <h3 class="panel-title"><i class="fa fa-users"></i> Recent Appointments</h3>
		  </div>
		  <div class="panel-body">
		    <div class="table-responsive">
		      <table class="table table-bordered table-hover table-striped tablesorter">
		        <thead>
		          <tr>
		            <th class="header">Full Name <i class="fa fa-sort"></i></th>
		            <th class="header">Email <i class="fa fa-sort"></i></th>
		            <th class="header">Date Joined <i class="fa fa-sort"></i></th>
		          </tr>
		        </thead>
		        <tbody>
				<?php foreach($model->recent_clients as $client): ?>	
		          <tr>
		            <td><a href="<?=ADMIN_URL?>users/update/<?=$client->id?>"><?php echo $client->first_name; ?> <?php echo $client->last_name; ?></a></td>
		            <td><a href="<?=ADMIN_URL?>users/update/<?=$client->id?>"><?php echo $client->email; ?></a></td>
		            <td><a href="<?=ADMIN_URL?>users/update/<?=$client->id?>"><?php echo $client->insert_time; ?></a></td>
		          </tr>
				<?php endforeach; ?>
		        </tbody>
		      </table>
		    </div>
		    <div class="text-right">
		      <a href="<?php echo ADMIN_URL; ?>users">View All Appointments <i class="fa fa-arrow-circle-right"></i></a>
		    </div>
		  </div>
		</div>
	</div>

</div>


</div>
</div>
<div class="right_col_dashboard">
	<div class="right_col_dashboard_header">
		<h2>Recent Activity</h2>
		<div class="right_abs">
		</div>
	</div>
	<div class="recent_updates_list">
		<div class="recent_update_item recent_update_item_lead">
			<div class="update_item_header">
				<div class="left_abs">
					<icon></icon>
					<p>added by <a>Tony Park</a></p>
				</div>
			</div>
			<div class="update_item_body">
				<label>Lead Summary</label>
				<p>Walter's Wickers Furniture is looking for a new office and showroom location in the Soho/Chelsea area.</p>
				<a class="recent_update_btn lead_cta_btn">Follow up with Elad</a>
			</div>
		</div>
		<div class="recent_update_item recent_update_item_listing">
			<div class="update_item_header">
				<div class="left_abs">
					<icon></icon>
					<p>added by <a>Elad Dror</a></p>
				</div>
			</div>
			<div class="update_item_body">
				<label>Listing Summary</label>
				<p>Excellent commercial retail / office development location potential. Signalized intersection with approved access on Colchester Drive.</p>
				<a class="recent_update_btn listing_cta_btn">View Listing</a>
			</div>
		</div>
		<div class="recent_update_item recent_update_item_appt">
			<div class="update_item_header">
				<div class="left_abs">
					<icon></icon>
					<p>added by <a>Francesco Bardazzi</a></p>
				</div>
			</div>
			<div class="update_item_body">
				<label>Appointment Summary</label>
				<p>Meeting Subway franchise owner Ken Burns at 23rd and Lex to review their application on our Soho Com property.</p>
				<a class="recent_update_btn appointment_cta_btn">View Appointment</a>
			</div>
		</div>
	</div>
	<div class="recent_updates_fixed_bottom">
		<a class="btn btn-default">View More</a>
	</div>
</div>
</div>

<?php echo footer(); ?>
<script>
	$(document).ready(function(){
		Chart.defaults.global.responsive = true;
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var leadsStatusData = [
			{
				value: 50,
				color:"rgba(220,220,220,0.5)",
				highlight: "rgba(220,220,220,0.5)",
				label: "Open"
			},
			{
				value: 10,
				color: "rgba(151,187,205,1)",
				highlight: "rgba(151,187,205,1)",
				label: "Offer"
			},
			{
				value: 40,
				color: "rgba(220,220,220,0.8)",
				highlight: "rgba(220,220,220,0.8)",
				label: "Closing"
			}
		];

		var leadsStatusOption = {
			//Boolean - Whether we should show a stroke on each segment
			segmentShowStroke : true,

			//String - The colour of each segment stroke
			segmentStrokeColor : "#fff",

			//Number - The width of each segment stroke
			segmentStrokeWidth : 2,

			//Number - The percentage of the chart that we cut out of the middle
			percentageInnerCutout : 0, // This is 0 for Pie charts

			//Number - Amount of animation steps
			animationSteps : 20,
			scaleFontFamily: "calibre",

			//String - Animation easing effect
			animationEasing : "easeIn",

			//Boolean - Whether we animate the rotation of the Doughnut
			animateRotate : false,

			//Boolean - Whether we animate scaling the Doughnut from the centre
			animateScale : true,

			//String - A legend template
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",

			tooltipTemplate: "<%= label %> - <%= value + '%' %>",

		};

		var ctxLeadsStatusEle = document.getElementById("leads-status-chart").getContext("2d");
		var myPieChart = new Chart(ctxLeadsStatusEle).Pie(leadsStatusData,leadsStatusOption);


		var closingRateData = {
			labels: ["November", "December", "January", "February", "March", "April"],
			datasets: [
				{
					label: "Open",
					fillColor: "rgba(220,220,220,0.5)",
					strokeColor: "rgba(220,220,220,0.8)",
					highlightFill: "rgba(220,220,220,0.75)",
					highlightStroke: "rgba(220,220,220,1)",
					data: [65, 59, 80, 81, 56, 55]
				},
				{
					label: "Closed",
					fillColor: "rgba(151,187,205,0.5)",
					strokeColor: "rgba(151,187,205,0.8)",
					highlightFill: "rgba(151,187,205,0.75)",
					highlightStroke: "rgba(151,187,205,1)",
					data: [28, 48, 40, 19, 86, 27]
				}
			]
		};

		var closingRateOption = {
			//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
			scaleBeginAtZero : true,

			//Boolean - Whether grid lines are shown across the chart
			scaleShowGridLines : true,

			//String - Colour of the grid lines
			scaleGridLineColor : "rgba(0,0,0,.05)",

			//Number - Width of the grid lines
			scaleGridLineWidth : 1,

			//Boolean - Whether to show horizontal lines (except X axis)
			scaleShowHorizontalLines: true,

			//Boolean - Whether to show vertical lines (except Y axis)
			scaleShowVerticalLines: true,

			//Boolean - If there is a stroke on each bar
			barShowStroke : true,

			//Number - Pixel width of the bar stroke
			barStrokeWidth : 2,

			//Number - Spacing between each of the X value sets
			barValueSpacing : 5,

			//Number - Spacing between data sets within X values
			barDatasetSpacing : 1,

			//String - A legend template
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

			tooltipTemplate: "<%= label %> - <%= '$' + value %>",

			multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"

		};

		var ctxClosingRateEle = document.getElementById("closing-rate-chart").getContext("2d");
		var myBarChart = new Chart(ctxClosingRateEle).Bar(closingRateData,closingRateOption);

		var commissionData = {
			labels: ["November", "December", "January", "February", "March", "April"],
			datasets: [
				{
					label: "Gross Commissions",
					fillColor: "rgba(151,187,205,0.2)",
					strokeColor: "rgba(151,187,205,1)",
					pointColor: "rgba(151,187,205,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(151,187,205,1)",
					data: [1100, 1800, 2800, 3500, 4000, 1900]
				}
			]
		};

		var commissionOption = {

			///Boolean - Whether grid lines are shown across the chart
			scaleShowGridLines : true,

			//String - Colour of the grid lines
			scaleGridLineColor : "rgba(0,0,0,.05)",

			//Number - Width of the grid lines
			scaleGridLineWidth : 1,

			//Boolean - Whether to show horizontal lines (except X axis)
			scaleShowHorizontalLines: true,

			//Boolean - Whether to show vertical lines (except Y axis)
			scaleShowVerticalLines: true,

			//Boolean - Whether the line is curved between points
			bezierCurve : true,

			//Number - Tension of the bezier curve between points
			bezierCurveTension : 0.4,

			//Boolean - Whether to show a dot for each point
			pointDot : true,

			//Number - Radius of each point dot in pixels
			pointDotRadius : 4,

			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 1,

			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 20,

			//Boolean - Whether to show a stroke for datasets
			datasetStroke : true,

			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,

			//Boolean - Whether to fill the dataset with a colour
			datasetFill : true,

			//String - A legend template
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
			tooltipTemplate: "<%= label %> - <%= '$' + value %>"

		};

		var ctxCommissionEle = document.getElementById("gross-commissions-chart").getContext("2d");
		var myLineChart = new Chart(ctxCommissionEle).Line(commissionData,commissionOption);


	})


</script>