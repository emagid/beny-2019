

<div class="form-box" id="login-box">
	
	<?php if(isset($model->errors)){ ?>
	<?php foreach ($model->errors as $error) { ?>
	<div class="alert alert-danger"><?php echo $error; ?></div>
	<?php } ?>
	<?php } ?>
	<?php if(isset($model->message)){ ?>
	<p><?php echo $model->message; ?></p>
	<?php } ?>
	<form id="admin_login" action="<?= ADMIN_URL ?>login" method="post">

		<div class="body bg-gray">
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control" placeholder="Username"/>
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password"/>
			</div>
		</div>
		
		<div class="footer">
			<button type="submit" class="btn btn-save btn-block btn-lg">Sign In</button>
		</div>
	</form>
</div>

<?php echo footer(); ?>

<script type="text/javascript">
	$("#admin_login").validate({
		rules: {
			username:{
				required: true,
			},
			password:{
				required: true,
			}
		},
		messages:{
			username:{ 
				required:"Please enter your username",
			},
			password:{ 
				required:"Please enter your password",
			}
		},
		errorClass: "error"
	}
	)
</script>