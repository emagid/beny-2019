<div class="row">
   <div class="col-md-8">SEARCH BY  <a style="cursor:pointer;" id="change_mpn">MPN</a> | <a style="cursor:pointer;" id="change_ids"  >Id or Name</a><br>
      <div class="box">
        <div class="input-group">

            <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name" />
            <input id="search_by_mpn" type="text" name="search_by_mpn" class="form-control" placeholder="Search by MPN" style="display:none;"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
        </div>
      </div>
       <div class="box">
           Show on page:
           <select class="how_many" name="how_many" style="cursor:pointer">

               <option <? if (isset($_GET['how_many'])) {
                   if ($_GET['how_many'] == 10) {
                       echo "selected";
                   }
               } ?> selected value="10">10
               </option>
               <option <? if (isset($_GET['how_many'])) {
                   if ($_GET['how_many'] == 50) {
                       echo "selected";
                   }
               } ?> value="50">50
               </option>
               <option <? if (isset($_GET['how_many'])) {
                   if ($_GET['how_many'] == 100) {
                       echo "selected";
                   }
               } ?> value="100">100
               </option>
               <option <? if (isset($_GET['how_many'])) {
                   if ($_GET['how_many'] == 500) {
                       echo "selected";
                   }
               } ?> value="500">500
               </option>
               <option <? if (isset($_GET['how_many'])) {
                   if ($_GET['how_many'] == 1000) {
                       echo "selected";
                   }
               } ?> value="1000">1000
               </option>
           </select>
       </div>
    </div><script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div> <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script>
$(document).ready(function() {

 $('body').on('change', '.change', function(){

  var id = $(this).attr('data-id');
  var status=$(this).val();

  $.ajax({
                type: "POST",
                url: "/admin/orders/update_post_status/",
                data:"id=" + id+"&status="+status,
                success: function(html)
                {

                    $("#show"+id).fadeIn().css("display","inline");
                     $("#show"+id).fadeOut(1000);
                }
        });



  });


    $('body').on('change', '.how_many', function () {
        var how_many = $(this).val();
        <?if (isset($_GET['status_show'])){?>
            <?$br = $_GET['status_show'];?>
            window.location.href = '/admin/orders?status_show=<?=$br?>&how_many=' + how_many;
        <?}else{?>
            window.location.href = '/admin/orders?how_many=' + how_many;
        <?}?>
    });

    $('#brand').on('change', function(){
        var brand = $(this).val();
        if(brand != 'all'){
            window.location.href = '/admin/orders?brand='+brand;
        } else {
            window.location.href = '/admin/orders';
        }
    });

    $('body').on('change', '.sort', function(){
        var status=$(this).val();
        var multipleStatus = status.join(',').replace( /,/g, '%2c');
        if(status=="all"){
            window.location.href = '/admin/orders';
        }
        else{
            window.location.href = '/admin/orders?status_show='+multipleStatus;
        }
    });


 $('body').on('click', '#change_mpn', function(){


   $("#search_by_mpn").show();
  $("#search").hide();

  });

  $('body').on('click', '.refresh_fraud_status', function(){


   $("#search_by_mpn").show();
  $("#search").hide();

  });


 $('body').on('click', '#change_ids', function(){

   $("#search").show();
   $("#search_by_mpn").hide();

  });

   });






</script>
<?php if (count($model->orders)>0) { ?>
<div class="box box-table">
<table  id="data-list" class="table">
  <thead>
    <tr>
      <th width="5%"></th>
      <th>
<?php
if (isset($_GET['sort']) && ($_GET['sort']=='id'))
  {

  if ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'asc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=id&sort_param=desc">ID</a>';
    }
  elseif ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'desc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=id&sort_param=asc">ID</a>';
    }
  }
  else
  {
  echo '<a style="color:green;" href="/admin/orders?sort=id&sort_param=desc">ID</a>';
  } ?>
      </th>
      <th>
<?php
if (isset($_GET['sort']) && ($_GET['sort']=='insert_time'))
  {
  if ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'asc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=insert_time&sort_param=desc">Date</a>';
    }
  elseif ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'desc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=insert_time&sort_param=asc">Date</a>';
    }
  }
  else
  {
  echo '<a style="color:green;" href="/admin/orders?sort=insert_time&sort_param=desc">Date</a>';
  } ?>
      </th>
      <th>Tracking #  </th>
      <th>
                <?php
if (isset($_GET['sort']) && ($_GET['sort']=='status'))
  {
  if ($_GET['sort'] == 'Status' && $_GET['sort_param'] == 'asc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=status&sort_param=desc">Status</a>';
    }
  elseif ($_GET['sort'] == 'status' && $_GET['sort_param'] == 'desc')
    {
    echo ' <a  style="color:green;" href="/admin/orders?sort=status&sort_param=asc">Status</a>';
    }
  }
  else
  {
  echo '<a style="color:green;" href="/admin/orders?sort=status&sort_param=desc">Status</a>';
  } ?>


        <?$statusList = ['Canceled', 'Complete', 'Declined', 'Delivered','New','Paid','Processed','Returned','Shipped'];
        if(isset($_GET['status_show'])){
            $explodedList = explode('%2c',$_GET['status_show']);
        }?>
        <select multiple class="sort">
        <option value="all">Select all</option>
            <?foreach($statusList as $status){?>
                <option <?if(isset($explodedList)){if(in_array($status, $explodedList)){echo"selected";}}?> value="<?=$status?>"><?=$status?></option>
            <?}?>
         </select></th>
      <th>Bill Name </th>
      <th>Brands
        <select id="brand">
            <option value="all">Select All</option>
            <?foreach($model->brands as $brand){?>
                <option <?if(isset($_GET['brand'])){if($_GET['brand']==$brand->id){echo"selected";}}?> value="<?=$brand->id?>"><?=strtoupper($brand->name)?></option>
            <?}?>
        </select>
    </th>
      <th >Products MPN</th>
      <th >
        <?php
if (isset($_GET['sort']) && ($_GET['sort']=='total'))
  {
  if ($_GET['sort'] == 'total' && $_GET['sort_param'] == 'asc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=total&sort_param=desc">Total</a>';
    }
  elseif ($_GET['sort'] == 'total' && $_GET['sort_param'] == 'desc')
    {
    echo ' <a style="color:green;" href="/admin/orders?sort=total&sort_param=asc">Total</a>';
    }
  }
  else
  {
  echo '<a  style="color:green;" href="/admin/orders?sort=total&sort_param=desc">Total</a>';
  } ?>
       </th> 
    </tr>
  </thead>
  <tbody>
    <?php foreach($model->orders as $obj) { ?>
    <tr class="originalProducts">
      <td>
      	<? if (is_null($obj->viewed) || !$obj->viewed) { ?>
      		<button type="button" class="btn btn-success" disabled="disabled">New</button>
      	<? } ?>
      </td>
      <td>
      	<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
      		<?php echo $obj->id;?>
      	</a>
      </td>
      <td>
      	<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
      		<?php echo $obj->insert_time;?>
      	</a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
          <?php echo $obj->tracking_number;?>
        </a>
      </td>
      <td>

       <?=$obj->status?>
      </td>

      <td>
        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
		<?$shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
		$obj->ship_country,  $obj->ship_city, $obj->ship_state, $obj->ship_zip);


		$blng = array($obj->bill_first_name,$obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
		$obj->bill_country,  $obj->bill_city, $obj->bill_state, $obj->bill_zip);


		 if (count(array_diff ($shp, $blng))==0){?><?php echo $obj->bill_first_name.' '. $obj->bill_last_name;?>
		 <?}else{?>
         <button type="button" class="btn btn-success" style="position:relative;background:red;" disabled="disabled"> <?php echo $obj->bill_first_name.' '. $obj->bill_last_name;?></button>
		 <?}?>
        </a>
      </td>
       <td>
        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
          <? foreach($model->products[$obj->id] as $key=>$product) { ?>
            <?=$product->brand_name?> <?=($key<(count($model->products[$obj->id])-1))?'<br />':''?>
          <? } ?>
        </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
		<?$i=0;?>
          <? foreach($model->products[$obj->id] as $key=>$product) { ?>
			<?$i++;?>
          	<?=$product->mpn?> <?=($key<(count($model->products[$obj->id])-1))?'<br />':''?>
          <? } ?>

        </a>
		<?if ($i>1){?> <button type="button" class="btn btn-success" style="position:relative;"disabled="disabled"><?=$i?></button><?}?>
      </td>

      <td>
        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
		<?if ($obj->payment_method==2){?>
           <button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">$<?php echo number_format($obj->total, 2);?></button>
		<?}else{?>$<?php echo number_format($obj->total, 2);}?>
        </a>
      </td>

    </tr>
    <?php } ?>
  </tbody>
</table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php }else{echo"<center>No matches!<br><a href='' onclick='history.go(-1)'>Go back!</a></center>";} ?>

<?php footer();?>

<script>
	var site_url = '/admin/orders?<?if (isset($_GET['status_show'])){echo "status_show=";echo $_GET['status_show'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function(){
        $("#search").keyup(function(){
            var url = "<?php echo ADMIN_URL; ?>orders/search";
            var keywords = $(this).val();
            if(keywords.length > 0) {
                $.get(url, {keywords:keywords}, function(data){
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                        var list = JSON.parse(data);
                        for (key in list){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html( );
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                          $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);

                        $('<td />').appendTo(tr).html("$"+list[key].total);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

<script type="text/javascript">
    $(function(){
        $("#search_by_mpn").keyup(function(){
            var url = "<?php echo ADMIN_URL; ?>orders/search_by_mpn";
            var keywords = $.trim($(this).val());
            if(keywords.length > 0) {
                $.get(url, {keywords:keywords}, function(data){

                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                        var list = JSON.parse(data);
                        for (key in list){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html( );
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html("$"+list[key].total);
                     /*   var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');*/
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

