<?php
$gender = [1=>'Unisex',2=>"Men",3=>"Women"];
?>

<form class="form" action="<?=ADMIN_URL?>products/multi_update" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->q;?>" />
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
      <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
      <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
      <li role="presentation"><a href="#collections-tab" aria-controls="collections" role="tab" data-toggle="tab">Collections</a></li>
      <li role="presentation"><a href="#materials-tab" aria-controls="materials" role="tab" data-toggle="tab">Materials</a></li>
      <!-- <li role="presentation" style="display:none;"><a href="#video-tab" aria-controls="video" role="tab" data-toggle="tab">Video</a></li> -->
    <?php if($model->product->id>0) { ?>
      <!-- <li role="presentation" style="display:none;"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li> -->
    <?php } ?>
      <!-- <li role="presentation"><a href="#notes-tab" aria-controls="notes" role="tab" data-toggle="tab">Notes</a></li> -->
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general-tab">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                      <input name="name" type="text" value=""/>  
                    </div>
                    
                  <!--   <div class="form-group">
                      <label>Featured image</label>
                      <p><small>(ideal featured image size is 500 x 300)</small></p>
                      <p><input type="file" name="featured_image" class='image' /></p>
                      <div style="display:inline-block">
                      <?php 
                        $img_path = "";
                        if($model->product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$model->product->featured_image)){ 
                            $img_path = UPLOAD_URL . 'products/' . $model->product->featured_image;
                      ?>
	                      <div class="well well-sm pull-left">
	                          <img src="<?php echo $img_path; ?>" width="100" />
	                          <br />
	                          <a href="<?= ADMIN_URL.'products/delete_image/'.$model->product->id;?>?featured_image=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
	                          <input type="hidden" name="featured_image" value="<?=$model->product->featured_image?>" />
	                      </div>
                      <?php } ?>
                      <div class='preview-container'></div>
                      </div>
                  </div> -->
                    
                    <div class="form-group">
                        <label>Summary</label>
                        <textarea name="summary"></textarea>  
                    </div>
                     <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="ckeditor"></textarea>  
                    </div>
                    
                    <!-- <div class="form-group">
                        <label>SKU</label>
                        <?php echo $model->form->editorFor("sku"); ?>
						
                    </div> -->
                    <div class="form-group">
                        <label>UPC</label>
                        <input name="upc" type="text" />
                    </div>
                    <div class="form-group">
                        <label>MPN</label>
                       <input name="mpn" type="text" />      
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                         <input name="quantity" type="text" />   
                    </div>
                    <div class="form-group">
                      <label>Brand</label>
                      <select name="brand">
                      	<option value="0">Please select a brand</option>
                      <?php foreach($model->brands as $b) {?>
                        <option value="<?php echo $b->id;?>"><?php echo $b->name;?></option>
                      <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Color</label>
                      <select name="color">
                      	<option value="0">Please select a color</option>
                      <?php foreach($model->colors as $c) { ?>
                        <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                      <?php } ?>
                      </select>
                    </div>
                     <div class="form-group">
                      <label>Gender</label>
                      <?= $model->form->dropDownListFor('gender', $gender); ?>
                    </div>
                     <div class="form-group">
                        <label>Manufacturer</label>
                        <input name="manufacturer" type="text" />   
                    </div>
                    <div class="form-group">
                        <label>Featured?</label>
                      <input name="featured" type="checkbox" value="1"  />
                    </div>
					<div class="form-group">
                        <label>Final sale?</label>
                       <input name="final_sale" type="checkbox" value="1"  />     
                    </div>
                     <div class="form-group">
                        <label>Bestseller</label>
                        <?php echo $model->form->checkBoxFor("bestseller",1); ?>
                    </div>
                    <div class="form-group">
                      <label>Option</label>
                      <select name="day_delivery"> 
						<option value="2" <?php echo "selected";?>>Active</option>
						<option value="1">In stock</option>
						<option value="0" >Inactive</option>
                      </select>
                    </div>
                </div>
            </div>
          <div class="col-md-12">
                <div class="box">
                    <h4>Prices</h4>
                    <div class="form-group">
                        <label>Price</label>
                      <input name="price" type="text" />    
                    </div>
                    <div class="form-group" style="display:none;">
                        <label>List Price</label>
                         <input name="list_price" type="text" />   
                    </div>
                    <div class="form-group">
                        <label>Wholesale Price</label>
                        <input name="wholesale_price" type="text" />   
                    </div>
                    <div class="form-group">
                        <label>MSRP</label>
                       <input name="msrp" type="text" />    
                    </div>
                    <div class="form-group" style="display:none;">
                        <label>MSRP Enabled?</label>
                       <input name="msrp_enabled" type="checkbox" value="1"  />
                    </div>
                </div>
          </div>
          <div class="col-md-12">
                <div class="box">
                    <h4>Size</h4>
                    <div class="form-group">
                        <label>Length</label>
                       <textarea name="length"></textarea>   
                    </div>
                     <div class="form-group">
                        <label>Width</label>
                       <textarea name="width"></textarea>  
                    </div>
                     <div class="form-group">
                        <label>Height</label>
                        <textarea name="height"></textarea>        
                    </div>
                </div>
          </div>
          <?php if(count($model->product_questions)>0) { ?>
          <div class="col-md-12">
                <div class="box">
                    <h4>Questions</h4>
                    <table id="data-list" class="table">
                        <thead>
                            <tr>
                                <th width="30%">Date</th>
                                <th width="60%">Subject</th>
                                <th width="10%">View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($model->product_questions as $obj){ ?>
                            <tr>
                                <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a></td>
                                <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?=$obj->subject?></a></td>
                                <td class="text-center">
                                    <a class="btn-actions" href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>">
                                        <i class="icon-eye"></i> 
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="seo-tab">
         <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>SEO</h4>
                    <div class="form-group">
                        <label>Slug</label>
                      <input name="slug" type="text" disabled="disabled" />   
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                     <input name="tags" type="text" />   
                    </div>
                    <div class="form-group">
                        <label>Meta Title</label>
                       <input name="meta_title" type="text" />  
                    </div>
                     <div class="form-group">
                        <label>Meta Keywords</label>
                        <input name="meta_keywords" type="text" />     
                    </div>
                     <div class="form-group">
                        <label>Meta Description</label>
                      <input name="meta_description" type="text" />        
                    </div>
                </div>
            </div>
         </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="categories-tab">
        <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
          <?php  foreach($model->categories as $cat) { ?>
          <option value="<?php echo $cat->id;?>"><?php echo $cat->name;?></option>
          <?php } ?>
        </select>
      </div>
      <div role="tabpanel" class="tab-pane" id="collections-tab">
        <select name="product_collection[]" class="multiselect" data-placeholder="Collections" multiple="multiple">
          <?php foreach($model->collections as $col) { ?>
          <option value="<?php echo $col->id;?>"><?php echo $col->name;?></option>
          <?php } ?>
        </select>
      </div>
      <div role="tabpanel" class="tab-pane" id="materials-tab">
         <select name="product_material[]" class="multiselect" data-placeholder="Materials" multiple="multiple">
          <?php foreach($model->materials as $obj) { ?>
          <option value="<?php echo $obj->id;?>"><?php echo $obj->name;?></option>
          <?php } ?>
        </select>
      </div>
      <!-- <div role="tabpanel" class="tab-pane" id="video-tab">
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <div class="form-group">
                <label>Video Url</label>
                 <input name="video_url" type="text" /> 
              </div>
              
              <div class="form-group">
                      <label>Video Thumbnail</label>
                      <p><small>(ideal featured image size is 1920 x 300)</small></p>
                     


                      <p><input type="file" name="video_thumbnail" class='image' /></p>
                      <div style="display:inline-block">
                      <?php 
                      $img_path = "";
                      if($model->product->video_thumbnail != "" && file_exists(UPLOAD_PATH.'video_thumbnails'.DS.$model->product->video_thumbnail)){ 
                        $img_path = UPLOAD_URL . 'video_thumbnails/' . $model->product->video_thumbnail;
                        ?>
                      <div class="well well-sm pull-left">
                          <img src="<?php echo $img_path; ?>" width="100" />
                          <br />

                          <a href="<?= ADMIN_URL.'products/delete_image/'.$model->product->id;?>?type=thumbnail&token_id=<?php echo get_token();?>" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>

                      </div>
                      <?php } ?>
                      <div id='preview-thumbnail-container'></div>
                      </div>
                  </div>
              
            </div>
          </div>
        </div>
      </div> -->
      <?php if($model->product->id>0) { ?>
      <div role="tabpanel" class="tab-pane" id="images-tab">
        <div class="row">
          <div class="col-md-24">
            <div class="box">
        <div class="dropzone" id="dropzoneForm" action="<?php echo ADMIN_URL.'products/upload_images/'.$model->product->id;?>">
          
        </div>
        <button id="upload-dropzone" class="btn btn-danger">Upload</button><br />
        </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <table id="image-container" class="table table-sortable-container">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>File Name</th>
                    <th>Display Order</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($model->product_images as $pimg) { 
                   
                    if($pimg->exists_image()) {
                    ?>
                  <tr data-image_id="<?php echo $pimg->id;?>">
                    <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100" /></td>
                    <td><?php echo $pimg->image;?></td>
                    <td class="display-order-td"><?php echo $pimg->display_order;?></td>
                    <td class="text-center">
                      <a class="btn-actions delete-product-image" href="<?php echo ADMIN_URL; ?>products/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token();?>">
                        <i class="icon-cancel-circled"></i> 
                      </a>
                    </td>
                  </tr>
                  <?php 
                    }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- <div role="tabpanel" class="tab-pane" id="notes-tab">
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <h4>New Note</h4>
              <div class="form-group">
                <label>Title</label>
                 <input name="note[name]" type="text" />
              </div>
              <div class="form-group">
                <label>Text</label>
               <textarea name="note[description]"></textarea>
              </div>
            </div>
          </div>
        </div>
        <?php foreach($model->product_notes as $note) { ?>
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <h4><?=$note->name?></h4>
              <h5><?=$note->insert_time?> - <b><?=$note->admin->first_name.' '.$note->admin->last_name?></b></h5>
              <p><?=$note->description?></p>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>  -->     
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">

  var categories = <?php echo json_encode($model->product_categories); ?>;
    var collections = <?php echo json_encode($model->product_collections); ?>;
    var materials = <?php echo json_encode($model->product_materials); ?>;
  //console.log(categories);
  var site_url=<?php echo json_encode(ADMIN_URL.'products/'); ?>;
  $(document).ready(function() {
    //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
    var video_thumbnail = new mult_image($("input[name='video_thumbnail']"),$("#preview-thumbnail-container"));
    	
    	function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				var img = $("<img />");
				reader.onload = function (e) {
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
				};
				$(input).parent().parent().find('.preview-container').html(img);
				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
		});	
        

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});
        
  $("select.multiselect").each(function(i,e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText:placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
          $("select[name='product_category[]']").val(categories);
          $("select[name='product_collection[]']").val(collections);
          $("select[name='product_material[]']").val(materials);
          $("select.multiselect").multiselect("rebuild");
          
          function sort_number_display() {
        var counter = 1;
         $('#image-container >tbody>tr').each(function(i,e) {
           $(e).find('td.display-order-td').html(counter);
           counter++;
         });
      }
      $("a.delete-product-image").on("click",function() {
        if(confirm("are you sure?")) {
          location.href=$(this).attr("href");
        } else {
          return false;
        }
      });
      $('#image-container').sortable({
          containerSelector: 'table',
          itemPath: '> tbody',
          itemSelector: 'tr',
          placeholder: '<tr class="placeholder"/>',
          onDrop: function ($item, container, _super, event) {
            
              if($(event.target).hasClass('delete-product-image')) {
                  $(event.target).trigger('click');
              } 

              var ids = [];
              var tr_containers = $("#image-container > tbody > tr");
              tr_containers.each(function(i,e) {
                  ids.push($(e).data("image_id"));

              });
              $.post(site_url+'sort_images', {ids: ids}, function(response){
                sort_number_display();
              });
              _super($item);
          }
        });
      
      });
</script>
<script type="text/javascript">
Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,
  //url: <?php //echo json_encode(ADMIN_URL.'products/upload_images/'.$model->product->id);?>,
  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    $("#upload-dropzone").on("click", function(e) {
      // Make sure that the form isn't actually being sent.
      e.preventDefault();
      e.stopPropagation();
      myDropzone.processQueue();
    });

    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sendingmultiple", function() {
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
      $("#upload-dropzone").prop("disabled",true);
    });
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      window.location.reload();
      $("#upload-dropzone").prop("disabled",false);
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }

}
</script>
