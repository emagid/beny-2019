<?php if (count($model->stores) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="21%">Name</th>
                    <th width="21%">Description</th>
                    <th width="21%">Notation</th>
                    <th width="21%" style="text-align: center;">Edit</th>
                    <th width="21%" style="text-align: center;">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;
                foreach ($model->stores as $obj) { ?>
                    <tr class="originalProducts">
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>store/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>store/update/<?php echo $obj->id; ?>"><?php echo $obj->description?:'-'; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>store/update/<?php echo $obj->id; ?>"><?php echo \Model\Notation::getItem($obj->notation_id)->name?:'-'; ?></a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions" href="<?php echo ADMIN_URL; ?>store/update/<?php echo $obj->id; ?>">
                                <i class="icon-pencil"></i>
                           </a>
                         </td>
                        <td class="text-center">
                            <a class="btn-actions" href="<?php echo ADMIN_URL; ?>store/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                                <i class="icon-cancel-circled"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

</form>

<?php echo footer(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#data-list').DataTable();
    })
</script>
































