<link href="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.min.css" />
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->store->id; ?>"/>
    <input type="hidden" name="redirectTo" value="<?= $_SERVER['HTTP_REFERER'] ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>store/delete/<?php echo $model->store->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

                            <div class="form-group">
                                <label>Description</label>

                                <?php echo $model->form->editorFor("description"); ?>
                            </div>

                            <div class="form-group">
                                <label>Notation</label>
                                <select name="notation_id" class="form-control">
                                    <option value="0">None</option>
                                    <?php foreach($model->notations as $notation) {
                                        $select = ($notation->id == $model->store->notation_id) ? " selected='selected'": "";
                                        ?>
                                        <option value="<?php echo $notation->id; ?>" <?php echo $select;?> ><?php echo $notation->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Image</label>

                                <p>
                                    <small style="color:#A81927"><b></b></small>
                                </p>
                                <p><input type="file" name="image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->store->image != "" && file_exists(UPLOAD_PATH . 'stores/' . $model->store->image)) {
                                        $img_path = UPLOAD_URL . 'stores/' . $model->store->image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <input type="hidden" name="image"
                                                   value="<?= $model->store->image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>
