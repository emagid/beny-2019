<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li>
      <?php if($model->user->id>0) { ?> 
	      <li role="presentation"><a href="#addresses-tab" aria-controls="seo" role="tab" data-toggle="tab">Delivery Addresses</a></li>
	      <li role="presentation"><a href="#payment-info-tab" aria-controls="categories" role="tab" data-toggle="tab">Payment</a></li>
	      <? if (isset($model->orders) and count($model->orders) > 0) { ?>
	      	<li role="presentation"><a href="#orders-tab" aria-controls="collections" role="tab" data-toggle="tab">Orders</a></li>
	      <? } ?>

          <li role="presentation"><a href="#wishlist-tab" aria-controls="collections" role="tab" data-toggle="tab">Wish list</a></li>
         
      <?php } ?>
    </ul>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="account-info-tab">
          <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
          <input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
          <input type=hidden name="token" value="<?php echo get_token(); ?>" />
          <div class="row">
              <div class="col-md-12">
                  <div class="box">
                      <h4>Account Information</h4>
                      <div class="form-group">
                        <label>Email Address</label>
                       <?php echo $model->user->email;?>
                      </div>
                      <div class="form-group">
                        <label>First Name</label>
                        <?php echo $model->user->first_name;?> 
                      </div>
                      <div class="form-group">
                        <label>Last Name</label>
                        <?php echo $model->user->last_name;?>  
                      </div>
                      <div class="form-group">
                        <label>Password (leave blank to keep current password)</label>
                        <input type="password" name="password" />
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-save">Save</button>
                      </div>
                  </div>
              </div>
          </div>
          
        </form>
      </div>
      <?php if($model->user->id>0) { ?>
      <div role="tabpanel" class="tab-pane" id="addresses-tab">
      	<div class="row">
      		<div class="col-md-24">
      			<div class="box">
        			<a href="<?php echo ADMIN_URL.'users/address/-1?user_id='.$model->user->id;?>" class="btn btn-primary btn-add">Add New Address</a>
        		</div>
        	</div>
    	</div>
        <div class="row">
       <?php foreach($model->addresses as $addr) { ?>
          <div class="col-md-6">
            <div class="box">
              <div class="form-group">
                <label>Label: </label>
                <?php echo $addr->label;?>
              </div>
              <div class="form-group">
                <label>First Name: </label>
                <?php echo $addr->first_name;?>
              </div>
              
              <div class="form-group">
                <label>Last Name: </label>
                <?php echo $addr->last_name;?>
              </div>
              <?php if ($addr->phone!="") { ?>
              <div class="form-group">
                <label>Phone Number: </label>
                <?php echo $addr->phone;?>
              </div>
              <?php } ?>
              <div class="form-group">
                <label>Address: </label>
                <?php echo $addr->address;?>
              </div>
              
              <div class="form-group">
                <label>Address 2: </label>
                <?php echo $addr->address2;?>
              </div>
              
              <div class="form-group">
                <label>City: </label>
                <?php echo $addr->city;?>
              </div>
              
              <div class="form-group">
                <label>State: </label>
                
                <?php echo $states[$addr->state];?>
              </div>
              
              <div class="form-group">
                <label>Zip Code: </label>
                <?php echo $addr->zip;?>
              </div>
              
              <div class="form-group">
                <label>Country: </label>
                <?php echo $addr->country;?>
              </div>
              
              <div class="form-group">
                 <a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/address/<?php echo $addr->id; ?>?user_id=<?php echo $model->user->id;?>">
		           <i class="icon-pencil"></i> 
		         </a>
                 <a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/address_delete/<?php echo $addr->id; ?>?token_id=<?php echo get_token();?>?user_id=<?php echo $model->user->id;?>" onClick="return confirm('Are You Sure?');">
		            <i class="icon-cancel-circled"></i> 
		         </a>
              </div>
            </div>
          </div>
       <?php } ?>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="payment-info-tab">
      	<div class="row">
      		<div class="col-md-24">
      			<div class="box">
        			<a href="<?php echo ADMIN_URL.'users/payment/-1?user_id='.$model->user->id;?>" class="btn btn-primary btn-add">Add Payment Method</a>
        		</div>
        	</div>
        </div>
        <div class="row">
          <?php foreach($model->payment_profiles as $payment_profile) { ?>
          <div class="col-md-6">
            <div class="box">
              <div class="form-group">
                <label>Label: </label>
                <?php echo $payment_profile->label;?>
              </div>
              <div class="form-group">
                <label>First Name: </label>
                <?php echo $payment_profile->first_name;?>
              </div>
              <div class="form-group">
                <label>Last Name: </label>
                <?php echo $payment_profile->last_name;?>
              </div>
              <?php if ($payment_profile->phone!="") { ?>
              <div class="form-group">
                <label>Phone Number: </label>
                <?php echo $payment_profile->phone;?>
              </div>
              <?php } ?>
              <div class="form-group">
                <label>Address: </label>
                <?php echo $payment_profile->address;?>
              </div>
              <div class="form-group">
                <label>Address 2: </label>
                <?php echo $payment_profile->address2;?>
              </div>
              <div class="form-group">
                <label>City: </label>
                <?php echo $payment_profile->city;?>
              </div>
              <div class="form-group">
                <label>State: </label>
                <?php echo $states[$payment_profile->state];?>
              </div>
              <div class="form-group">
                <label>Zip Code: </label>
                <?php echo $payment_profile->zip;?>
              </div>
              <div class="form-group">
                <label>Country: </label>
                <?php echo $payment_profile->country;?>
              </div>

              <div class="form-group">
                <label>Credit Card Number: </label>
                <?php echo $payment_profile->cc_number;?>
              </div>
              <div class="form-group">

              </div>
              
              <div class="form-group">
                 <a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/payment/<?php echo $payment_profile->id; ?>?user_id=<?php echo $model->user->id;?>">
		           <i class="icon-pencil"></i> 
		         </a>
                 <a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/payment_delete/<?php echo $payment_profile->id; ?>?token_id=<?php echo get_token();?>?user_id=<?php echo $model->user->id;?>" onClick="return confirm('Are You Sure?');">
		            <i class="icon-cancel-circled"></i> 
		         </a>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
	      <? if (isset($model->orders) and count($model->orders) > 0) { ?>
	      <div role="tabpanel" class="tab-pane" id="orders-tab">
	      	<div class="box">
	        <table class="table">
			  <thead>
			    <tr>
			      <th>Order #</th>
			      <th>Tracking #</th>
			      <th>Status</th>
			      <th>Bill Name</th>
			      <th>Total</th>
			      <th>View</th>
			    </tr>
			  </thead>
			  <tbody>
			    <?php foreach($model->orders as $obj) { ?>
			    <tr>
			      <td>
			      	<a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			      		<?php echo $obj->id;?>
			      	</a>
			      </td>
			      <td>
			        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			          <?php echo $obj->tracking_number;?>
			        </a>
			      </td>
			      <td>
			        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			          <?php echo $obj->status;?>
			        </a>
			      </td>
			      <td>
			        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			          <?php echo $obj->bill_first_name.' '. $obj->bill_last_name;?>
			        </a>
			      </td>
			      <td>
			        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			          $<?php echo number_format($obj->total, 2);?>
			        </a>
			      </td>
			      <td>
			         <a class="btn-actions" href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
			           <i class="icon-eye"></i> 
			         </a>
			      </td>
			    </tr>
			    <?php } ?>
			  </tbody>  
			</table>
			</div>
	      </div>
	      <? } ?>
        <div role="tabpanel" class="tab-pane" id="wishlist-tab">
          <div class="box">
          <table class="table">
        <thead>
          <tr>
             <th>Coupon</th>
            <th>MPN</th>
            <th>Product</th>
            <th>Price</th> 
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          
         
            
            <?foreach ($this->_viewData->wishlist as $Product) {?>
             <tr>
             <td>
              <select name="coupon" class="coupon_send" product="<?=$Product->id?>"  user="<?=$model->user->id?>">
                <option>Select coupon...</option>  
              <?foreach (\Model\Coupon::getList() as $coupon) {?>
               <option value="<?=$coupon->id;?>"><?=$coupon->name;?></option>  
              <?}?>
              </select>  
            </td>
              <td>
              <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $Product->id; ?>">
                <?php echo $Product->mpn;?>
              </a>
            </td>
            <td>
              <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $Product->id; ?>">
                 <?php echo $Product->name;?>
              </a>
            </td>
            <td>
              <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $Product->id; ?>">
                 <?php echo $Product->price;?>
              </a>
            </td>
            
           
            <td>
               <a class="btn-actions" href="/product/<?php echo $Product->slug; ?>">
                 <i class="icon-eye"></i> 
               </a>
            </td>
          </tr>
          <?}?>
          
          
        </tbody>  
      </table>
      </div>
        </div>
      <?php } ?>
    </div>
  </div>
<?php footer();?>

<script type="text/javascript">
  var site_url = "<?php echo ADMIN_URL;?>";
  var user_id = <?php echo $model->user->id;?>;
  jQuery.validator.addMethod("validate_user_email",function(value,element) {
    var return_val = false;

    var data = {email:value,id: user_id};
    $.ajax({
     url: site_url+"users/validate_user_email",
     async: false,
     data: data,
     method: "GET",
     success: function(response) {
       if(response.success) {
         return_val = true;
       }
     }
     
    });
    return return_val;
  },"Email is invalid or already being used.");
  

$("#user-form").validate({
  onkeyup: false,
  onfocusout: false,
  onclick: false,
  focusInvalid :false,
  rules: {
    email: {required:true},
    first_name: {required:true},
    last_name: {required:true},
  },
});

   
        $('.coupon_send').change(function() {
            var product_id = $(this).attr("product");
            var user_id = $(this).attr("user");
            var coupon_id = $(this).val();
             
      

            $.ajax({
                url: '/admin/users/send_coupon/',
                method: 'POST',
                data: {
                    product_id: product_id, user_id:user_id, coupon_id: coupon_id 
                },
                success: function(data) {
                  alert(data);

                 /* window.location.replace("/checkout/");*/
                }
            });
        });

$(".phone-us").inputmask("(999)999-9999");
</script>