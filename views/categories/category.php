<?
	if (is_null($model->category->banner) || $model->category->banner == ""){
		$banner = '/content/frontend/img/banner_new_arrivals.jpg';
	} else {
		$banner = UPLOAD_URL.'categories/1200_344'.$model->category->banner;
	}
?>


<!-- Banner -->
<div class="banner default" style="background-image:url('<?=$banner?>');">
	<div class="container">
		<? if ($banner == '/content/frontend/img/banner_new_arrivals.jpg'){ ?>
			<div class="inner">
				<h1> <?=$model->category->title!=''?$model->category->title: $model->category->name?> </h1>
			</div>
		<? } ?>
	</div>
</div>

<? require_once(ROOT_DIR.'templates/'.$this->template.'/filters.php'); ?>

<?php if (isset($model->products)) { ?>
<!-- Items -->
<div class="container">
	<div class="brand_items">
		<div class="category">

			<div class="row">


				<?php foreach($model->products as $product) { ?>


				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
					<div class="product brand_product">
						
							<?php 
								$img_path = UPLOAD_URL . 'products/' . $product->featuredImage();
					        ?>
						<a href="<?=SITE_URL.'products/'.$product->slug?>">
					        <div class="product-img-holder" style="background-image:url(<? echo $img_path; ?>);">
					        	<img src="<?php echo $img_path; ?>" />
					        </div>
						</a>
						<div class="inner">
							
							<h2>
								<a href="<?=SITE_URL.'products/'.$product->slug?>"><?=$product->alias!=''?$product->alias:$product->name?></a>
							</h2>	
							<span itemprop="model" class="model"><?=$product->mpn?></span>
							<p itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<span itemprop="price">$<?=number_format($product->price, 2)?></span>
						</p>
						</div>
						<div class="hover">
						<a class="btn" href="<?=SITE_URL.'products/'.$product->slug?>">
							View Details
						</a>
						<h2>
							<a href="product.php">
								<!-- Corum Admiral's Cup Legend 38 -->
							</a>
						</h2>
						<!-- <span class="model">A082-01272</span> -->
						<p>
							<!-- $8,850.00 -->
						</p>
					</div>
					</div>
				</div>

				<?php } ?>
				<?php } ?>	

			</div>	
		</div>	
	</div>
	<nav class="text-right">
		<ul class="pagination">
		</ul>
	</nav>
</div>

<? footer(); ?>
<script>
var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
if(params instanceof Array) {
	params = {};
}
var site_url = '<?= SITE_URL."category/".$model->category->slug;?>';
$(function() {
	$('ul.pagination').pagination({
		pages: <?=$model->pagination->total_pages?>,
		currentPage: <?=$model->pagination->current_page_index + 1?>,
		cssStyle: 'light-theme',
		onPageClick: function(pageNumber,event) {
			var url_params = params || {};
			url_params.page = parseInt(pageNumber);
			var full_url = site_url;
			build_url(full_url,url_params,true);			
		}
	});
});
function build_url(url,params,redirect) {
	var params_arr = [];
	$.each(params,function(i,e) {
		params_arr.push(i+"="+e);
	});
	if(redirect) {
		window.location.href = url + "?"+params_arr.join("&");
		return false;
	} else {
		return url + "?"+params_arr.join("&");
	}
}
</script>

