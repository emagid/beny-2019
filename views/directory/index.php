<body class='directory_page'>
    <div class="return_home">
        <a href="/">
            <img src="<?=FRONT_IMG?>home.png">
        </a>
    </div>

    <div class="home direct" style="width: 100%; height: 1920px">
        <?php include __DIR__."/../../templates/modernvice/directory.php";?>
    </div>

    <div class='info'>
        <img id='img' src="<?=FRONT_IMG?>bike_hike.png">
        <p id='name'></p>
    </div>
</body>
<!-- END HOME PAGE -->

<?php echo footer(); ?>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript">

    $('.booth').click(function(){
        $('.info').hide();
    });

    $('.booth').click(function(){
        $('.info').removeClass('right');
        var name = $(this).attr('data-name');
        var img = "<?=FRONT_IMG?>sponsors/" + $(this).children('text').html();
        var x = $(this)[0].getBoundingClientRect().x;
        var y = $(this)[0].getBoundingClientRect().y;
        var h = $(this)[0].getBoundingClientRect().height;
        var w = $(this)[0].getBoundingClientRect().width;


        if ( name ) {
            $('.info').fadeIn(500);
            $('.info #name').html(name);
            $('.info #img').attr('src', img + ".png");
            if ( x > 680 ) {
                x += w;
                $('.info').addClass('right');
            }
            $('.info').css({"top": y + h, "left":  x});
        }else {
            $('info').fadeOut(500);
        }
    });

    $('svg').click(function(e){
        if ( !$(e.target).hasClass('booth') ) {
            if ( $(e.target).parents('.booth').length === 0 ) {
                $('.info').fadeOut(500);
            }
        }
    });
</script>
