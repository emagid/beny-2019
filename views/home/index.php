<body>
    <div class='bike_background'>
        <video class='active' id='1' src='<?=FRONT_IMG?>vid1.mov' type='video/mov' autoplay muted></video>
        <video id='2' src='<?=FRONT_IMG?>vid2.mov' type='video/mov' muted></video>
        <video id='3' src='<?=FRONT_IMG?>vid3.mov' type='video/mov' muted></video>
    </div>
    <section class='new_home_page'>
        <div class='home_section'>
            <h1>Bike Expo New York</h1>
            <div class='links'>
                <a href='/directory' class='link'>
                    <img src="<?=FRONT_IMG?>icon_map.png">
                    <p>Expo Booth Map</p>
                </a>
                <a href="/pages/hub" class='link'>
                    <img src="<?=FRONT_IMG?>icon_hub.png">
                    <p>The Hub</p>
                </a>
                <a href="/pages/food" class='link'>
                    <img src="<?=FRONT_IMG?>icon_food.png">
                    <p>Food Trucks & Beer Garden</p>
                </a>
            </div>
        </div>

        <div class='home_section'>
            <h1>TD Five Boro Bike Tour</h1>
            <div class='links'>
                <a href="/pages/start" class='link'>
                    <img src="<?=FRONT_IMG?>icon_start.png">
                    <p>Start Map</p>
                </a>
                <!-- <a href="/map/start" class='link'> -->
                <a href="/pages/route" class='link'>
                    <img src="<?=FRONT_IMG?>icon_route.png">
                    <p>Route Map</p>
                </a>
                <a href="/regulations" class='link'>
                    <img src="<?=FRONT_IMG?>icon_bag.png">
                    <p>Bag Restrictions</p>
                </a>
            </div>
        </div>

        <div class='home_section'>
            <h1>Bike New York</h1>
            <div class='links'>
                <a href='/home/information' class='link'>
                    <img src="<?=FRONT_IMG?>icon_info.png">
                    <p>Information</p>
                </a>
                <a href="/regionalrides" class='link'>
                    <img src="<?=FRONT_IMG?>icon_rides.png">
                    <p>Regional Rides</p>
                </a>
                <a href="/bikeinfo" class='link'>
                    <img src="<?=FRONT_IMG?>icon_member.png">
                    <p>Membership</p>
                </a>
            </div>
        </div>

        <a href="/home/photobooth">
            <img style='max-height: 180px;' class='photobooth_link' src="<?=FRONT_IMG?>home_photo.png">
        </a>

        <div class='slider'>
            <p>Thanks to our 2019 TD Five Boro Bike Tour Partners:</p>
            <div class='logo_holder'>
                <video autoplay muted loop src='<?=FRONT_IMG?>logoGif.mov' type='video/mov'></video>
                <div class='logos'></div>
            </div>
        </div>
        
    </section>
</body>



<?php echo footer(); ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('.bike_background').css('background-position', '-700px');

        setTimeout(function(){
            $('.new_home_page').fadeIn(1000);
            $('.slider video').css('margin-left', '0');
            $('.slider .logo_holder').addClass('active');
        }, 1500);


        setTimeout(function(){
            $('.slider .logos, .slider p').fadeIn(1000);
        }, 2500);



      // / Choice click
      var timer;
        $(".directory ul li a").on({
             'click': function clickAction(e) {
              e.preventDefault();
                 var self = this;
                 var link = $(this).attr("href");
                    $(self).children('.square').css('transform', 'scale(.9)');
                  timer = setTimeout(function () {
                      $(self).children('.square').css('transform', 'scale(1)');
                  }, 100);
                  timer = setTimeout(function () {
                      window.location.href = link;
                  }, 500);
             }
        });


        var timer;
        $(".link, .photobooth").on({
             'click': function clickAction(e) {
              e.preventDefault();
                 var self = this;
                 var link = $(this).attr("href");
                    $('.link').fadeOut();
                    $('.photobooth_link').fadeOut();
                    $(self).show();
                    $('.new_home_page h1').fadeOut();
                    $(self).css('transform', 'scale(.9)');
                  timer = setTimeout(function () {
                      $(self).css('transform', 'scale(1)');
                  }, 200);
                  timer = setTimeout(function () {
                      window.location.href = link;
                  }, 500);
             }
        });


        var timer;
        $(".mailing").on({
             'click': function clickAction(e) {
              e.preventDefault();
                 var self = this;
                    $(self).css('transform', 'scale(.9)');
                  timer = setTimeout(function () {
                      $(self).css('transform', 'scale(1)');
                      $('.newsletter').fadeIn();
                      $('.newsletter').css('display', 'flex');
                  }, 100);
             }
        });

        $('.popup_drop').click(function(){
            $('.popup').fadeOut();
            $('.jQKeyboardContainer').fadeOut();
        });


        $('form#newsletter').submit(function(e){
            e.preventDefault();
            form = this
            $('.success').fadeIn();
            $('.success').css('display', 'flex');
            $('.jQKeyboardContainer').fadeOut();
            setTimeout(function(){
                form.submit();
            }, 4000);
        });

        $('video')[0].addEventListener('ended',myHandler,false);
        $('video')[1].addEventListener('ended',myHandler,false);
        $('video')[2].addEventListener('ended',myHandlerLast,false);


        function myHandler(e) {
            $('video').removeClass('active');
            var nxt = $(e.target).next('video');

            nxt.addClass('active');
            nxt[0].play();
            nxt[0].autoplay = true;
        }

        function myHandlerLast(e) {
            $('video').removeClass('active');
            var nxt = $($('video')[0]);

            nxt.addClass('active');
            nxt[0].play();
            nxt[0].autoplay = true;
        }
    });
</script>

<style type="text/css">
html,body, .full_view{
    height: 2500px;
    width: 100%;
    overflow: hidden;
}

.row {
    height: 1920px;
    overflow: hidden;
}

.bike-brand img {
    opacity: 0.8;
    width: 100px; 
    margin-left: 763px;
}
</style>
