<body>
  <div class="return_home">
        <a href="/">
            <img src="<?=FRONT_IMG?>home.png">
        </a>
    </div>
	<section class="home photobooth" >
        <!-- <link rel="stylesheet" href="<?=FRONT_CSS?>photobooth.css"> -->
        <!-- Begining animation -->


        <!-- background -->
        <div class='vid_overlay'></div>     
        <div class='vid' style='z-index: -1;'></div>

        <!-- home button -->
        <a href="/"><aside id='home_click_white' class='home_click'>
            <img class='white_img' src="<?=FRONT_IMG?>home.png"> 
        </aside></a>

        <!-- Header -->
<!--         <header>
            <a href="/"><img src="<?=FRONT_IMG?>webair.png"></a>
        </header> -->

		<body>

			<!--  ==========  PHOTOS  =============== -->
			<section id='photos' class='photos'>
                <img style='display: none;' id='logo' src="<?=FRONT_IMG?>webair.png">
                <img style='display: none;' id='background' src="<?=FRONT_IMG?>cover.png">

				<!-- Cam -->
                <h3 class='pic_text'>Click below and look up!</h3>
        				<video id="video" width="1080px" height="1700px" autoplay></video>
                <img class='filter' src="<?=FRONT_IMG?>filter.png">
                <device type="media" onchange="update(this.data)"></device>

                <script>
                  var video = document.getElementById('video');

                  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                      navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                          try {
                            video.src = window.URL.createObjectURL(stream);
                          } catch(error) {
                            video.srcObject = stream;
                          }
                      try {
                            video.play();
                      } catch (error) {
                        console.log(error)
                          }
                      });
                  }
              </script>

					<div id='snap_photo' class='camera_button'>					
						<img src="<?=FRONT_IMG?>pic_cam.png">
					</div>

                    <div id='snap_gif' class='camera_button'>                 
                        <img src="<?=FRONT_IMG?>gif_cam.png">
                    </div>

					<!-- countdown --><div class='countdown'>3</div>
					<!-- countdown --><div class='countdown'>2</div>
					<!-- countdown --><div class='countdown'>1</div>
					<!-- flash --><div class='flash'></div>
			</section>

			<!-- Choosing pictures -->
			<div id='pictures'>
                <!-- <img id='pen' src="<?= FRONT_ASSETS ?>img/draw.png"> -->

                <div class='draw_ctl drawing'>
                    <i class="fa fa-close" style="font-size:36px"></i>
                </div>

                <div class='colors drawing'>
                    <div class='color' id='colorRed'></div>
                    <div class='color' id='colorGreen'></div>
                    <div class='color' id='colorBlue'></div>
                    <div class='color' id='colorWhite'></div>
                    <div class='color' id='colorBlack'></div>
                </div>


                <div class='sizes drawing'>
                    <div class='size' id='small'></div>
                    <div class='size' id='medium'></div>
                    <div class='size' id='large'></div>
                    <div class='size' id='huge'></div>
                </div>

                 <!-- <div  id='canvasDiv'></div> -->


				<div class='button next'>NEXT</div>
				<p class='gif_info'>Choose 4 photos to create your GIF</p>
				<div class='button submit'>SHARE</div>
                <a href="/home/photobooth"><div class='button retake'>RETAKE</div></a>
				<div class='container gif_container'></div>
			</div>

			<!-- Showing Gif -->
			<section class='gif_show'>
                <img id='loader' src="<?=FRONT_IMG?>loader.gif">
				<div class='button submit'>SHARE</div>
			</section>

			<!-- Share overlay -->
            <!-- <i class="fa fa-close sharex" style="font-size:36px"></i> -->
			<section class='share_overlay'>
				<div class='container'>
					<form id='submit_form'>
						<input type='hidden' name='form' value="1">
                        <input type="hidden" name="image" class="image_encoded">
                        <span>
                            <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
                        </span>
                        <p id='add_email' style='color: #ffffffab;'>Add an email +</p>
                        <div class='line'></div>
                        <span>
                            <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
                        </span>
                        <p id='add_phone' style='color: #ffffffab;'>Add a phone number +</p>
						<input id='share_btn' class='button' type='submit' value='SHARE'>
                        <!-- <div class='button print'>PRINT</div> -->
					</form>
					<!-- <a href='/' id='done' class='button'>DONE</a> -->
				</div>
			</section>

			<!-- Alerts -->
			<section id='share_alert'>
                <h3>Thank you!</h3>
				<p>Your picture is on it's way</p>
			</section>

		</body>

        <section class='event_pics'>
        <div class='overlay'>
            <h2>EVENT PICTURES</h2>
            <!--  -->
            <div class='inner_content'>
                <!-- <div class='event_images choice'><h2>IMAGES</h2></div> -->
                    <div class='display_images'></div>
                <!-- <div class='event_gifs choice'><h2>GIFS</h2></div> -->
                    <!-- <div class='display_gifs'></div> -->
                <div id='even<!-- t_images' class='button'>IMAGES</div>
                <div id='event -->_gifs' class='button'>GIFS</div>
            </div>
        </div>
    </section>

  </section>

</body>
 <!-- <script type="text/javascript" src="<?= FRONT_JS ?>html5-canvas-drawing-app.js"></script>
    <script type="text/javascript">
         drawingApp.init();
    </script> -->
<?php echo footer(); ?>

            
<script type="text/javascript">
    $(document).ready(function(){
    // alert('hi')
       
        var images = <?= \Model\Snapshot_Contact::slider()?>; //array of image urls
        var gifs =   <?= \Model\Gif::slider()?>; //array of gif urls

        addMedia(images, gifs);


        $('#add_email').click(function(){
            $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
            $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#add_phone').click(function(){
            $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
            $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('.vid').fadeIn(1000);
        $('.vid_overlay').fadeIn(1000);

        setTimeout(function(){
            $('.pic_text').fadeIn(2000);
            $('#snap_photo').fadeIn(2000);
            $('#snap_gif').fadeIn(2000);
        }, 1000);

        setTimeout(function(){
            $('#video').fadeIn(1000);
        }, 2000);


        function addMedia(images, gifs) {

        for ( i=0; i<images.length; i++ ) {
          var div = "<img class='event_pic' src='" + images[i].replace('\\','/') + "'></div>";
          $('.event_pics .display_images').append(div);
        }
            
        for ( i=0; i<gifs.length; i++ ) {
              var div = "<img class='event_pic' src='" + gifs[i].replace('\\','/') + "'></div>";
            $('.event_pics .display_gifs').append(div);
        }
      }


      // ===============================================  PHOTOBOOTH CODE  ===============================================
    


       // Gif Next click
       function picAction( pic ) {
        $(pic).fadeIn('fast');
        $(pic).delay(1300).fadeOut(400);
      }

       function gif( pics ) {
        var offset = 0

        for ( i=0; i<2; i++ ) {
          pics.each(function(){
            var timer
            var self = this;
            timer = setTimeout(function(){
                picAction(self)
            }, 0 + offset);

            offset += 1500;
          });
        }

        setTimeout(function(){
              var gif_pics = $('.gif_show').children('.checked');
              gif( gif_pics )
          }, 1200);
      }

      
      

       // Share click
       $(document).on('click', '.submit', function(){
         
          if ( $('#draw').is(':visible') ) {
               saveDrawing();


              $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
                  $('#submit_form input[name=image]').val(data.image.id);
              });
          }

          $('.share_overlay').slideDown();
          $('.fa-close').fadeIn();
       });

        $('#share_btn').click(function(){
          $('.fa-close').fadeOut();
          $('.jQKeyboardContainer').fadeOut();
       })

       function saveDrawing() {
              
              var draw_canvas = $('#draw');
              $($(draw_canvas[0]).parents('#pictures')[0]).append(convertCanvasToImage(draw_canvas[0]))

              $('.drawing').hide();
          }
       


       // Form submit
       $('#submit_form').on('submit', function(e){
          e.preventDefault();
          $('#share_alert').slideDown();
          $('#share_alert').css('display', 'flex');
          $.post('/contact', $(this).serialize(), function (response) {
              if(response.status){
                setTimeout(function(){
                  $('#submit_form')[0].reset();
                  window.location.href = '/';
                }, 3000);
              }else {
                setTimeout(function(){
                  $('#submit_form')[0].reset();
                  window.location.href = '/';
                }, 3000);
              }
          });
       });


       // Click of done sharing
       $('#done').on('click', function(e){
          $('.share_overlay').slideUp();
          $('#jQKeyboardContainer').remove()
          // remove all images
          $('#pictures').children('.canvas_holder');
       });


       // click of showing images
       $('.event_images').on('click', function(){
          $('.choice').fadeOut();
          $('#event_gifs').fadeIn();
          $('.display_images').slideDown();
       });


        // click of showing gifs
       $('.event_gifs').on('click', function(){
          $('.choice').fadeOut();
          $('#event_images').fadeIn();
          $('.display_gifs').slideDown();
       });


        // click of showing gifs when images are shown
       $('#event_gifs').on('click', function(){
          $('#event_images').fadeIn();
          $('#event_gifs').fadeOut();
          $('.display_images').slideUp();
          setTimeout(function(){
              $('.display_gifs').slideDown();
          }, 1000)
       });

       // click of showing images when gifs are shown
       $('#event_images').on('click', function(){
          $('#event_gifs').fadeIn();
          $('#event_images').fadeOut();
          $('.display_gifs').slideUp();
          setTimeout(function(){
              $('.display_images').slideDown();
          }, 1000)
       });


       $('.print').click(function(){
            saveDrawing();  
            var source = $($('#pictures img:last-child')[1]).attr('src');
            VoucherPrint(source);
       });


       function convertCanvasToImage(canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          console.log(image)
          return image;
      }





      // ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    function gifPhotos() {
        var divs = $('.countdown');
        var offset = 1000

        for ( i=0; i<3; i++) {

            (function(i){
              setTimeout(function(){
                    $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")
                    var canvas = $('.canvas');
                    var context = canvas[canvas.length -1].getContext('2d');
                    var video = document.getElementById('video');
                    var filter = $('.filter')[0];

                     $('.flash').fadeIn(200);
                    $('.flash').fadeOut(400);

                    $(divs).css('font-size', '0px');

                   context.drawImage(video, 0, 0, 900, 675);
                   context.drawImage(filter, 0, 0, 900, 675);
            
                   $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

                   // watermark
                    // var img2 = new Image();
                    // img2.src = '/content/frontend/assets/img/logo_holder.png';
                    // context.drawImage(img2,530,420, 80, 43);

                    $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
              }, offset);
              offset += 1000
            }(i));  
        }
    }


    function showGifs() {
        $('#pictures').fadeOut();
        $('.gif_show').fadeIn();
        $('.gif_show').addClass('flex');

        // =========  NEED TO TURN IMAGES INTO GIF TO SHOW HERE  ================
        $('#submit_form').find('.image_encoded').remove();
        $('.canvas_holder img').each(function () {
            $('#submit_form').append($('<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'));
        });
        let deferreds = [];
        let imgs = [];
        $("#submit_form input[name='images[]']").each(function(i,el){
            deferreds.push(
                $.post('/contact/make_frame/', {
                    image: el.value
                },function(data) {
                  // Need to manually type in the url
                    var newref = "https://beny2019.popshap.net/"
                    imgs.push(newref+'content/uploads/Snapshots/'+data.image.image);
                    $('input[name="images[]"]')[i].value = data.image.id;
                }));
        });

        $('.fa-close').click(function(){
          $('.share_overlay').slideUp();
          $('.jQKeyboardContainer').slideUp();
          $(this).fadeOut();
          });


        //Show loading image
        $.when(...deferreds).then( function() {
             gifshot.createGIF({
                 'images': imgs,
                 'frameDuration':4,
                 'gifWidth': 900,
                 'gifHeight': 675
             },function(obj) {
                 if(!obj.error) {
                     var image = obj.image;
                     console.log(image)
                     $('#loader').fadeOut();
                     $('.button.submit').fadeIn();
                     $('.gif_show').append("<img src=' " + image + "'>")
                     $('#submit_form').append($('<input type="hidden" name="gif" value="'+image+'">'));
                     $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
                         $('#submit_form input[name=gif]').val(data.gif.id);
                     });
                 }
             });
         });
    }


    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').fadeIn(2000);
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });

        $('#pen').fadeIn();

    }



    // GIF CLICK
    document.getElementById("snap_gif").addEventListener("click", function() {
        $('.camera_button').fadeOut(1000);
        $('.camera_button').css('pointer-events', 'none');
        var pics = []

        countDown();
        setTimeout(function(){
            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")                
                var canvas = $('.canvas');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var filter = $('.filter')[0];
                 context.drawImage(video, 0, 0, 900, 675);
                 context.drawImage(filter, 0, 0, 900, 675);
               

               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');
               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


           gifPhotos();

        }, 4000 )

        setTimeout(function(){
            $('#photos').css('opacity', '0');
            setTimeout(function(){
                $('#photos').fadeOut();
            }, 1100);

            showGifs();
            $(this).css('pointer-events', 'all');
        }, 8000);
    });



    // PHOTO CLICK
    document.getElementById("snap_photo").addEventListener("click", function() {
        $('.camera_button').fadeOut(1000);
        $('.camera_button').css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='900' height='675'></canvas></div>")
                
                var canvas = $('#pic');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var filter = $('.filter')[0];
               context.drawImage(video, 0, 0, 900, 675);
               context.drawImage(filter, 0, 0, 900, 675);
               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


                $('#pictures').append("<canvas id='draw' class='drawing' width='900' height='675'></canvas>");
                var drawcanvas = $('#draw');
                var ctx = drawcanvas[0].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               ctx.drawImage(video, 0, 0, 900, 675);
               ctx.drawImage(background, 0, 0, 900, 675);






            }, 4000);

        setTimeout(function(){
            $('#photos').slideUp()
            $('.submit, .retake').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // DRAWING
    $('#pen').click(function(){
        // $('.canvas_holder').append("<canvas id='draw' width='900' height='675'></canvas>")
       context = document.getElementById('draw').getContext("2d");
        $('#draw').fadeIn()
        $('.draw_ctl').fadeIn();
        $('.colors').fadeIn();
        $('.sizes').fadeIn();
        $('#pictures .canvas_holder img').hide();

        can = document.getElementById('draw');
        can.addEventListener('mousedown', onMouseDown);
        can.addEventListener('touchstart', onTouchStart);
        can.addEventListener('mousemove', onMouseMove);
        can.addEventListener('touchmove', onTouchMove);
        can.addEventListener('mouseup', onMouseEnd);
        can.addEventListener('touchend', onMouseEnd);
        can.addEventListener('mouseleave', onMouseEnd);
        can.addEventListener('touchleave', onMouseEnd);
        $(this).fadeOut();
    });



    function onMouseDown (e){

      var mouseX = e.pageX - this.offsetLeft;
      var mouseY = e.pageY - this.offsetTop;
      console.log(e)

            
      paint = true;
      addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
      redraw();
      onTouchMove(e);
    };

    function onTouchStart (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop);
        redraw();
      
    };

    function onMouseMove (e){
        if(paint){
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
            redraw();
            onTouchMove(e)
      }
      
    };

    function onTouchMove (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop, true);
        redraw();
    };

    function onMouseEnd (e){
         paint = false;
    };



    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;
    var colorWhite = "#ffffff";
    var colorGreen = "#49a463";
    var colorBlue = "#30b9dc";
    var colorBlack = "#000000";
    var colorRed = "#c22628";

    var curColor = colorRed;
    var clickColor = new Array();

    $('#colorWhite').click(function(){
        curColor = colorWhite
    });

    $('#colorGreen').click(function(){
        curColor = colorGreen
    });

    $('#colorBlue').click(function(){
        curColor = colorBlue
    });

    $('#colorBlack').click(function(){
        curColor = colorBlack
    });

    $('#colorRed').click(function(){
        curColor = colorRed
    });

    $('.color').click(function(){
        $('.canvas_holder').css('background-color', curColor);
        setTimeout(function(){
            $('.canvas_holder').css('background-color', 'transparent');
        }, 200)
    });


    var small = 5;
    var medium = 13;
    var large = 20;
    var huge = 30;

    var curSize = medium;
    var clickSize = new Array();

    $('#small').click(function(){
        curSize = small;
    });

    $('#medium').click(function(){
        curSize = medium;
    });

    $('#large').click(function(){
        curSize = large;
    });

    $('#huge').click(function(){
        curSize = huge;
    });



    

    function addClick(x, y, dragging)
    {
      clickX.push(x);
      clickY.push(y);
      clickDrag.push(dragging);
      clickColor.push(curColor);
      clickSize.push(curSize);
    }

    function redraw(){
      // context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
      
      context.strokeStyle = "#df4b26";
      context.lineJoin = "round";
      

                
      for(var i=0; i < clickX.length; i++) {        
        context.beginPath();
        if(clickDrag[i] && i){
          context.moveTo(clickX[i-1], clickY[i-1]);
         }else{
           context.moveTo(clickX[i]-1, clickY[i]);
         }
         context.lineTo(clickX[i], clickY[i]);
         context.closePath();
         context.strokeStyle = clickColor[i];
         context.lineWidth = clickSize[i];
         context.stroke();
      }
    }


    $('.draw_ctl').click(function(){
      clickX = new Array();
      clickY = new Array();
      clickDrag = new Array();
      clickColor = new Array();
      clickSize = new Array();
      context.clearRect(0, 0, 900, 675);

      var drawcanvas = $('#draw');
      var ctx = drawcanvas[0].getContext('2d');
      var image = $('#pictures .canvas_holder img')[0];
       ctx.drawImage(image, 0, 0, 900, 675);
    });
            
    });
</script>






