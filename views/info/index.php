
    <div class="home full_view" style="background:url(<?=FRONT_IMG?>inner_bg.jpg)">

        <div class="return_home">
            <a href="/">
                <img src="<?=FRONT_IMG?>home-wht.png">
            </a>
        </div>

        <div class="site_description">
            <p>Bike New York is a nonprofit organization that promotes and encourages bicycling and bicycle safety through education, community outreach, and events.</p>

            <p>We offer free bike education programs throughout the five boroughs; kids and adults are taught how to ride bicycles, and how to do so safely and confidently in the city. In 2016 alone, more than 17,000 New Yorkers learned bike skills thanks to our efforts.</p>

            <p>We also organize numerous annual events, including the TD Five Boro Bike Tour Presented by REI (the largest bike ride in the U.S.), Bike Expo New York, and smaller regional and community rides.</p>


        <p>Swing by our booth to find out more about how you can get involved as a student, volunteer, or member, or go to <a href="http://www.bike.nyc/" target="_blank">bike.nyc.</a></p>
        </div>

            <div class="bike-brand">
                <a href="/">
                    <img src="<?=FRONT_IMG?>bike-brand.svg">
                </a>
            </div>
    </div>


<?php echo footer(); ?>

<style type="text/css">
html,body, .full_view, .row{
    height: 100%;
    min-height: 2500px !important;
    width: 100%;
}
</style>