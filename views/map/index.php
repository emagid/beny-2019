<style>
    .popover-title {
        text-align: center;
        border-bottom: 1px solid rgb(66,185,123);
        background-color: white;
    }

    .popover-content {
        text-align: center;
    }
</style>
<!-- HOME PAGE -->
<body>
    <div class="return_home" style="border-radius: 15px; position: absolute;">
        <a href="/">
            <img width="85" src="/content/frontend/images/home-wht.png" style="width: 85px">
        </a>
    </div>
    <div class="home" style="width: 100%; height: 100%; background: #383739;">
        <?php include __DIR__."/../../templates/modernvice/map.php";?>
    </div>
</body>
<!-- END HOME PAGE -->

<?php echo footer(); ?>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var notations = [
            { number: "1", image: "", name: "Music With A Message", url: "http://musicwam.org/" },
            { number: "2", image: "", name: "Gotham Cheer!", url: "https://www.instagram.com/gothamcheer/" },
            { number: "3", image: "", name: "Cheer NY", url: "http://www.cheerny.org/ Cheerleaders" },
            { number: "4", image: "", name: "Cheer NY", url: "http://www.cheerny.org/ Cheerleaders" },
            { number: "5", image: "", name: "Sterling Strings", url: "http://www.sterlingstringsnyc.com" },
            { number: "6", image: "", name: "Angela Missy Billups & Voices", url: "https://www.facebook.com/angela.billups1" },
            { number: "7", image: "", name: "Inner Roots", url: "http://www.reverbnation.com/innerrootsband" },
            { number: "8", image: "", name: "Bomba Yo", url: "http://www.bombayo.org/" },
            { number: "9", image: "", name: "Spanglish Fly", url: "http://www.spanglishfly.com" },
            { number: "10", image: "", name: "Street Beat Brass Band", url: "http://streetbeatbrass.com/" },
            { number: "11", image: "", name: "Dragonfly 13", url: "http://www.dragonfly13.com/" },
            { number: "12", image: "", name: "The Bergamot", url: "http://www.thebergamot.com/" },
            { number: "13", image: "", name: "Giant Flying Turtles", url: "http://www.giantflyingturtles.com/" },
            { number: "14", image: "", name: "Clif Bar Entertainment", url: "" },
            { number: "15", image: "", name: "2/3 Goat", url: "http://www.twothirdsgoat.com" },
            { number: "16", image: "", name: "Night Spins", url: "http://www.nightspins.com/" },
            { number: "17", image: "", name: "The Rusty Guns", url: "http://www.therustyguns.com/" },
            { number: "18", image: "", name: "Bonnie Brae Knights", url: "http://www.bonnie-brae.org/bonnie-brae-knights/" },
            { number: "19", image: "", name: "Ola Fresca/Jose Conde", url: "http://www.olafresca.com" },
            { number: "20", image: "", name: "Apple Bonkers", url: "https://theapplebonkers.wordpress.com/" },
            { number: "21", image: "", name: "Trapped In Static", url: "http://trappedinstatic.com/" },
            { number: "22", image: "", name: "#1 -The Suyat Band #2 - Men of Horses", url: "" },
            { number: "23", image: "", name: "DJ Luis", url: "http://www.nyeventsound.com" }
        ];

        function getTitle(title){
            return title;
        }

        function getContent(description, image){
            var html = '';
            html += '<div class="row">';
            if(image){
                html += '<div class="col-sm-12"><img src="'+ image +'"></div>';
                html += '<div class="col-sm-12">'+description+'</div>';
            } else {
                html += '<div class="col-sm-24">'+description+'</div>';
            }

            html += '</div>';
            return html;
        }

        function getBeachContent(content){
            var html = '';
            html += '<div class="row" style="text-align: left;">';
            for(i in content){
                var item = content[i];
                html += '<p>' + item['title'] +'</p>';
                html += '<ul>';
                for(var i in item["names"]){
                    html += '<li>' + item["names"][i] + '</li>';
                }
                html += '</ul>';
            }
            html += '</div>';
            return html;
        }

        var beachData = [
            {
                id: 1,
                title: 'FDR Rest Area',
                content: [
                    {
                        title: "Sponsor Product", 
                        names: ["Del Monte Bananas", "Nature Addict Fruit Snacks", "Davidovich Bagels", "Nuun active hydration"]
                    },
                    {
                        title: "Bike Repair Shop",
                        names: ["Treads Bike Shop", "Recycle-A-Bicycle Pump Team"]
                    }
                ]
            },
            {
                id: 2,
                title: 'Astoria Rest Area',
                content: [
                    {
                        title: "Sponsor Product", 
                        names: ["Del Monte Bananas", "Utz Pretzels", "Nuun active hydration", "Purity Organic Juice or Superjuice"]
                    },
                    {
                        title: "Bike Repair Shop",
                        names: ["Tony’s Bicycles", "Recycle-A-Bicycle Pump Team"]
                    },
                    {
                        title: "Entertainment",
                        names: ["The Bergamot"]
                    }
                ]
            },
            {
                id: 3, 
                title: 'ConEd Rest Area',
                content: [
                    {
                        title: "Sponsor Product", 
                        names: ["Del Monte Bananas", "SunMaid Raisins", "CLIF Blok Party", "Nuun active hydration"]
                    },
                    {
                        title: "Bike Repair Shop",
                        names: ["Chelsea Bicycles"]
                    },
                    {
                        title: "Entertainment",
                        names: ["Entertainment provided by Clifbar"]
                    }
                ]
            },
            {
                id: 4, 
                title: 'Commodore Barry Rest Area',
                content: [
                    {
                        title: "Sponsor Product", 
                        names: ["Del Monte Bananas", "SunMaid Raisins", "Nature Addict Fruit Snacks", "Davidovich Bagels", "Nuun active hydration"]
                    },
                    {
                        title: "Bike Repair Shop",
                        names: ["Sun and Air", "Recycle-A-Bicycle Pump Team"]
                    },
                    {
                        title: "Entertainment",
                        names: ["Ola Fresca/Jose Conde"]
                    }
                ]
            }
        ];

        for(var i in beachData){
            $('#beach-' + beachData[i].id).popover({
                animation: false,
                placement: 'bottom',
                title: beachData[i].title,
                content: getBeachContent(beachData[i].content),
                trigger: 'click',
                html: true
            });
        }

        $('[id ^= "beach-"]').on('click', function(){
            $('[id ^= "beach-"]').not(this).popover('hide');
        });

        $('[id ^= "beach-"]').on('shown.bs.popover', function(){
            var that = $(this);
            setTimeout(function(){
                that.popover('hide');
            }, 3000);

        });

        for(var i in notations){
            var notation = notations[i];
            $('#notation-' + notation["number"]).popover({
    //            container: 'body',
                animation: false,
                placement: 'bottom',
                title: notation['name'],
                content: getContent(notation['name'], notation['image']),
                trigger: 'click',
                html: true
            });

        }


        $('[id ^= "notation"]').on('click', function(){
            $('[id ^= "notation"]').not(this).popover('hide');
        });

        $('[id ^= "notation"]').on('shown.bs.popover', function(){
            var that = $(this);
            setTimeout(function(){
                that.popover('hide');
            }, 3000);

        });
    });
</script>
