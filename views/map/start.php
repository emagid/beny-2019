<style>
    .popover{
        max-width: 100%; /* Max Width of the popover (depending on the container!) */
        font-size: 20px;
    }
    .popover-title {
        text-align: center;
        font-size: 30px;
    }

    .start_map {
        text-transform: uppercase;
        font-family: 'Bike Sans';
        font-weight: bold;
        font-style: normal;
        font-size: 46px;
        margin-top: 10px;
        position: absolute;
        color: white;
        top: 55px;
        left: 68px;
    }

    svg {
        position: relative;
        top: -50px;
    }

    .home {
        background-color: rgb(86,181,221);
    }
</style>
<div class="home full_view" >
    <div class="return_home" style="border-radius: 15px; position: absolute;">
        <a href="/">
            <img width="85" src="/content/frontend/images/home-wht.png" style="width: 85px">
        </a>
    </div>
    <h3 class='start_map'>START MAP</h3>
    <?php include __DIR__."/../../templates/modernvice/start.php";?>
</div>


<?php echo footer(); ?>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        function getTitle(title){
            return title;
        }

        function getContent(description, image){
            var html = '';
            html += '<div class="row">';
            if(image){
                html += '<div class="col-sm-12"><img src="'+ image +'"></div>';
                html += '<div class="col-sm-12">'+description+'</div>';
            } else {
                html += '<div class="col-sm-24">'+description+'</div>';
            }

            html += '</div>';
            return html;
        }

        var data = [
            {
                id: 1, title:'Start Wave 1', content: 'Start Wave 1: 7:30am, arrive between 6:30-7:15am', color: '#FFA037'
            },
            {
                id: 2, title:'Start Wave 2', content: 'Start Wave 2: 8:10am, arrive between 7:10-7:55am', color: '#D959AE'
            },
            {
                id: 3, title:'Start Wave 3', content: 'Start Wave 3: 8:45am, arrive between 7:45-8:30am', color: '#076DB7'
            },
            {
                id: 4, title:'Start Wave 4', content: 'Start Wave 4: 9:20am, arrive between 8:20-9:05am', color: '#CB4537'
            }
        ];

        var colorId = {
            'wave-1': '#FFA037',
            'wave-2': '#D959AE',
            'wave-3': '#076DB7',
            'wave-4': '#CB4537',
        };

        for(var i in data){
            $('#wave-' + data[i].id).popover({
                animation: false,
                placement: 'bottom',
                title: data[i].title,
                content: getContent(data[i].content),
                trigger: 'click',
                html: true
            });
        }


        $('[id ^= "wave-"]').on('click', function(){
            var color = colorId[$(this).attr('id')];
            $('.popover').css('background-color', color);
            $('.popover-title').css('background-color', color);
            $('[id ^= "wave-"]').not(this).popover('hide');
        });

        $('[id ^= "wave-"]').on('shown.bs.popover', function(){
            var that = $(this);
            setTimeout(function(){
                that.popover('hide');
            }, 3000);

        });
    });
</script>
