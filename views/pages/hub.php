<div class="home full_view green">
	<div class="return_home">
	    <a href="/">
	        <img src="<?=FRONT_IMG?>home-wht.png">
	    </a>
	</div>

	<main class='centered'>
		<img src="<?=FRONT_IMG?>hub.png">
		<p>Check out our interactive activity area for games, prizes, and bike-themed fun. <strong>THE HUB is where it’s at!</strong></p>
		<div class='txt_holder'>
			<section class='txt'>
				<h3>Bike New York Bike Swing</h3>
				<p>This indoor bike swing is the place to snap a selfie or a photo with your best bike bud! (And be sure to tag us – @bikenewyork #bikeexpoNY)</p>
			</section>
			<section class='txt'>
				<h3>CLIF Goldsprints</h3>
				<p>Race your friends or someone new and see who will be crowned the fastest cyclist at Bike Expo New York.</p>
			</section>
			<section class='txt'>
				<h3>New Belgium Slow Ride</h3>
				<p>Slow your roll and put your balance to the test with a slow bike race!</p>
			</section>
			<section class='txt'>
				<h3>Bike New York Giant Plinko!</h3>
				<p>Saving your legs for Tour Day? Thanks to the generosity of our exhibitors below, you can win a prize just by hitting the right spot on the board!</p>
			</section>
		</div>
	</main>

	<div class='slider'>
        <p>Thanks to The Hub sponsors</p>
        <div class='logo_holder'>
            <video autoplay muted loop src='<?=FRONT_IMG?>logoGif.mov' type='video/mov'></video>
            <div class='logos sponsors'></div>
        </div>
    </div>
</div>

<script type="text/javascript">
	setTimeout(function(){
        $('.slider video').css('margin-left', '0');
        $('.slider .logo_holder').addClass('active');
    }, 500);


    setTimeout(function(){
        $('.slider .logos, .slider p').fadeIn(1000);
    }, 1500);
</script>