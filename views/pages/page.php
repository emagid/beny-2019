<?php
	$page = $model->page;
?>

<div class="banner <?=is_null($page->featured_image)?'cms':'';?>" <?=(is_null($page->featured_image)?'':'style="background-image:url(\''.UPLOAD_URL.'pages/'.$page->featured_image.'\')"')?> >
	<div class="container">
		<? if (is_null($page->featured_image)) { ?>
			<div class="inner">
				<h1><?=$page->title;?></h1>
			</div>
		<? } ?>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="cms-pages">
			<?=$page->description;?>	
		</div>	
	</div>
</div>

<?php echo footer(); ?>