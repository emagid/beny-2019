<div class="home full_view green">
	<div class="return_home">
	    <a href="/">
	        <img src="<?=FRONT_IMG?>home-wht.png">
	    </a>
	</div>

	<div class='info'>
		<p id='title'></p>
		<p id='desc'></p>
	</div>
	<img src="<?=FRONT_IMG?>route_map.jpg">
	<?php include __DIR__."/routes.php";?>
</div>

<script type="text/javascript">
	$('g').click(function(){
		$('.info').hide();
	});

	$('g').click(function(){
		var title = $(this).attr('data-title');
		var desc = $(this).attr('data-desc');
		var x = $(this)[0].getBoundingClientRect().x
		var y = $(this)[0].getBoundingClientRect().y

		if ( title ) {
			$('.info').fadeIn(500);
			$('.info #title').html(title);
			$('.info #desc').html(desc);
			$('.info').css({"top": y + 30, "left":  x+ 30});
		}else {
			$('info').fadeOut(500);
		}
	});

	$('svg').click(function(e){
		if ( !$(e.target).hasClass('icon') ) {
			if ( $(e.target).parents('.icon').length === 0 ) {
				$('.info').fadeOut(500);
			}
		}
	});

</script>
