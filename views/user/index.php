<div class="banner cms myaccount_banner">
	<div class="container">
		<div class="inner">
			<h1>My Account</h1>
		</div>
	</div>
</div>

<div class="container container_myaccount">
	<div class="myaccount_table nav_table">
		<? require_once(ROOT_DIR.'templates/'.$this->template.'/account_menu.php'); ?>
		<h4>Account Information</h4>
		<form action="<?=SITE_URL?>user/update" method="post">
			<div class="account_info">
				<div class="cust_profile_table">
			        <div class="form-group myaccount_input">
			          <label>Email Address</label><br />
			          <?php echo $model->form->editorFor('email');?>
			        </div>

			        <div class="form-group myaccount_input">
			          <label>First Name</label><br />
			          <?php echo $model->form->editorFor('first_name');?>
			        </div>

			        <div class="form-group myaccount_input">
			          <label>Last Name</label><br />
			          <?php echo $model->form->editorFor('last_name');?>
			        </div>

			         <div class="form-group myaccount_input">
			          <label>Password (leave blank to keep current password)</label><br />
			          <input type="password" name="password">
			        </div>
	    
			        <div class="form-group myaccount_input">
			          <button type="submit" class="btn btn-lg btn-success">Save</button>
			        </div>
		        </div>
		    </div><!-- .account_info -->
		</form>
	</div><!-- .table-responsive -->
</div><!-- .container -->




